(function() {
  'use strict';

  ////animacao no icone de scroll
  function scrollIconAnimate() {
    $('#scrollIcon').addClass('animated bounce').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend',
      function() {
        $('#scrollIcon').removeClass('animated bounce');
        setTimeout(function() {
          scrollIconAnimate();
        }, 2000);
      });
  };

  if (!isMobile.any) {
    scrollIconAnimate();
  };

  //scroll animado
  $('.animateAnchor').on('click', function(event) {
    event.preventDefault();
    $('html, body').animate({
      scrollTop: $($(this).attr("href")).offset().top - 78
    }, 1000);
  });

  // background menu na rolagem da pagina
	$(function() {
		var nav = $('.box-header');

		$(window).scroll(function () {
			if ($(this).scrollTop() > 80) {
				nav.addClass('white');
			} else {
				nav.removeClass('white');
			}
		});
	});

  //open menu mobile
  $(document).ready(function () {

    $('.menu-mobile').hide();

    $('.menu-nav-mobile span').on('click', function(event){

      event.preventDefault();

      var slideoutMenu = $('.menu-mobile');

      slideoutMenu.toggleClass("open");

      if (slideoutMenu.hasClass("open")) {

        slideoutMenu.show('200');
        $('html, body').animate({
          scrollTop: $($(this).attr("href")).offset().top - 78
        }, 1000);

      } else {
        slideoutMenu.hide('200');
      }

    });
  });

})();
