'use strict';

var moment = require('moment');
var nbSlug = require('nb-slug');
var TagService = require('../../adm/api/Tag/TagService');

function TagEngine(database) {
  var engine = this;
  var tagService = new TagService(database);

  engine.prepareLinkTag = function(tag) {
    tag.link = 'tags/' + nbSlug(tag.tagname + '-' + tag.id);
    return tag;
  };

  engine.getTag = function(result) {
    if (!result.rows) return undefined;
    var tag = result.data.pop();
    return engine.prepareLinkTag(tag);
  };

  engine.getTagById = function(tagId) {
    var params = engine.getParams();

    params.conditions.push({ field: 'id', comparator: '=', value: tagId});
    return tagService.search(params).then(engine.getTag);
  };

  engine.getParams = function() {
    return {
      limit: 1,

      conditions: [
        { field: 'tags.tagstatus', comparator: '=', value: 'online'},
        { field: 'tags.taglaunch', comparator: '<=', value: moment().format('YYYY-MM-DD').concat(' 23:59:59') }
      ],

      sortBy: [
        'tags.taglaunch DESC',
        'tags.id DESC'
      ]
    };
  };
}

module.exports = TagEngine;
