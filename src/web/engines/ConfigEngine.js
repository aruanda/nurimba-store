'use strict';

var ConfigService = require('../../adm/api/Config/ConfigService');

function ConfigEngine(database) {
  var engine = this;
  var configService = new ConfigService(database);

  engine.getBlogConfig = function() {
    var blogConfigId = 1;
    return configService.getConfigById(blogConfigId).then(function(config) {
      config.path = '/blog/';
      return config;
    });
  };

  engine.getStoreConfig = function() {
    var storeConfigId = 2;
    return configService.getConfigById(storeConfigId).then(function(config) {
      config.path = '/';
      return config;
    });
  };

  engine.mergeSEO = function(config, title, seo) {
    if (!seo) seo = {};
    if (!config) config = {};
    if (!config.confseo) config.confseo = {};

    if (title) config.confname = title;

    if (seo.robots)      config.confseo.robots      = seo.robots;
    if (seo.keywords)    config.confseo.keywords    = seo.keywords;
    if (seo.description) config.confseo.description = seo.description;

    if (seo.googleTitle)       config.confseo.googleTitle       = seo.googleTitle;
    if (seo.googleDescription) config.confseo.googleDescription = seo.googleDescription;

    if (seo.facebookTitle)       config.confseo.facebookTitle       = seo.facebookTitle;
    if (seo.facebookDescription) config.confseo.facebookDescription = seo.facebookDescription;

    if (seo.twitterTitle)       config.confseo.twitterTitle       = seo.twitterTitle;
    if (seo.twitterDescription) config.confseo.twitterDescription = seo.twitterDescription;

    if (seo.googleImage   && seo.googleImage.src)   config.confseo.googleImage.src   = seo.googleImage.src;
    if (seo.googleImage   && seo.googleImage.alt)   config.confseo.googleImage.alt   = seo.googleImage.alt;
    if (seo.twitterImage  && seo.twitterImage.src)  config.confseo.twitterImage.src  = seo.twitterImage.src;
    if (seo.twitterImage  && seo.twitterImage.alt)  config.confseo.twitterImage.alt  = seo.twitterImage.alt;
    if (seo.facebookImage && seo.facebookImage.src) config.confseo.facebookImage.src = seo.facebookImage.src;
    if (seo.facebookImage && seo.facebookImage.alt) config.confseo.facebookImage.alt = seo.facebookImage.alt;

    return config;
  };
}

module.exports = ConfigEngine;
