'use strict';

var moment = require('moment');
var nbSlug = require('nb-slug');
var PostService = require('../../adm/api/Post/PostService');

function PostEngine(database) {
  var engine = this;
  var postService = new PostService(database);

  engine.prepareLinkPost = function(post) {
    post.link = nbSlug(post.posttitle + '-' + post.id);

    return post;
  };

  engine.getPost = function(result) {
    if (!result.rows) return undefined;
    var post = result.data.pop();
    return engine.prepareLinkPost(post);
  };

  engine.getPostById = function(postId) {
    var params = engine.getParams();
    params.limit = 1;
    params.conditions.push({ field: 'id', comparator: '=', value: postId});
    return postService.search(params).then(engine.getPost);
  };

  engine.getPriorPostById = function(postId) {
    var params = engine.getParams();
    params.limit = 1;
    params.conditions.push({ field: 'id', comparator: '>', value: postId});
    params.sortBy = [
      'posts.postlaunch ASC',
      'posts.id ASC'
    ];

    return postService.search(params).then(engine.getPost);
  };

  engine.getNextPostById = function(postId) {
    var params = engine.getParams();
    params.limit = 1;
    params.conditions.push({ field: 'id', comparator: '<', value: postId});
    return postService.search(params).then(engine.getPost);
  };

  engine.getPosts = function(limitPosts) {
    var params = engine.getParams();
    if (limitPosts) params.limit = limitPosts;

    return postService.search(params).then(function(result) {
      var posts = result.rows ? result.data : [];
      return posts.map(engine.prepareLinkPost);
    });
  };

  engine.getPostsByTag = function(tag, maxPosts) {
    var params = engine.getParams();
    if (maxPosts) params.limit = maxPosts;

    params.joins.push('JOIN posttags ON (posts.id = posttags.postid)');

    params.conditions.push({
      field: 'posttags.tagid',
      comparator: '=',
      value: tag.id
    });

    return postService.search(params).then(function(result) {
      var posts = result.rows ? result.data : [];
      return posts.map(engine.prepareLinkPost);
    });
  };

  engine.getPostsRelated = function(post, maxPosts) {
    var withTags = Boolean(post && post.posttags && post.posttags.length);

    var tagsIds = [];
    if (withTags) tagsIds = post.posttags.map(function(tag) {
      return tag.id;
    });

    var params = engine.getParams();
    if (maxPosts) params.limit = maxPosts;

    params.joins.push('JOIN posttags ON (posts.id = posttags.postid)');

    params.conditions.push({
      field: 'posttags.tagid',
      comparator: 'in',
      value: tagsIds
    });

    params.conditions.push({
      field: 'posttags.postid',
      comparator: '<>',
      value: post.id
    });

    params.groupBy = ['posts.id'];

    return postService.search(params).then(function(result) {
      var posts = result.rows ? result.data : [];
      return posts.map(engine.prepareLinkPost);
    });
  };

  engine.getPostsRecent = function(post, maxPosts) {
    var withRelated = Boolean(post && post.related && post.related.length);

    var ignoreIds = [post.id];
    if (withRelated) post.related.forEach(function(rel) {
      ignoreIds.push(rel.id);
    });

    var params = engine.getParams();
    if (maxPosts) params.limit = maxPosts;

    params.conditions.push({
      field: 'posts.id',
      comparator: 'not in',
      value: ignoreIds
    });

    return postService.search(params).then(function(result) {
      var posts = result.rows ? result.data : [];
      return posts.map(engine.prepareLinkPost);
    });
  };

  engine.getParams = function() {
    return {
      limit: 3,

      joins: [],

      conditions: [
        { field: 'posts.poststatus', comparator: '=',  value: 'online'},
        { field: 'posts.postlaunch', comparator: '<=', value: moment().format('YYYY-MM-DD').concat(' 23:59:59') }
      ],

      sortBy: [
        'posts.postlaunch DESC',
        'posts.id DESC'
      ]
    };
  };
}

module.exports = PostEngine;
