'use strict';

function PaymentRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/payment';

  route.action = function(req, res, next) {
    res.render('store/payment');
    next();
  };
}

module.exports = PaymentRoute;
