'use strict';

function SuccessRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/success';

  route.action = function(req, res, next) {
    res.render('store/success');
    next();
  };
}

module.exports = SuccessRoute;
