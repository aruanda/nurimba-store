'use strict';

var ConfigEngine = require('../../engines/ConfigEngine');

function CategoryRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/categoria';

  route.action = function(req, res, next) {
    var configEngine = new ConfigEngine(req.database);

    configEngine.getStoreConfig().then(function(config) {
      res.render('store/category', {
        config: config
      });
    }).then(next);
  };
}

module.exports = CategoryRoute;
