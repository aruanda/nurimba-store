'use strict';

function AccountRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/account';

  route.action = function(req, res, next) {
    res.render('store/account');
    next();
  };
}

module.exports = AccountRoute;
