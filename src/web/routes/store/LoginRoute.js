'use strict';

function LoginRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/login';

  route.action = function(req, res, next) {
    res.render('store/login');
    next();
  };
}

module.exports = LoginRoute;
