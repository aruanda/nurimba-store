'use strict';

var ConfigEngine = require('../../engines/ConfigEngine');

function HomeRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/';

  route.action = function(req, res, next) {
    var configEngine = new ConfigEngine(req.database);

    configEngine.getStoreConfig().then(function(config) {
      res.render('store/home', {
        config: config
      });
    }).then(next);
  };
}

module.exports = HomeRoute;
