'use strict';

var ConfigEngine = require('../../engines/ConfigEngine');

function CategoryRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/contato';

  route.action = function(req, res, next) {
    var configEngine = new ConfigEngine(req.database);

    configEngine.getStoreConfig().then(function(config) {
      res.render('store/contact', {
        config: config
      });
    }).then(next);
  };
}

module.exports = CategoryRoute;
