'use strict';

var ConfigEngine = require('../../engines/ConfigEngine');

function CartRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/carrinho';

  route.action = function(req, res, next) {
    var configEngine = new ConfigEngine(req.database);

    configEngine.getStoreConfig().then(function(config) {
      res.render('store/cart', {
        config: config
      });
    }).then(next);
  };
}

module.exports = CartRoute;
