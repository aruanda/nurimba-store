'use strict';

function ProductRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/product';

  route.action = function(req, res, next) {
    res.render('store/product');
    next();
  };
}

module.exports = ProductRoute;
