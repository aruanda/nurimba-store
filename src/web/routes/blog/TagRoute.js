'use strict';

var TagEngine = require('../../engines/TagEngine');
var PostEngine = require('../../engines/PostEngine');
var ConfigEngine = require('../../engines/ConfigEngine');

function TagRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = /\/blog\/tags\/([\w-\.]+)\-(\d+)?$/;

  route.action = function(req, res, next) {
    var props        = Object.keys(req.params);
    var propsId      = props.pop();
    var tagId        = req.params[propsId];
    var tagEngine    = new TagEngine(req.database);
    var postEngine   = new PostEngine(req.database);
    var configEngine = new ConfigEngine(req.database);

    var getPosts = function(tag) {
      var maxPosts = 6;
      return postEngine.getPostsByTag(tag, maxPosts).then(function(posts) {
        tag.posts = posts;
        return tag;
      });
    };

    var renderTag = function(tag) {
      return configEngine.getBlogConfig().then(function(config) {
        var seoPage   = tag.tagseo;
        var titlePage = tag.tagname;
        var confPage  = configEngine.mergeSEO(config, titlePage, seoPage);

        res.render('blog/tag', {
          tag: tag,
          config: confPage,
          menuBlog: 'active',
          viewSlider: false,
          classSlider: '-int'
        });
      });
    };

    tagEngine
      .getTagById(tagId)
      .then(getPosts)
      .then(renderTag)
      .then(next)
      .catch(function(err) {
        console.log(err);
        next();
      });
  };
}

module.exports = TagRoute;
