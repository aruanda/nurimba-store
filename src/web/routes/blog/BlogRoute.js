'use strict';

var TagEngine = require('../../engines/TagEngine');
var PostEngine = require('../../engines/PostEngine');
var ConfigEngine = require('../../engines/ConfigEngine');

function BlogRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/blog';

  route.action = function(req, res, next) {
    var limitPosts   = 5;
    var tagEngine    = new TagEngine(req.database);
    var postEngine   = new PostEngine(req.database);
    var configEngine = new ConfigEngine(req.database);

    var getLinkTags = function(posts) {
      posts.forEach(function(post) {
        var tags = post.posttags || [];
        tags.forEach(tagEngine.prepareLinkTag);
      });

      return posts;
    };

    var renderPosts = function(posts) {
      return configEngine.getBlogConfig().then(function(config) {
        res.render('blog/home', {
          posts: posts,
          config: config,
          menuBlog: 'active',
          viewSlider: false,
          classSlider: '-int'
        });
      });
    };

    return postEngine
      .getPosts(limitPosts)
      .then(getLinkTags)
      .then(renderPosts)
      .then(next)
      .catch(next);
  };
}

module.exports = BlogRoute;
