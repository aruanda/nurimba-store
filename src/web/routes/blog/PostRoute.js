'use strict';

var TagEngine = require('../../engines/TagEngine');
var PostEngine = require('../../engines/PostEngine');
var ConfigEngine = require('../../engines/ConfigEngine');

function PostRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = /\/blog\/([\w-\.]+)\-(\d+)?$/;

  route.action = function(req, res, next) {
    var props        = Object.keys(req.params);
    var propsId      = props.pop();
    var postId       = req.params[propsId];
    var tagEngine    = new TagEngine(req.database);
    var postEngine   = new PostEngine(req.database);
    var configEngine = new ConfigEngine(req.database);

    var getPriorPost = function(post) {
      return postEngine.getPriorPostById(post.id).then(function(priorPost) {
        post.priorPost = priorPost;
        return post;
      });
    };

    var getLinkTags = function(post) {
      var tags = post.posttags || [];
      tags.forEach(tagEngine.prepareLinkTag);
      return post;
    };

    var getRelated = function(post) {
      var maxRelateds = 3;

      return postEngine.getPostsRelated(post, maxRelateds).then(function(relatedPosts) {
        post.related = relatedPosts;
        return post;
      });
    };

    var getRecent = function(post) {
      var maxRelateds = 3;
      return postEngine.getPostsRecent(post, maxRelateds).then(function(recentPosts) {
        post.recent = recentPosts;
        return post;
      });
    };

    var getNextPost = function(post) {
      return postEngine.getNextPostById(post.id).then(function(nextPost) {
        post.nextPost = nextPost;
        return post;
      });
    };

    var renderPost = function(post) {
      return configEngine.getBlogConfig().then(function(config) {
        var seoPage   = post.postseo;
        var titlePage = post.posttitle;
        var confPage  = configEngine.mergeSEO(config, titlePage, seoPage);

        res.render('blog/post', {
          post: post,
          config: confPage,
          menuBlog: 'active',
          viewSlider: false,
          classSlider: '-int'
        });
      });
    };

    postEngine
      .getPostById(postId)
      .then(getPriorPost)
      .then(getLinkTags)
      .then(getNextPost)
      .then(getRelated)
      .then(getRecent)
      .then(renderPost)
      .then(next)
      .catch(next);
  };
}

module.exports = PostRoute;
