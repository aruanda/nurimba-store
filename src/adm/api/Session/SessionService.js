'use strict';

var nurimbaCreate  = require('../../lib/NurimbaCreate');
var nurimbaUpdate  = require('../../lib/NurimbaUpdate');
var nurimbaSearch  = require('../../lib/NurimbaSearch');
var sessionFields = require('./SessionFields')();

function SessionService(connection) {
  var service = this;
  var sessionTable = 'sessions';

  service.create = nurimbaCreate(connection, sessionTable, sessionFields);
  service.update = nurimbaUpdate(connection, sessionTable, sessionFields);
  service.search = nurimbaSearch(connection, sessionTable, sessionFields);
}

module.exports = SessionService;
