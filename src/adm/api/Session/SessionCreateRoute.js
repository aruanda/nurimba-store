'use strict';

var SessionService = require('./SessionService');

function SessionCreateRoute() {
  var route = this;

  route.verb = 'POST';
  route.url  = '/api/sessions';

  route.action = function(req, res, next) {
    var sessionArgs = req.body;
    var sessionService = new SessionService(req.database);

    sessionService.create(sessionArgs).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = SessionCreateRoute;
