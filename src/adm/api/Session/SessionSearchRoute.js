'use strict';

var SessionService = require('./SessionService');

function SessionSearchRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/api/sessions';

  route.action = function(req, res, next) {
    var params = {};
    var withParams = Boolean(req && req.query && req.query.q);
    if (withParams) params = JSON.parse(req.query.q);

    var sessionService = new SessionService(req.database);
    sessionService.search(params).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = SessionSearchRoute;
