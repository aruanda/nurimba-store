'use strict';

var SessionService = require('./SessionService');

function SessionUpdateRoute() {
  var route = this;

  route.verb = 'PUT';
  route.url  = '/api/sessions';

  route.action = function(req, res, next) {
    var sessionArgs = req.body;
    var sessionService = new SessionService(req.database);

    sessionService.update(sessionArgs).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = SessionUpdateRoute;
