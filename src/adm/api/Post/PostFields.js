'use strict';

var ValidationRequired = require('../../lib/validations/ValidationRequired');

module.exports = function() {
  return [
    {
      attr: 'id',
      kind: 'primary',

      viewForm: true,
      orderForm: 1,

      viewList: true,
      orderList: 1,

      viewPreview: true,
      orderPreview: 1,

      viewFilter: true,
      orderFilter: 1,

      colName: 'firstCol',

      validations: []
    },

    {
      attr: 'posttitle',
      kind: 'title',

      viewForm: true,
      orderForm: 10,

      pathSlug: 'blog/',

      viewList: true,
      orderList: 10,

      viewPreview: true,
      orderPreview: 10,

      viewFilter: false,
      orderFilter: 10,

      colName: 'firstCol',

      validations: [
        new ValidationRequired()
      ]
    },

    {
      attr: 'postresume',
      kind: 'textarea',

      viewForm: true,
      orderForm: 35,

      viewList: false,
      orderList: 35,

      viewPreview: true,
      orderPreview: 35,

      viewFilter: false,
      orderFilter: 40,

      colName: 'firstCol',

      validations: []
    },

    {
      attr: 'postdesc',
      kind: 'text',

      viewForm: true,
      orderForm: 40,

      viewList: false,
      orderList: 40,

      viewPreview: true,
      orderPreview: 40,

      viewFilter: false,
      orderFilter: 40,

      colName: 'firstCol',

      validations: []
    },

    {
      attr: 'postacervo',
      kind: 'images',

      viewForm: true,
      orderForm: 45,

      viewList: false,
      orderList: 45,

      viewPreview: true,
      orderPreview: 45,

      viewFilter: false,
      orderFilter: 45,

      colName: 'firstCol',

      validations: []
    },

    {
      attr: 'postseo',
      kind: 'seo',

      viewForm: true,
      orderForm: 50,

      viewList: false,
      orderList: 50,

      viewPreview: true,
      orderPreview: 50,

      viewFilter: false,
      orderFilter: 50,

      colName: 'firstCol',

      validations: []
    },

    {
      attr: 'postimagemain',
      kind: 'imageMain',

      viewForm: true,
      orderForm: 70,

      viewList: false,
      orderList: 70,

      viewPreview: true,
      orderPreview: 70,

      viewFilter: false,
      orderFilter: 70,

      colName: 'secondCol',

      validations: []
    },

    {
      attr: 'posttags',
      kind: 'suggestMultiple',

      suggest: {
        domain: 'tags',
        label: 'tagname',

        relation: 'postTags',
        relDomain: 'postid',
        relSuggest: 'tagid'
      },

      viewForm: true,
      orderForm: 90,

      viewList: true,
      orderList: 90,

      viewPreview: true,
      orderPreview: 90,

      viewFilter: false,
      orderFilter: 90,

      colName: 'secondCol',

      validations: [
        new ValidationRequired()
      ]
    },

    {
      attr: 'postpublication',
      kind: 'publication',

      publication: {
        fieldStatus: 'poststatus',
        fieldDateTime: 'postlaunch',
        optionsStatus: [
          'offline',
          'rascunho',
          'online'
        ]
      },

      viewForm: true,
      orderForm: 95,

      viewList: false,
      orderList: 95,

      viewPreview: true,
      orderPreview: 95,

      viewFilter: false,
      orderFilter: 95,

      colName: 'secondCol',

      validations: []
    },

    {
      attr: 'poststatus',
      kind: 'hidden',

      viewForm: true,
      orderForm: 80,

      viewList: true,
      orderList: 80,

      viewPreview: true,
      orderPreview: 80,

      viewFilter: false,
      orderFilter: 80,

      colName: 'secondCol',

      validations: []
    },

    {
      attr: 'postlaunch',
      kind: 'hidden',

      viewForm: true,
      orderForm: 80,

      viewList: true,
      orderList: 80,

      viewPreview: true,
      orderPreview: 80,

      viewFilter: false,
      orderFilter: 80,

      colName: 'secondCol',

      validations: []
    }
  ];
};
