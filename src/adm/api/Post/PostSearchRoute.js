'use strict';

var PostService = require('./PostService');

function PostSearchRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/api/posts';

  route.action = function(req, res, next) {
    var params = {};
    var withParams = Boolean(req && req.query && req.query.q);
    if (withParams) params = JSON.parse(req.query.q);

    var postService = new PostService(req.database);
    postService.search(params).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = PostSearchRoute;
