'use strict';

var PostService = require('./PostService');

function PostUpdateRoute() {
  var route = this;

  route.verb = 'PUT';
  route.url  = '/api/posts';

  route.action = function(req, res, next) {
    var postArgs = req.body;
    var postService = new PostService(req.database);

    postService.update(postArgs).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = PostUpdateRoute;
