'use strict';

var nurimbaCreate = require('../../lib/NurimbaCreate');
var nurimbaUpdate = require('../../lib/NurimbaUpdate');
var nurimbaSearch = require('../../lib/NurimbaSearch');
var postFields    = require('./PostFields')();

function PostService(connection) {
  var service = this;
  var postTable = 'posts';

  service.create = nurimbaCreate(connection, postTable, postFields);
  service.update = nurimbaUpdate(connection, postTable, postFields);
  service.search = nurimbaSearch(connection, postTable, postFields);
}

module.exports = PostService;
