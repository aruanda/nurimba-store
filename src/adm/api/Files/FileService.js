'use strict';

var nurimbaImages = require('../../lib/NurimbaImages');

function FileService(connection) {
  var service = this;

  service.upload = function(args, files) {
    var rowObj = {};
    var imagesFields = [{ attr: args.attr }];
    return nurimbaImages(connection, imagesFields, rowObj, files);
  };
}

module.exports = FileService;
