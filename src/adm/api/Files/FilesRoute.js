'use strict';

var FileService = require('./FileService');

function FilesRoute() {
  var route = this;

  route.verb   = 'POST';
  route.url    = '/api/files';
  route.upload = true;

  route.action = function(req, res, next) {
    var fileArgs = req.body;
    var fileFiles = req.files;
    var fileService = new FileService(req.database);

    fileService.upload(fileArgs, fileFiles).then(function(resultRow) {
      res.json(resultRow);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = FilesRoute;
