'use strict';

var nurimbaCreate  = require('../../lib/NurimbaCreate');
var nurimbaUpdate  = require('../../lib/NurimbaUpdate');
var nurimbaSearch  = require('../../lib/NurimbaSearch');
var tagFields = require('./TagFields')();

function TagService(connection) {
  var service = this;
  var tagTable = 'tags';

  service.create = nurimbaCreate(connection, tagTable, tagFields);
  service.update = nurimbaUpdate(connection, tagTable, tagFields);
  service.search = nurimbaSearch(connection, tagTable, tagFields);
}

module.exports = TagService;
