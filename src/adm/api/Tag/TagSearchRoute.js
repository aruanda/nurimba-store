'use strict';

var TagService = require('./TagService');

function TagSearchRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/api/tags';

  route.action = function(req, res, next) {
    var params = {};
    var withParams = Boolean(req && req.query && req.query.q);
    if (withParams) params = JSON.parse(req.query.q);

    var tagService = new TagService(req.database);
    tagService.search(params).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = TagSearchRoute;
