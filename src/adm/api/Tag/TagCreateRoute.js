'use strict';

var TagService = require('./TagService');

function TagCreateRoute() {
  var route = this;

  route.verb = 'POST';
  route.url  = '/api/tags';

  route.action = function(req, res, next) {
    var tagArgs = req.body;
    var tagService = new TagService(req.database);

    tagService.create(tagArgs).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = TagCreateRoute;
