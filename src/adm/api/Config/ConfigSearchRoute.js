'use strict';

var ConfigService = require('./ConfigService');

function ConfigSearchRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/api/configs';

  route.action = function(req, res, next) {
    var params = {};
    var withParams = Boolean(req && req.query && req.query.q);
    if (withParams) params = JSON.parse(req.query.q);

    var configService = new ConfigService(req.database);
    configService.search(params).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = ConfigSearchRoute;
