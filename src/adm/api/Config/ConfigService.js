'use strict';

var nurimbaCreate = require('../../lib/NurimbaCreate');
var nurimbaUpdate = require('../../lib/NurimbaUpdate');
var nurimbaSearch = require('../../lib/NurimbaSearch');
var configFields  = require('./ConfigFields')();

function ConfigService(connection) {
  var service = this;
  var configTable = 'configs';

  service.create = nurimbaCreate(connection, configTable, configFields);
  service.update = nurimbaUpdate(connection, configTable, configFields);
  service.search = nurimbaSearch(connection, configTable, configFields);

  service.getConfigById = function(configId) {
    var params = service.getParams();

    params.conditions.push({
      field: 'id',
      comparator: '=',
      value: configId
    });

    return service.search(params).then(function(result) {
      if (!result.rows) return undefined;
      var config = result.data.pop();
      return config;
    });
  };

  service.getParams = function() {
    return {
      limit: 1,
      sortBy: [],
      conditions: []
    };
  };
}

module.exports = ConfigService;
