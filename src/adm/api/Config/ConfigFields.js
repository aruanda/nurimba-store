'use strict';

module.exports = function() {
  return [
    {
      attr: 'id',
      kind: 'hidden',

      viewForm: true,
      orderForm: 1,

      viewList: true,
      orderList: 1,

      viewPreview: true,
      orderPreview: 1,

      viewFilter: true,
      orderFilter: 1,

      colName: 'firstCol',

      validations: []
    },

    {
      attr: 'confweb',
      kind: 'group',

      group: {
        inputs: [
          { attr: 'confname',      type: 'text', classBox: 'client-info col-xs-12 col-sm-12', icon: 'fa fa-home'       },
          { attr: 'conffacebook',  type: 'text', classBox: 'client-info col-xs-12 col-sm-6',  icon: 'fa fa-facebook'   },
          { attr: 'conftwitter',   type: 'text', classBox: 'client-info col-xs-12 col-sm-6',  icon: 'fa fa-twitter'    },
          { attr: 'confgoogle',    type: 'text', classBox: 'client-info col-xs-12 col-sm-6',  icon: 'fa fa-google'     },
          { attr: 'confinstagram', type: 'text', classBox: 'client-info col-xs-12 col-sm-6',  icon: 'fa fa-instagram'  },
          { attr: 'confwhatsapp',  type: 'text', classBox: 'client-info col-xs-12 col-sm-6',  icon: 'fa fa-whatsapp'   },
          { attr: 'confemail',     type: 'text', classBox: 'client-info col-xs-12 col-sm-6',  icon: 'fa fa-envelope-o' },
          { attr: 'confcep',       type: 'text', classBox: 'client-info col-xs-12 col-sm-6',  icon: 'fa fa-truck'      },
          { attr: 'confstatecity', type: 'text', classBox: 'client-info col-xs-12 col-sm-6',  icon: 'fa fa-map-o'      },
          { attr: 'confaddress',   type: 'text', classBox: 'client-info col-xs-12 col-sm-12', icon: 'fa fa-home'       }
        ]
      },

      viewForm: true,
      orderForm: 10,

      viewList: true,
      orderList: 10,

      viewPreview: true,
      orderPreview: 10,

      viewFilter: false,
      orderFilter: 10,

      colName: 'firstCol',

      validations: []
    },

    {
      attr: 'confacervo',
      kind: 'images',

      viewForm: true,
      orderForm: 45,

      viewList: false,
      orderList: 45,

      viewPreview: true,
      orderPreview: 45,

      viewFilter: false,
      orderFilter: 45,

      colName: 'firstCol',

      validations: []
    },

    {
      attr: 'confseo',
      kind: 'seo',

      viewForm: true,
      orderForm: 50,

      viewList: false,
      orderList: 50,

      viewPreview: true,
      orderPreview: 50,

      viewFilter: false,
      orderFilter: 50,

      colName: 'firstCol',

      validations: []
    },

    {
      attr: 'confpublication',
      kind: 'publication',

      publication: {
        fieldStatus: 'confstatus',
        fieldDateTime: 'conflaunch',
        optionsStatus: [
          'construcao',
          'menutencao',
          'online'
        ]
      },

      viewForm: true,
      orderForm: 95,

      viewList: false,
      orderList: 95,

      viewPreview: true,
      orderPreview: 95,

      viewFilter: false,
      orderFilter: 95,

      colName: 'secondCol',

      validations: []
    },

    { attr: 'confstatus',    kind: 'hidden', viewForm: true, orderForm: 0, viewList: true, orderList: 0, viewPreview: true, orderPreview: 0, viewFilter: false, orderFilter: 0, validations: [] },
    { attr: 'conflaunch',    kind: 'hidden', viewForm: true, orderForm: 0, viewList: true, orderList: 0, viewPreview: true, orderPreview: 0, viewFilter: false, orderFilter: 0, validations: [] },
    { attr: 'confname',      kind: 'hidden', viewForm: true, orderForm: 0, viewList: true, orderList: 0, viewPreview: true, orderPreview: 0, viewFilter: false, orderFilter: 0, validations: [] },
    { attr: 'conffacebook',  kind: 'hidden', viewForm: true, orderForm: 0, viewList: true, orderList: 0, viewPreview: true, orderPreview: 0, viewFilter: false, orderFilter: 0, validations: [] },
    { attr: 'conftwitter',   kind: 'hidden', viewForm: true, orderForm: 0, viewList: true, orderList: 0, viewPreview: true, orderPreview: 0, viewFilter: false, orderFilter: 0, validations: [] },
    { attr: 'confgoogle',    kind: 'hidden', viewForm: true, orderForm: 0, viewList: true, orderList: 0, viewPreview: true, orderPreview: 0, viewFilter: false, orderFilter: 0, validations: [] },
    { attr: 'confinstagram', kind: 'hidden', viewForm: true, orderForm: 0, viewList: true, orderList: 0, viewPreview: true, orderPreview: 0, viewFilter: false, orderFilter: 0, validations: [] },
    { attr: 'confwhatsapp',  kind: 'hidden', viewForm: true, orderForm: 0, viewList: true, orderList: 0, viewPreview: true, orderPreview: 0, viewFilter: false, orderFilter: 0, validations: [] },
    { attr: 'confemail',     kind: 'hidden', viewForm: true, orderForm: 0, viewList: true, orderList: 0, viewPreview: true, orderPreview: 0, viewFilter: false, orderFilter: 0, validations: [] },
    { attr: 'confcep',       kind: 'hidden', viewForm: true, orderForm: 0, viewList: true, orderList: 0, viewPreview: true, orderPreview: 0, viewFilter: false, orderFilter: 0, validations: [] },
    { attr: 'confstatecity', kind: 'hidden', viewForm: true, orderForm: 0, viewList: true, orderList: 0, viewPreview: true, orderPreview: 0, viewFilter: false, orderFilter: 0, validations: [] },
    { attr: 'confaddress',   kind: 'hidden', viewForm: true, orderForm: 0, viewList: true, orderList: 0, viewPreview: true, orderPreview: 0, viewFilter: false, orderFilter: 0, validations: [] }
  ];
};
