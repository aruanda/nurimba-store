'use strict';

var ConfigService = require('./ConfigService');

function ConfigCreateRoute() {
  var route = this;

  route.verb = 'POST';
  route.url  = '/api/configs';

  route.action = function(req, res, next) {
    var configArgs = req.body;
    var configService = new ConfigService(req.database);

    configService.create(configArgs).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = ConfigCreateRoute;
