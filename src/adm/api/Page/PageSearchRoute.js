'use strict';

var PageService = require('./PageService');

function PageSearchRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/api/pages';

  route.action = function(req, res, next) {
    var params = {};
    var withParams = Boolean(req && req.query && req.query.q);
    if (withParams) params = JSON.parse(req.query.q);

    var pageService = new PageService(req.database);
    pageService.search(params).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = PageSearchRoute;
