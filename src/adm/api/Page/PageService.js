'use strict';

var nurimbaCreate = require('../../lib/NurimbaCreate');
var nurimbaUpdate = require('../../lib/NurimbaUpdate');
var nurimbaSearch = require('../../lib/NurimbaSearch');
var pageFields = require('./PageFields')();

function PageService(connection) {
  var service = this;
  var pageTable = 'pages';

  service.create = nurimbaCreate(connection, pageTable, pageFields);
  service.update = nurimbaUpdate(connection, pageTable, pageFields);
  service.search = nurimbaSearch(connection, pageTable, pageFields);
}

module.exports = PageService;
