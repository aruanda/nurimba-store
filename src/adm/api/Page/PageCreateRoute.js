'use strict';

var PageService = require('./PageService');

function PageCreateRoute() {
  var route = this;

  route.verb = 'POST';
  route.url  = '/api/pages';

  route.action = function(req, res, next) {
    var pageArgs = req.body;
    var pageService = new PageService(req.database);

    pageService.create(pageArgs).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = PageCreateRoute;
