'use strict';

var PageService = require('./PageService');

function PageUpdateRoute() {
  var route = this;

  route.verb = 'PUT';
  route.url  = '/api/pages';

  route.action = function(req, res, next) {
    var pageArgs = req.body;
    var pageService = new PageService(req.database);

    pageService.update(pageArgs).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = PageUpdateRoute;
