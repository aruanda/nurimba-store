'use strict';

var ProductService = require('./ProductService');

function ProductSearchRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/api/products';

  route.action = function(req, res, next) {
    var params = {};
    var withParams = Boolean(req && req.query && req.query.q);
    if (withParams) params = JSON.parse(req.query.q);

    var productService = new ProductService(req.database);
    productService.search(params).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = ProductSearchRoute;
