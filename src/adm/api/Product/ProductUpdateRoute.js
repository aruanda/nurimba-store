'use strict';

var ProductService = require('./ProductService');

function ProductUpdateRoute() {
  var route = this;

  route.verb = 'PUT';
  route.url  = '/api/products';

  route.action = function(req, res, next) {
    var productArgs = req.body;
    var productService = new ProductService(req.database);

    productService.update(productArgs).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = ProductUpdateRoute;
