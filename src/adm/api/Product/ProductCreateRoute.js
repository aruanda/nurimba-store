'use strict';

var ProductService = require('./ProductService');

function ProductCreateRoute() {
  var route = this;

  route.verb = 'POST';
  route.url  = '/api/products';

  route.action = function(req, res, next) {
    var productArgs = req.body;
    var productService = new ProductService(req.database);

    productService.create(productArgs).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = ProductCreateRoute;
