'use strict';

var nurimbaCreate = require('../../lib/NurimbaCreate');
var nurimbaUpdate = require('../../lib/NurimbaUpdate');
var nurimbaSearch = require('../../lib/NurimbaSearch');
var productFields = require('./ProductFields')();

function ProductService(connection) {
  var service = this;
  var productTable = 'products';

  service.create = nurimbaCreate(connection, productTable, productFields);
  service.update = nurimbaUpdate(connection, productTable, productFields);
  service.search = nurimbaSearch(connection, productTable, productFields);
}

module.exports = ProductService;
