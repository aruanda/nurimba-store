'use strict';

var CategoryService = require('./CategoryService');

function CategoryUpdateRoute() {
  var route = this;

  route.verb = 'PUT';
  route.url  = '/api/categories';

  route.action = function(req, res, next) {
    var categoryArgs = req.body;
    var categoryService = new CategoryService(req.database);

    categoryService.update(categoryArgs).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = CategoryUpdateRoute;
