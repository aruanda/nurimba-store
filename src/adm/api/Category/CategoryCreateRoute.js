'use strict';

var CategoryService = require('./CategoryService');

function CategoryCreateRoute() {
  var route = this;

  route.verb = 'POST';
  route.url  = '/api/categories';

  route.action = function(req, res, next) {
    var categoryArgs = req.body;
    var categoryService = new CategoryService(req.database);

    categoryService.create(categoryArgs).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = CategoryCreateRoute;
