'use strict';

var nurimbaCreate  = require('../../lib/NurimbaCreate');
var nurimbaUpdate  = require('../../lib/NurimbaUpdate');
var nurimbaSearch  = require('../../lib/NurimbaSearch');
var categoryFields = require('./CategoryFields')();

function CategoryService(connection) {
  var service = this;
  var categoryTable = 'categories';

  service.create = nurimbaCreate(connection, categoryTable, categoryFields);
  service.update = nurimbaUpdate(connection, categoryTable, categoryFields);
  service.search = nurimbaSearch(connection, categoryTable, categoryFields);
}

module.exports = CategoryService;
