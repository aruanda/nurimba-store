'use strict';

var ValidationRequired = require('../../lib/validations/ValidationRequired');

module.exports = function() {
  return [
    {
      attr: 'id',
      kind: 'primary',

      viewForm: true,
      orderForm: 1,

      viewList: true,
      orderList: 1,

      viewPreview: true,
      orderPreview: 1,

      viewFilter: true,
      orderFilter: 1,

      colName: 'firstCol',

      validations: []
    },

    {
      attr: 'catname',
      kind: 'title',

      viewForm: true,
      orderForm: 10,

      pathSlug: 'categorias/',

      viewList: true,
      orderList: 10,

      viewPreview: true,
      orderPreview: 10,

      viewFilter: false,
      orderFilter: 10,

      colName: 'firstCol',

      validations: [
        new ValidationRequired()
      ]
    },

    {
      attr: 'catdesc',
      kind: 'text',

      viewForm: true,
      orderForm: 40,

      viewList: false,
      orderList: 40,

      viewPreview: true,
      orderPreview: 40,

      viewFilter: false,
      orderFilter: 40,

      colName: 'firstCol',

      validations: []
    },

    {
      attr: 'catacervo',
      kind: 'images',

      viewForm: true,
      orderForm: 45,

      viewList: false,
      orderList: 45,

      viewPreview: true,
      orderPreview: 45,

      viewFilter: false,
      orderFilter: 45,

      colName: 'firstCol',

      validations: []
    },

    {
      attr: 'catseo',
      kind: 'seo',

      viewForm: true,
      orderForm: 50,

      viewList: false,
      orderList: 50,

      viewPreview: true,
      orderPreview: 50,

      viewFilter: false,
      orderFilter: 50,

      colName: 'firstCol',

      validations: []
    },

    {
      attr: 'catimagemain',
      kind: 'imageMain',

      viewForm: true,
      orderForm: 70,

      viewList: false,
      orderList: 70,

      viewPreview: true,
      orderPreview: 70,

      viewFilter: false,
      orderFilter: 70,

      colName: 'secondCol',

      validations: []
    },

    {
      attr: 'catparent',
      kind: 'suggestSingle',

      suggest: {
        domain: 'categories',
        label: 'catname'
      },

      viewForm: true,
      orderForm: 90,

      viewList: true,
      orderList: 90,

      viewPreview: true,
      orderPreview: 90,

      viewFilter: false,
      orderFilter: 90,

      colName: 'secondCol',

      validations: []
    },

    {
      attr: 'catpublication',
      kind: 'publication',

      publication: {
        fieldStatus: 'catstatus',
        fieldDateTime: 'catlaunch',
        optionsStatus: [
          'offline',
          'rascunho',
          'online'
        ]
      },

      viewForm: true,
      orderForm: 95,

      viewList: false,
      orderList: 95,

      viewPreview: true,
      orderPreview: 95,

      viewFilter: false,
      orderFilter: 95,

      colName: 'secondCol',

      validations: []
    },

    {
      attr: 'catstatus',
      kind: 'hidden',

      viewForm: true,
      orderForm: 80,

      viewList: true,
      orderList: 80,

      viewPreview: true,
      orderPreview: 80,

      viewFilter: false,
      orderFilter: 80,

      colName: 'secondCol',

      validations: []
    },

    {
      attr: 'catlaunch',
      kind: 'hidden',

      viewForm: true,
      orderForm: 80,

      viewList: true,
      orderList: 80,

      viewPreview: true,
      orderPreview: 80,

      viewFilter: false,
      orderFilter: 80,

      colName: 'secondCol',

      validations: []
    }
  ];
};
