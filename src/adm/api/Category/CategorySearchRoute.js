'use strict';

var CategoryService = require('./CategoryService');

function CategorySearchRoute() {
  var route = this;

  route.verb = 'GET';
  route.url  = '/api/categories';

  route.action = function(req, res, next) {
    var params = {};
    var withParams = Boolean(req && req.query && req.query.q);
    if (withParams) params = JSON.parse(req.query.q);

    var categoryService = new CategoryService(req.database);
    categoryService.search(params).then(function(resultSearch) {
      res.json(resultSearch);
    }).catch(function(err) {
      res.status(422).json(err.message);
    }).then(next);
  };
}

module.exports = CategorySearchRoute;
