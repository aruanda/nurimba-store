'use strict';

var virtualFields = [
  'group',
  'images',
  'publication',
  'suggestMultiple'
];

var isVirtualField = function(kind) {
  return Boolean(virtualFields.indexOf(kind) > -1);
};

module.exports = isVirtualField;
