'use strict';

var types = {
  id:        'SERIAL PRIMARY KEY',
  string:    'VARCHAR(255)',
  text:      'TEXT',
  int:       'INTEGER',
  money:     'DECIMAL(15, 2)',
  float:     'DECIMAL(18, 6)',
  bool:      'BOOLEAN',
  date:      'date',
  time:      'time',
  timestamp: 'timestamp',
  json:      'json'
};

var verifyExist = function() {
  this.verifyTable = true;
  return this;
};

var addTable = function(tableName) {
  this.tableName = tableName;
  return this;
};

var setDistinct = function(distinct) {
  this.useDistinct = Boolean(distinct);
  return this;
};

var addJoins = function(joins) {
  this.queryJoins = joins || [];
  return this;
};

var addReturning = function(fieldsReturning) {
  this.fieldsReturning = fieldsReturning;
  return this;
};

var addOrderBy = function(orderBy) {
  this.fieldsOrder = orderBy;
  return this;
};

var addGroupBy = function(groupBy) {
  this.fieldsGroup = groupBy;
  return this;
};

var addLimit = function(limit) {
  if (limit) this.limitRows = limit;
  return this;
};

var addField = function(name, type, required, unique) {
  if (!unique)   unique   = '';
  if (!required) required = '';

  if (unique   === true) unique   = ' UNIQUE';
  if (required === true) required = ' NOT NULL';
  if (!this.hasOwnProperty('fields') || !this.fields) this.fields = [];

  var typing = type ? types[String(type).toLowerCase()].concat(unique, required) : '';
  this.fields.push(String(name).concat(typing ? ' ' + typing : ''));
  return this;
};

var getSql = function() {
  var template = this.template;

  if (this.hasOwnProperty('tableName')) template = template.replace(/\{\{TABLE_NAME\}\}/g,   this.tableName);
  if (this.hasOwnProperty('fields'))    template = template.replace(/\{\{TABLE_FIELDS\}\}/g, this.fields.join(',\r\n      '));

  var verifyTable = this.hasOwnProperty('verifyTable') && this.verifyTable;
  template = template.replace(/\{\{IF_NOT_EXIST\}\}/g, (verifyTable ? 'IF NOT EXISTS' : ''));
  template = template.replace(/\{\{IF_EXIST\}\}/g,     (verifyTable ? 'IF EXISTS'     : ''));

  var isJoins    = this.hasOwnProperty('queryJoins')      && this.queryJoins      && this.queryJoins.length;
  var isWhere    = this.hasOwnProperty('whereConditions') && this.whereConditions && this.whereConditions.length;
  var isOrder    = this.hasOwnProperty('fieldsOrder')     && this.fieldsOrder     && this.fieldsOrder.length;
  var isGroup    = this.hasOwnProperty('fieldsGroup')     && this.fieldsGroup     && this.fieldsGroup.length;
  var isLimit    = this.hasOwnProperty('limitRows')       && this.limitRows;
  var isUpdate   = this.hasOwnProperty('updateFields')    && this.updateFields    && this.updateFields.length;
  var isReturn   = this.hasOwnProperty('fieldsReturning') && this.fieldsReturning && this.fieldsReturning.length;
  var isDistinct = this.hasOwnProperty('useDistinct')     && this.useDistinct;

  template = template.replace(/\{\{DISTINCT\}\}/g,  (isDistinct ? 'DISTINCT'                                                         : ''));
  template = template.replace(/\{\{JOINS\}\}/g,     (isJoins    ? this.queryJoins.join('\r\n            ')                           : ''));
  template = template.replace(/\{\{WHERE\}\}/g,     (isWhere    ? 'WHERE '.concat(this.whereConditions.join(' AND\r\n            ')) : ''));
  template = template.replace(/\{\{ORDER_BY\}\}/g,  (isOrder    ? 'ORDER BY '.concat(this.fieldsOrder.join(', '))                    : ''));
  template = template.replace(/\{\{GROUP_BY\}\}/g,  (isGroup    ? 'GROUP BY '.concat(this.fieldsGroup.join(', '))                    : ''));
  template = template.replace(/\{\{LIMIT\}\}/g,     (isLimit    ? 'LIMIT '.concat(this.limitRows)                                    : ''));
  template = template.replace(/\{\{UPDATE\}\}/g,    (isUpdate   ? this.updateFields.join(', ')                                       : ''));
  template = template.replace(/\{\{RETURNING\}\}/g, (isReturn   ? 'RETURNING '.concat(this.fieldsReturning.join(', '))               : ''));

  template = template.replace(/\{\{VALUES\}\}/g,    (this.values || []).map(function(value) {
    if (value === undefined) return 'null';
    if ((typeof value) === 'number') return value;
    return '\'' + String(value) + '\'';
  }).join(', '));

  var sql = template.replace(/\ \ /g, ' ');
  return sql;
};

function CreateTable(tableName, verifyTable) {
  var builder = this;

  builder.table = addTable;
  builder.field = addField;
  builder.build = getSql;
  builder.verifyExist = verifyExist;

  builder.verifyTable = Boolean(verifyTable);
  builder.tableName   = tableName || '';
  builder.template    = '\
    CREATE TABLE {{IF_NOT_EXIST}} {{TABLE_NAME}} ( \
      {{TABLE_FIELDS}} \
    ); \
  ';
}

function DropTable(tableName) {
  var builder = this;

  builder.table = addTable;
  builder.build = getSql;

  builder.tableName = tableName || '';
  builder.template  = 'DROP TABLE {{IF_EXIST}} {{TABLE_NAME}};';
}

var setConditions = function(conditions) {
  var builder = this;
  builder.whereConditions = [];

  var isArray = Array.isArray(conditions);
  if (isArray) {
    conditions.forEach(function(cond) {
      var comparator = ' ' + (String(cond.comparator) || '=') + ' ';
      var value = cond.comparator === 'like' ? ('%' + String(cond.value) + '%') : cond.value;

      var isArray = Array.isArray(value);
      var whereIn = (['in', 'not in'].indexOf(comparator.toLowerCase().trim()) > -1) && isArray;

      if (whereIn) {
        builder.whereConditions.push(String(cond.field).concat(comparator, '(', value.join(','), ')'));
      } else {
        var isString = !((typeof value) === 'number');
        if (isString) value = '\'' + String(value) + '\'';
        builder.whereConditions.push(String(cond.field).concat(comparator, value));
      }
    });
  } else {
    var isObject = (typeof conditions) === 'object';
    if (isObject) {
      Object.keys(conditions).forEach(function(field) {
        var value = conditions[field];
        var isString = !((typeof value) === 'number');
        if (isString) value = '\'' + String(value) + '\'';
        builder.whereConditions.push(String(field).concat(' = ', value));
      });
    }
  }

  return builder;
};

function SelectTable(fields) {
  var builder = this;

  builder.limitRows   = '';
  builder.fieldsOrder = [];
  builder.fieldsWhere = [];

  builder.from       = addTable;
  builder.distinct   = setDistinct;
  builder.build      = getSql;
  builder.limit      = addLimit;
  builder.fields     = fields || [];
  builder.orderBy    = addOrderBy;
  builder.groupBy    = addGroupBy;
  builder.joins      = addJoins;
  builder.conditions = setConditions;
  builder.template   = '\
    SELECT {{DISTINCT}} {{TABLE_FIELDS}} \
    FROM {{TABLE_NAME}} \
    {{JOINS}} \
    {{WHERE}} \
    {{GROUP_BY}} \
    {{ORDER_BY}} \
    {{LIMIT}} \
  ';
}

function InsertTable(attrs) {
  var builder = this;
  builder.fields = [];
  builder.values = [];

  builder.into = addTable;
  builder.build = getSql;
  builder.addField = addField;
  builder.returning = addReturning;

  Object.keys(attrs).forEach(function(field) {
    builder.addField(field);
    builder.values.push(attrs[field]);
  });

  builder.template = '\
    INSERT INTO {{TABLE_NAME}} \
     ({{TABLE_FIELDS}}) \
     VALUES ({{VALUES}}) \
     {{RETURNING}} \
  ';
}

function UpdateTable(tableName, attrs, conditions) {
  var builder = this;

  builder.tableName    = tableName || '';
  builder.updateFields = [];
  builder.conditions   = setConditions;

  Object.keys(attrs).forEach(function(field) {
    var value = attrs[field];

    var isNull   = value === null;
    var isString = !isNull && !((typeof value) === 'number');

    if (isNull)   value = 'null';
    if (isString) value = '\'' + String(value) + '\'';

    builder.updateFields.push(String(field).concat(' = ', value));
  });

  builder.conditions(conditions);

  builder.build = getSql;
  builder.returning = addReturning;

  builder.template  = '\
    UPDATE {{TABLE_NAME}} \
    SET {{UPDATE}} \
    {{WHERE}} \
    {{RETURNING}} \
  ';
}

function DeleteTable(conditions) {
  var builder = this;
  builder.whereConditions = [];

  builder.from = addTable;
  builder.build = getSql;
  builder.addField = addField;

  Object.keys(conditions).forEach(function(field) {
    var value = conditions[field];
    var isString = !((typeof value) === 'number');
    if (isString) value = '\'' + String(value) + '\'';
    builder.whereConditions.push(String(field).concat(' = ', value));
  });

  builder.template = '\
    DELETE FROM {{TABLE_NAME}} \
     {{WHERE}} \
  ';
}

module.exports = {
  createTable: function(tableName, ifTableNotExist) {
    return new CreateTable(tableName, ifTableNotExist);
  },

  dropTable: function(tableName) {
    return new DropTable(tableName);
  },

  select: function(fields) {
    return new SelectTable(fields);
  },

  insert: function(values) {
    return new InsertTable(values);
  },

  update: function(tableName, attrs, conditions) {
    return new UpdateTable(tableName, attrs, conditions);
  },

  delete: function(conditions) {
    return new DeleteTable(conditions);
  }
};
