'use strict';

var q = require('q');
var builder = require('./QueryBuilder');
var isVirtualField = require('./NurimbaVirtualField');

function nurimbaSearch(connection, tableName, fields) {
  var tableFields = fields.filter(function(field) {
    return !isVirtualField(field.kind);
  }).map(function(field) {
    return tableName.concat('.', field.attr);
  });

  var singleSuggests = fields.filter(function(field) {
    var isSuggestSingle = field.kind === 'suggestSingle';
    return isSuggestSingle;
  });

  var multipleSuggests = fields.filter(function(field) {
    var isSuggestMultiple = field.kind === 'suggestMultiple';
    return isSuggestMultiple;
  });

  var imagesFields = fields.filter(function(field) {
    var isImages = field.kind === 'images';
    return isImages;
  });

  return function(params) {
    if (!params) params = {};
    if (!params.distinct) params.distinct = false;

    if (!Array.isArray(params.joins))   params.joins   = [];
    if (!Array.isArray(params.sortBy))  params.sortBy  = [];
    if (!Array.isArray(params.groupBy)) params.groupBy = [];

    var selCount = (params.distinct || params.groupBy.length)
      ? 'COUNT(DISTINCT ' + tableName + '.id) as tot'
      : 'COUNT(-1) as tot';

    var sqlCount = builder
      .select([selCount])
      .from(tableName)
      .joins(params.joins)
      .conditions(params.conditions)
      .build();


    return connection.runScript(sqlCount).then(function(res) {
      var resultSet = {
        rows: 0,
        data: []
      };

      var tot = res.rows && res.rows.length ? parseInt(res.rows.pop().tot) : 0;
      if (!tot) return resultSet;

      var sqlSearch = builder
        .select(tableFields)
        .distinct(params.distinct)
        .from(tableName)
        .joins(params.joins)
        .conditions(params.conditions)
        .limit(params.limit)
        .groupBy(params.groupBy)
        .orderBy(params.sortBy)
        .build();

      var loadSuggest = function(res) {
        return singleSuggests.map(function(field) {
          var loadSingleSuggest = res.rows.map(function(row) {
            var codeSuggest = row[field.attr];
            if (!codeSuggest) return;

            var labelSql = builder
              .select(['*'])
              .from(field.suggest.domain)
              .conditions({ id: codeSuggest })
              .build();

            return connection.runScript(labelSql).then(function(resLabel) {
              if (!resLabel.rows) return;
              if (!resLabel.rows.length) return;

              var suggest = resLabel.rows[0];
              suggest.value = suggest.id;
              suggest.label = suggest[field.suggest.label];

              row[field.attr] = suggest;
            });
          });

          return q.all(loadSingleSuggest);
        });
      };

      var loadSuggests = function(res) {
        return multipleSuggests.map(function(field) {
          var fieldDomain   = field.suggest.relDomain;
          var fieldSuggest  = field.suggest.relSuggest;
          var tableRelation = field.suggest.relation;

          var loadMultipleSuggest = res.rows.map(function(row) {
            var condition = {};
            condition[fieldDomain] = row.id;

            var suggestSqlBuilder = builder
              .select([fieldSuggest])
              .from(tableRelation)
              .conditions(condition);

            var sqlMultipleSuggest = suggestSqlBuilder.build();

            return connection.runScript(sqlMultipleSuggest).then(function(resSuggest) {
              if (!resSuggest.rows) return;
              if (!resSuggest.rows.length) return;

              var newValue = [];
              var load = resSuggest.rows.map(function(suggest) {
                var codeSuggest = suggest[fieldSuggest];

                var labelSql = builder
                  .select(['*'])
                  .from(field.suggest.domain)
                  .conditions({ id: codeSuggest })
                  .build();

                return connection.runScript(labelSql).then(function(resLabel) {
                  if (!resLabel.rows) return;
                  if (!resLabel.rows.length) return;

                  var suggest = resLabel.rows[0];
                  suggest.value = suggest.id;
                  suggest.label = suggest[field.suggest.label];

                  newValue.push(suggest);
                });
              });

              return q.all(load).then(function() {
                row[field.attr] = newValue;
              });
            });
          });

          return q.all(loadMultipleSuggest);
        });
      };

      var loadImages = function(res) {
        return imagesFields.map(function(field) {
          var tag = field.attr;
          var loadAllImages = res.rows.map(function(row) {
            var condition = {};
            condition['filetag']   = tag;
            condition['filetagid'] = row.id;

            var imagesSqlBuilder = builder
              .select(['id', 'filecreated', 'fileupdated', 'filetag', 'filesrc', 'filename', 'filetype', 'filesize', 'filetitle', 'filetagid'])
              .from('images')
              .conditions(condition);

            var sqlImage = imagesSqlBuilder.build();
            return connection.runScript(sqlImage).then(function(resImages) {
              row[field.attr] = resImages.rows || [];
            });
          });

          return q.all(loadAllImages);
        });
      };

      return connection.runScript(sqlSearch).then(function(res) {
        var mImages   = loadImages(res);
        var sSuggest  = loadSuggest(res);
        var mSuggests = loadSuggests(res);
        var mLoads    = mImages.concat(sSuggest, mSuggests);

        return q.all(mLoads).then(function() {
          resultSet.rows = tot;
          resultSet.data = res.rows;
          return resultSet;
        });
      });
    });
  };
}

module.exports = nurimbaSearch;
