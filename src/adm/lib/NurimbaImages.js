'use strict';

var q = require('q');
var fs = require('fs');
var moment = require('moment');
var builder = require('./QueryBuilder');
var listFieldsFile = ['id', 'filecreated', 'fileupdated', 'filetag', 'filesrc', 'filename', 'filetype', 'filesize', 'filetitle', 'filetagid'];

function nurimbaImages(connection, imagesFields, rowObj, files, args) {
  var updateFile = function(fileObj) {
    var editArgs = {
      filetag:     fileObj.filetag,
      filetitle:   fileObj.filetitle,
      filetagid:   rowObj.id,
      fileupdated: moment().format('YYYY-MM-DD HH:mm:ss')
    };

    var whereById = { id: fileObj.id };

    var sqlUpdate = builder
      .update('images', editArgs, whereById)
      .returning(listFieldsFile)
      .build();

    return connection.runScript(sqlUpdate).then(function(res) {
      var notReturning = !res.rows || !res.rows.length;
      if (notReturning) throw new Error('Not Returning data');
      return res.rows.pop();
    });
  };

  var insertFile = function(fileObj) {
    var newArgs = {
      filetag:     fileObj.filetag,
      filesrc:     fileObj.filesrc,
      filename:    fileObj.filename,
      filetype:    fileObj.filetype,
      filesize:    fileObj.filesize,
      filetitle:   fileObj.filetitle,
      filetagid:   fileObj.filetagid,
      filecreated: moment().format('YYYY-MM-DD HH:mm:ss')
    };

    var sqlInsert = builder
      .insert(newArgs)
      .into('images')
      .returning(listFieldsFile)
      .build();

    return connection.runScript(sqlInsert).then(function(res) {
      var notReturning = !res.rows || !res.rows.length;
      if (notReturning) throw new Error('Not Returning data');
      return res.rows.pop();
    });
  };

  var deleteFile = function(fileObj) {
    var whereById = { id: fileObj.id };

    var sqlDelete = builder
      .delete(whereById)
      .from('images')
      .build();

    return connection.runScript(sqlDelete).then(function() {
      fs.unlinkSync(fileObj.filesrc.concat(fileObj.filename));
    });
  };

  var references = imagesFields.map(function(field) {
    var fieldValue = rowObj[field.attr];

    if (!fieldValue && args) fieldValue = args[field.attr];

    if (fieldValue) {
      var isNotArray = !Array.isArray(fieldValue);
      if (isNotArray) fieldValue = JSON.parse(rowObj[field.attr]);
    }

    if (!fieldValue) fieldValue = [];

    var withFiles = files && files.length;
    if (withFiles) {
      files.forEach(function(newFile) {
        fieldValue.push({
          filetag:   field.attr,
          filesrc:   newFile.destination,
          filename:  newFile.filename,
          filetype:  newFile.mimetype,
          filesize:  newFile.size,
          filetitle: newFile.fieldname,
          filetagid: rowObj.id
        });
      });
    }

    var actions = fieldValue.map(function(fv, key) {
      var isRemove = Boolean(fv.hasOwnProperty('id') && fv.id && fv.hasOwnProperty('removed') && fv.removed);
      if (isRemove) return deleteFile(fv);

      var isUpdate = Boolean(fv.hasOwnProperty('id') && fv.id);
      if (isUpdate) return updateFile(fv).then(function(fileUpdated) {
        fieldValue[key] = fileUpdated;
      });

      return insertFile(fv).then(function(fileInserted) {
        fieldValue[key] = fileInserted;
      });
    });

    return q.all(actions).then(function() {
      rowObj[field.attr] = fieldValue;
    });
  });

  return q.all(references).then(function() {
    return rowObj;
  });
}

module.exports = nurimbaImages;
