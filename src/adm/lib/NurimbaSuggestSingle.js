'use strict';

function NurimbaSuggestSingle(args, suggests, rowObj) {
  if (!args) return;

  suggests.forEach(function(field) {
    var valObj = args[field.attr];
    var suggestNotFilled = Boolean(!valObj);
    if (suggestNotFilled) return;
    rowObj[field.attr] = valObj.value;
  });
}

module.exports = NurimbaSuggestSingle;
