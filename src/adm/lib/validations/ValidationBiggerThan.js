'use strict';

var promise = require('q').Promise;

function ValidationBiggerThan(maxNumber) {
  var validation = this;
  var errorRef = 'msgErrorBiggerThan';

  validation.envs = ['front', 'back'];

  validation.verify = function(value) {
    return promise(function(resolve, reject) {
      var baseDecimal = 10;
      if (value === undefined) return resolve();
      var isNotNumber = isNaN(parseFloat(value, baseDecimal));
      if (isNotNumber) return resolve();
      value = parseFloat(value, baseDecimal);

      var isBiggerThan = value > maxNumber;

      isBiggerThan
        ? reject({ message: errorRef.translate(maxNumber), ref: errorRef })
        : resolve();
    });
  };
}

module.exports = ValidationBiggerThan;
