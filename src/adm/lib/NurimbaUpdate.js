'use strict';

var builder = require('./QueryBuilder');
var nurimbaSuggestMultiple = require('./NurimbaSuggestMultiple');
var nurimbaImages = require('./NurimbaImages');
var isVirtualField = require('./NurimbaVirtualField');

function nurimbaUpdate(connection, tableName, fields) {
  var tableFields = fields.filter(function(field) {
    return !isVirtualField(field.kind);
  }).map(function(field) {
    return field.attr;
  });

  var singleSuggests = fields.filter(function(field) {
    var isSuggestSingle = field.kind === 'suggestSingle';
    return isSuggestSingle;
  });

  var multipleSuggests = fields.filter(function(field) {
    var isSuggestMultiple = field.kind === 'suggestMultiple';
    return isSuggestMultiple;
  });

  var imagesFields = fields.filter(function(field) {
    var isImages = field.kind === 'images';
    return isImages;
  });

  var imagesMainFields = fields.filter(function(field) {
    var isImageMain = field.kind === 'imageMain';
    return isImageMain;
  });

  var seoFields = fields.filter(function(field) {
    var isSEO = field.kind === 'seo';
    return isSEO;
  });

  return function(args, files) {
    var withId = args && args.hasOwnProperty('id') && args.id;
    var updateId = withId ? args.id : 0;
    var whereById = { id: updateId };
    if (withId) delete args.id;
    var listFields = Object.keys(args);
    var newArgs = {};

    listFields.filter(function(field) {
      var existsField = tableFields.indexOf(field) > -1;
      var idFilled = field !== 'id' || Boolean(args[field]);
      return existsField && idFilled;
    }).forEach(function(field) {
      newArgs[field] = args[field];
    });

    seoFields.forEach(function(field) {
      var seoObj = {};
      var withField = newArgs && newArgs.hasOwnProperty(field.attr) && newArgs[field.attr] && (typeof newArgs[field.attr]) === 'object';
      if (withField) seoObj = newArgs[field.attr];
      newArgs[field.attr] = JSON.stringify(seoObj);
    });

    imagesMainFields.forEach(function(field) {
      var imageMain = {};
      var withField = newArgs && newArgs.hasOwnProperty(field.attr) && newArgs[field.attr] && (typeof newArgs[field.attr]) === 'object';
      if (withField) imageMain = newArgs[field.attr];
      newArgs[field.attr] = JSON.stringify(imageMain);
    });

    singleSuggests.forEach(function(field) {
      var withField = Boolean(args[field.attr]);
      if (withField) newArgs[field.attr] = args[field.attr].value;
    });

    var sqlUpdate = builder
      .update(tableName, newArgs, whereById)
      .returning(tableFields)
      .build();

    return connection.runScript(sqlUpdate).then(function(res) {
      var rowObj = res.rows.pop();

      singleSuggests.forEach(function(field) {
        var withField = Boolean(args[field.attr]);
        if (withField) rowObj[field.attr] = args[field.attr];
      });

      return nurimbaSuggestMultiple(connection, args, multipleSuggests, rowObj).then(function() {
        return nurimbaImages(connection, imagesFields, rowObj, files, args);
      });
    });
  };
}

module.exports = nurimbaUpdate;
