'use strict';

var q = require('q');
var builder = require('./QueryBuilder');

function NurimbaSuggestMultiple(connection, args, multipleSuggests, rowObj) {
  var references = multipleSuggests.map(function(field) {
    var val = args[field.attr];
    var suggestNotFilled = Boolean(!args || val === null || val === undefined);
    if (suggestNotFilled) return;

    var fieldValue = args[field.attr];

    var fieldDomain   = field.suggest.relDomain;
    var fieldSuggest  = field.suggest.relSuggest;
    var tableRelation = field.suggest.relation;

    var deleteSuggestById = {};
    deleteSuggestById[fieldDomain] = rowObj.id;

    var sqlDeleteSuggest = builder
      .delete(deleteSuggestById)
      .from(tableRelation)
      .build();

    return connection.runScript(sqlDeleteSuggest).then(function() {
      var insertsSuggests = fieldValue.map(function(suggest) {
        var suggestObj = {};
        suggestObj[fieldDomain]  = rowObj.id;
        suggestObj[fieldSuggest] = suggest.value;

        var sqlInsertSuggest = builder
          .insert(suggestObj)
          .into(tableRelation)
          .build();

        return connection.runScript(sqlInsertSuggest);
      });

      return q.all(insertsSuggests).then(function() {
        rowObj[field.attr] = args[field.attr];
      });
    });
  });

  return q.all(references).then(function() {
    return rowObj;
  });
}

module.exports = NurimbaSuggestMultiple;
