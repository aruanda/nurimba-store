/*globals $:false*/
'use strict';

var React    = require('react');
var ReactDOM = require('react-dom');

var Remember = React.createClass({
  componentDidMount: function() {
    var loginApp = ReactDOM.findDOMNode(this);
    $(loginApp).attr('class', 'login-box');
    $('body').attr('class', 'hold-transition login-page');

    setTimeout(function() {
      $.AdminLTE.layout.activate();
    });
  },

  render: function() {
    return(
      <div className="login-box">
        <div className="login-logo">
          <a href="../../index2.html"><b>nbA</b>dmin</a>
        </div>

        <div className="login-box-body">
          <p className="login-box-msg">Sign in to start your session</p>

          <form action={'/login'.translate()} method="get">
            <div className="form-group has-feedback">
              <input type="email" className="form-control" placeholder="Email" />
              <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div className="row">
              <div className="col-xs-4">
                <button type="submit" className="btn btn-primary btn-block btn-flat">Remember</button>
              </div>
            </div>
          </form>

          <a href={'/login'.translate()}>I already have a membership</a><br />
        </div>
      </div>
    );
  }
});

module.exports = Remember;
