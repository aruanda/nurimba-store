'use strict';

var RuleAuth = require('./RuleAuth');
var ruleAuth = new RuleAuth();

function RequireAuth(nextState, stateReplace, callback) {
  var isAuthenticated = ruleAuth.isAuthenticated();
  if (!isAuthenticated) stateReplace('/'.translate() + '/login'.translate());
  callback();
}

module.exports = RequireAuth;
