'use strict';

var localStorage     = require('local-storage');
var nurimbaAdminKey  = 'nurimbaAdmin';
var keyAuthenticated = nurimbaAdminKey.concat('Authenticated');

function RuleAuth() {
  var ruleAuth = this;

  ruleAuth.isAuthenticated = function() {
    return Boolean(localStorage(keyAuthenticated));
  };

  ruleAuth.setAuthenticated = function(authenticated) {
    var isAuthenticated = Boolean(authenticated);
    localStorage.set(keyAuthenticated, isAuthenticated);
  };
}

module.exports = RuleAuth;
