/*globals $:false*/
'use strict';

var React     = require('react');
var ReactDOM  = require('react-dom');
var RuleAuth  = require('./RuleAuth');
var ruleAuth  = new RuleAuth();
var nbHistory = require('react-router').browserHistory;

var Login = React.createClass({
  componentDidMount: function() {
    var loginApp = ReactDOM.findDOMNode(this);
    $(loginApp).attr('class', 'login-box');
    $('body').attr('class', 'hold-transition login-page');

    setTimeout(function() {
      $.AdminLTE.layout.activate();
      $(loginApp).find('input[type="checkbox"]').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
      });
    });
  },

  componentWillUnmount: function() {
    var loginApp = ReactDOM.findDOMNode(this);

    $(loginApp)
      .find('input[type="checkbox"]')
      .iCheck('destroy');
  },

  handleTryAuth: function() {
    var isAuthenticated = true;
    ruleAuth.setAuthenticated(isAuthenticated);
    nbHistory.push('/admin');
  },

  render: function() {
    return(
      <div className="login-box">
        <div className="login-logo">
          <a href="../../index2.html"><b>nb</b>Admin</a>
        </div>

        <div className="login-box-body">
          <p className="login-box-msg"><b>Clique no botão entrar.</b><br/><small>Não é necessário ter cadastro para ver o painel.</small></p>

          <form>
            <div className="form-group has-feedback">
              <input type="email" className="form-control" placeholder="contato@nurimba.com" />
              <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>

            <div className="form-group has-feedback">
              <input type="password" className="form-control" placeholder="••••••••" />
              <span className="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div className="row">
              <div className="col-xs-8"></div>

              <div className="col-xs-4">
                <button type="button" onClick={this.handleTryAuth} className="btn btn-primary btn-block btn-flat">Entrar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
});

module.exports = Login;
