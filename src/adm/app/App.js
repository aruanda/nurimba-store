/*globals $:false*/
'use strict';

var React       = require('react');
var DashHeader  = require('./dash/DashHeader');
var DashSidebar = require('./dash/DashSidebar');
var DashFooter  = require('./dash/DashFooter');

var App = React.createClass({
  componentActive: function() {
    $.AdminLTE.layout.activate();
  },

  componentDidMount: function() {
    $('body').attr('class', 'hold-transition skin-blue sidebar-mini wysihtml5-supported');
    this.componentActive();
  },

  componentDidUpdate: function() {
    this.componentActive();
  },

  render: function() {
    return(
      <div className="wrapper">
        <DashHeader />
        <DashSidebar />
        {this.props.children}
        <DashFooter />
      </div>
    );
  }
});

module.exports = App;
