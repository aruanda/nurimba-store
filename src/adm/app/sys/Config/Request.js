'use strict';

var domain = require('./domain');
var nbConnection = require('nb-connection');

module.exports = {
  getInstance: function() {
    var backend = nbConnection.getInstance();
    return backend.all(domain);
  }
};
