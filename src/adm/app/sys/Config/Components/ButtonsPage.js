'use strict';

var domain    = require('../domain');
var links     = require('../Links');
var nbHistory = require('react-router').browserHistory;

module.exports = {
  getListButton: function(isPrimary) {
    return {
      text: domain.concat('.btn.view.list'),
      className: 'btn btn-block ' + (isPrimary ? 'btn-primary' : 'btn-default') + ' btn-sm',
      type: 'button',

      onClick: function() {
        nbHistory.push(links.list);
      }
    };
  },

  getEditButton: function(isPrimary) {
    return {
      text: domain.concat('.btn.view.edit'),
      className: 'btn btn-block ' + (isPrimary ? 'btn-primary' : 'btn-default') + ' btn-sm',
      type: 'button',

      viewOnly: function(model) {
        return Boolean(model && model.hasOwnProperty('id') && model.id);
      },

      onClick: function(model) {
        nbHistory.push(links.editById(model.id));
      }
    };
  }
};
