'use strict';

var React       = require('react');
var domain      = require('../domain');
var links       = require('../Links');
var fields      = require('../Fields')('viewForm');
var formStore   = require('../Store/FormStore');
var formActions = require('../Actions/FormActions');
var nbFlow      = require('nb-flow');
var pageBtns    = require('./ButtonsPage');

var buttons = [
  pageBtns.getListButton()
];

var nbFormPage = nbFlow.nbFormPage(domain, fields, formStore, formActions, links, buttons);

module.exports = React.createClass(nbFormPage);
