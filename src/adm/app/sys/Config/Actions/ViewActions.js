'use strict';

var Reflux = require('reflux');
var nbFlow = require('nb-flow');
var nbViewActions = nbFlow.nbViewActions;

module.exports = Reflux.createActions(nbViewActions);
