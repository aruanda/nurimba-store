'use strict';

var domain = require('./domain');
var nbFlow = require('nb-flow');
var fields = require('db-tag-fields')();

module.exports = nbFlow.nbFields(domain, fields);
