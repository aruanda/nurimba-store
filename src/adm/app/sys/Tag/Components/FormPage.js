'use strict';

var React       = require('react');
var nbFlow      = require('nb-flow');
var domain      = require('../domain');
var links       = require('../Links');
var fields      = require('../Fields')('viewForm');
var formStore   = require('../Store/FormStore');
var formActions = require('../Actions/FormActions');
var pageBtns    = require('./ButtonsPage');
var isPrimary   = true;

var buttons = [
  pageBtns.getListButton(),
  pageBtns.getInsertButton(),
  pageBtns.getPageButton(isPrimary)
];

var nbFormPage = nbFlow.nbFormPage(domain, fields, formStore, formActions, links, buttons);

module.exports = React.createClass(nbFormPage);
