'use strict';

var Reflux      = require('reflux');
var requester   = require('../Request');
var listActions = require('../Actions/ListActions');
var nbFlow      = require('nb-flow');

var nbListStore = nbFlow.nbListStore(listActions, requester);

module.exports = Reflux.createStore(nbListStore);
