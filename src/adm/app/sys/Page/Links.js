'use strict';

var domain  = require('./domain');
var nbLinks = require('nb-flow-links')(domain);

module.exports = nbLinks;
