'use strict';

var Reflux = require('reflux');
var nbFlow = require('nb-flow');
var nbListActions = nbFlow.nbListActions;

module.exports = Reflux.createActions(nbListActions);
