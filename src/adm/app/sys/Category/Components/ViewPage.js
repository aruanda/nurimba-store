'use strict';

var React       = require('react');
var domain      = require('../domain');
var links       = require('../Links');
var views       = require('../Fields')('viewPreview');
var viewStore   = require('../Store/ViewStore');
var viewActions = require('../Actions/ViewActions');
var nbFlow      = require('nb-flow');
var pageBtns    = require('./ButtonsPage');
var isPrimary   = true;

var buttons = [
  pageBtns.getListButton(),
  pageBtns.getInsertButton(),
  pageBtns.getPageButton(),
  pageBtns.getEditButton(isPrimary)
];

var nbViewPage = nbFlow.nbViewPage(domain, views, viewStore, viewActions, links, buttons);

module.exports = React.createClass(nbViewPage);
