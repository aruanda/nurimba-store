'use strict';

var links    = require('./Links');
var FormPage = require('./Components/FormPage');
var ViewPage = require('./Components/ViewPage');
var ListPage = require('./Components/ListPage');

var register = {};
register[links.add]  = FormPage;
register[links.edit] = FormPage;
register[links.view] = ViewPage;
register[links.list] = ListPage;

module.exports = register;
