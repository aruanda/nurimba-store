'use strict';

var Reflux      = require('reflux');
var requester   = require('../Request');
var viewActions = require('../Actions/ViewActions');
var nbFlow      = require('nb-flow');

var nbViewStore = nbFlow.nbViewStore(viewActions, requester);

module.exports = Reflux.createStore(nbViewStore);
