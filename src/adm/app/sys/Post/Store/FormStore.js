'use strict';

var Reflux      = require('reflux');
var requester   = require('../Request');
var formActions = require('../Actions/FormActions');
var nbFlow      = require('nb-flow');

var nbFormStore = nbFlow.nbFormStore(formActions, requester);

module.exports = Reflux.createStore(nbFormStore);
