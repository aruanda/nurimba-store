/*globals window: false*/
'use strict';

var nbSlug    = require('nb-slug');
var domain    = require('../domain');
var links     = require('../Links');
var nbLink    = require('nb-link');
var nbHistory = require('react-router').browserHistory;

module.exports = {
  getListButton: function(isPrimary) {
    return {
      text: domain.concat('.btn.view.list'),
      className: 'btn btn-block ' + (isPrimary ? 'btn-primary' : 'btn-default') + ' btn-sm',
      type: 'button',

      onClick: function() {
        nbHistory.push(links.list);
      }
    };
  },

  getInsertButton: function(isPrimary, ignoreModel) {
    return {
      text: domain.concat('.btn.view.new'),
      className: 'btn btn-block ' + (isPrimary ? 'btn-primary' : 'btn-default') + ' btn-sm',
      type: 'button',

      viewOnly: function(model) {
        return ignoreModel || Boolean(model && model.hasOwnProperty('id') && model.id);
      },

      onClick: function() {
        nbHistory.push(links.add);
      }
    };
  },

  getEditButton: function(isPrimary) {
    return {
      text: domain.concat('.btn.view.edit'),
      className: 'btn btn-block ' + (isPrimary ? 'btn-primary' : 'btn-default') + ' btn-sm',
      type: 'button',

      viewOnly: function(model) {
        return Boolean(model && model.hasOwnProperty('id') && model.id);
      },

      onClick: function(model) {
        nbHistory.push(links.editById(model.id));
      }
    };
  },

  getPageButton: function(isPrimary) {
    return {
      text: domain.concat('.btn.view.page'),
      className: 'btn btn-block ' + (isPrimary ? 'btn-primary' : 'btn-default') + ' btn-sm',
      type: 'button',

      viewOnly: function(model) {
        return Boolean(model && model.hasOwnProperty('id') && model.id);
      },

      onClick: function(model) {
        var pathUrl = 'blog/' + nbSlug(model.posttitle) + '-' + String(model.id);
        var linkUrl = nbLink(pathUrl);
        window.open(linkUrl, '_blank');
      }
    };
  }
};
