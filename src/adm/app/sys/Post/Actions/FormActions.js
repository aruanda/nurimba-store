'use strict';

var Reflux = require('reflux');
var nbFlow = require('nb-flow');
var nbFormActions = nbFlow.nbFormActions;

module.exports = Reflux.createActions(nbFormActions);
