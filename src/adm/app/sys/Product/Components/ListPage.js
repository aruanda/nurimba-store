'use strict';

var React       = require('react');
var domain      = require('../domain');
var links       = require('../Links');
var cols        = require('../Fields')('viewList');
var listStore   = require('../Store/ListStore');
var listActions = require('../Actions/ListActions');
var nbFlow      = require('nb-flow');
var pageBtns    = require('./ButtonsPage');
var isPrimary   = true;
var ignoreModel = true;

var buttons = [
  pageBtns.getInsertButton(isPrimary, ignoreModel)
];

var nbListPage = nbFlow.nbListPage(domain, cols, listStore, listActions, links, buttons);

module.exports = React.createClass(nbListPage);
