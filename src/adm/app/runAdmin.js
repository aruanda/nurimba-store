'use strict';

require('./lib/Translate');
require('./lib/Capitalize');

var React       = require('react');
var ReactDom    = require('react-dom');
var ReactRouter = require('react-router');
var nbHistory   = require('react-router').browserHistory;

var Route      = ReactRouter.Route;
var Router     = ReactRouter.Router;
var IndexRoute = ReactRouter.IndexRoute;

var App         = require('./App');
var Login       = require('./auth/Login');
var Remember    = require('./auth/Remember');
var requireAuth = require('./auth/RequireAuth');
var DashBoard   = require('./dash/DashBoard');

var tagRoutes = require('./sys/Tag/Routes');
var postRoutes = require('./sys/Post/Routes');
var pageRoutes = require('./sys/Page/Routes');
var configRoutes = require('./sys/Config/Routes');
var productRoutes = require('./sys/Product/Routes');
var sessionRoutes = require('./sys/Session/Routes');
var categoryRoutes = require('./sys/Category/Routes');
var baseHref = '/'.translate();

var registerRoutes = function(route) {
  return Object.keys(route).map(function(url, i) {
    return <Route
      key={i}
      path={url}
      component={route[url]}
    />;
  });
};

module.exports = function(domAdmin) {
  ReactDom.render((
    <Router history={nbHistory}>
      <Route path={ baseHref + '/login'.translate() } component={Login} />
      <Route path={ baseHref + '/remember'.translate() } component={Remember} />

      <Route path={ baseHref } component={App} onEnter={requireAuth}>
        <IndexRoute component={DashBoard} />

        { registerRoutes(configRoutes) }

        { registerRoutes(tagRoutes) }
        { registerRoutes(postRoutes) }

        { registerRoutes(pageRoutes) }
        { registerRoutes(sessionRoutes) }

        { registerRoutes(productRoutes) }
        { registerRoutes(categoryRoutes) }
      </Route>
    </Router>
  ), domAdmin);
};
