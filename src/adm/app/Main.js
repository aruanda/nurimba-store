/*globals document:false*/
'use strict';

var runAdmin = require('./runAdmin');
var domAdmin = document.getElementById('nbAdmin');

document.addEventListener('DOMContentLoaded', function() {
  runAdmin(domAdmin);
});
