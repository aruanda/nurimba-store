'use strict';

var React = require('react');

var DashBoard = React.createClass({
  render: function () {
    return this.props.children || (
      <div className="content-wrapper">
        <section className="content-header">
          <h1>
            Dashboard
            <small>Acompanhe seus dados aqui.</small>
          </h1>
        </section>
      </div>
    );
  }
});

module.exports = DashBoard;
