/*globals $:false*/
'use strict';

var React    = require('react');
var Link     = require('react-router').Link;
var nbLink   = require('nb-link');
var ReactDOM = require('react-dom');

var tagLinks      = require('../sys/Tag/Links');
var postLinks     = require('../sys/Post/Links');
var productLinks  = require('../sys/Product/Links');
var categoryLinks = require('../sys/Category/Links');
var configLinks   = require('../sys/Config/Links');

var DashSidebar = React.createClass({
  render: function() {
    var linkBlog = nbLink('blog');
    var linkSite = nbLink('');

    return (
      <aside className="main-sidebar">
        <section className="sidebar">
          <ul className="sidebar-menu">
            <li>
              <Link to={ '/admin' }>
                <i className="fa fa-circle-thin text-aqua"></i>
                Dashboard
              </Link>
            </li>

            <li>
              <a href="#">
                <i className="fa fa-shopping-cart"></i>
                <span>Loja</span>
                <i className="fa fa-angle-left pull-right"></i>
              </a>

              <ul className="treeview-menu displayNone">
                <li>
                  <Link to={productLinks.list}>
                    { 'products.menu'.translate() }
                  </Link>
                </li>

                <li>
                  <Link to={categoryLinks.list}>
                    { 'categories.menu'.translate() }
                  </Link>
                </li>

                <li>
                <a href={linkSite} target="_blank">
                  <small><i className="fa fa-link"></i>&nbsp;&nbsp;</small>
                  <span>{ 'menu.loja.ver'.translate() }</span>
                </a>
                </li>
              </ul>
            </li>

            <li className="treeview">
              <a href="#">
                <i className="fa fa-file-text"></i>
                <span>Blog</span>
                <i className="fa fa-angle-left pull-right"></i>
              </a>

              <ul className="treeview-menu displayNone">
                <li>
                  <Link to={postLinks.list}>
                    { 'posts.menu'.translate() }
                  </Link>
                </li>

                <li>
                  <Link to={tagLinks.list}>
                    { 'tags.menu'.translate() }
                  </Link>
                </li>

                <li>
                  <a href={linkBlog} target="_blank">
                    <small><i className="fa fa-link"></i>&nbsp;&nbsp;</small>
                    <span>{ 'blogs.menu.ver'.translate() }</span>
                  </a>
                </li>
              </ul>
            </li>

            <li className="treeview">
              <Link to={configLinks.list}>
                <i className="fa fa-cogs"></i>
                <span>{ 'configs.menu'.translate() }</span>
              </Link>
            </li>
          </ul>
        </section>
      </aside>
    );
  }
});

module.exports = DashSidebar;
