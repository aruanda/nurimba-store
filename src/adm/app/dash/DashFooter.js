'use strict';

var React = require('react');

var DashFooter = React.createClass({
  render: function() {
    return (
      <footer className="main-footer overflowHidden">
        <div className="pull-left">
          <strong>
            Gostou? <a href="http://nurimba.com.br/" target={'_blank'}>Solicite um orçamento!</a>
          </strong>
        </div>

        <div className="pull-right">
          &copy; 2016 
          <a href="http://nurimba.com" target={'_blank'}>Nurimba Developers Team</a>.
          Todos direitos reservados.
        </div>
      </footer>
    );
  }
});

module.exports = DashFooter;
