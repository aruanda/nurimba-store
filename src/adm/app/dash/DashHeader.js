'use strict';

var React = require('react');

var DashHeader = React.createClass({
  render: function() {
    return(
      <header className="main-header">
        <a href="index2.html" className="logo">
          <span className="logo-mini"><b>nb</b>A</span>
          <span className="logo-lg"><b>nb</b>Admin</span>
        </a>

        <nav className="navbar navbar-static-top" role="navigation">
          <a href="#" className="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span className="sr-only">Toggle navigation</span>
          </a>

          <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">
              <li>
                <a href="http://nurimba.com.br/" target={'_blank'}><b>Gostou?&nbsp;&nbsp;&nbsp;Solicite um orçamento!</b></a>
              </li>
              <li>
                <a href="mailto:contato@nurimba.com?subject=nbAdmin"><i className="fa fa-envelope"></i></a>
              </li>
              <li>
                <a href="/"><i className="fa fa-power-off"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    );
  }
});

module.exports = DashHeader;
