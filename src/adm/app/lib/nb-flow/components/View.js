'use strict';

var React        = require('react');
var Reflux       = require('reflux');
var nbHistory    = require('react-router').browserHistory;
var ViewContent  = require('nb-view');
var AlertModal   = require('nb-modal').AlertModal;
var LoadingModal = require('nb-modal').LoadingModal;
var Modes        = require('../Modes');

module.exports = function(pDomain, views, pStore, pActions, links, buttons) {
  return {
    mixins: [
      Reflux.ListenerMixin
    ],

    getInitialState: function() {
      return {
        mode: Modes.view,

        model: {},

        alert: {
          show: false,
          type: '',
          title: '',
          message: '',
          action: null
        },

        loading: {
          show: false,
          type: '',
          title: '',
          message: ''
        },

        error: false,
        waiting: false
      };
    },

    componentDidMount: function() {
      this.listenTo(pStore, this.viewStoreRender);
      var code = this.props.params.code;
      pActions.load(code);
    },

    openModalLoading: function() {
      var state = this.state;

      state.alert.show    = false;
      state.loading.show  = true;
      state.loading.title   = pDomain.concat('.waiting.view.title');
      state.loading.message = pDomain.concat('.waiting.view.message');
      state.loading.type    = 'warning';

      if (this.timout) clearTimeout(this.timout);
      this.timout = setTimeout(function() {
        this.setState(state);
      }.bind(this), 150);
    },

    closeModalLoading: function() {
      var state = this.state;
      var store = pStore.state;

      state.loading.show = false;
      state.model = store.model;

      if (this.timout) clearTimeout(this.timout);
      this.timout = setTimeout(function() {
        this.setState(state);
      }.bind(this), 150);
    },

    openModalAlertError: function() {
      var state = this.state;
      var store = pStore.state;

      state.error = store.error;
      state.model = store.model;

      state.loading.show  = false;
      state.alert.show    = true;
      state.alert.type    = 'danger';
      state.alert.title   = pDomain.concat('.alert.view.title');
      state.alert.message = store.error;

      state.alert.btnAction = function() {
        this.handleGoToList();
      }.bind(this);

      if (this.timout) clearTimeout(this.timout);
      this.timout = setTimeout(function() {
        this.setState(state);
      }.bind(this), 150);
    },

    viewStoreRender: function() {
      var store = pStore.state;

      var isWait   = Boolean(store.waiting);
      var isError  = Boolean(store.error);
      var isLoaded = !isError && !isWait && store.mode === Modes.loaded;

      if (isLoaded) return this.closeModalLoading();
      if (isError)  return this.openModalAlertError();
      if (isWait)   return this.openModalLoading();
    },

    handleGoToNew: function() {
      nbHistory.push(links.add);
    },

    handleGoToEdit: function() {
      var code = String(this.state.model.id);
      nbHistory.push(links.editById(code));
    },

    handleGoToList: function() {
      nbHistory.push(links.list);
    },

    render: function() {
      var ref = this;
      var store = ref.state;

      if (!buttons) buttons = [];

      return (
        <div className="content-wrapper">
          <ViewContent
            error={store.error}
            model={store.model}
            views={views}
            domain={pDomain}
            btns={buttons.filter(function(btn) {
              return Boolean(!btn.hasOwnProperty('viewOnly') || !btn.viewOnly || btn.viewOnly(store.model));
            })}
          />

          <AlertModal
            show={store.alert.show}
            type={store.alert.type}
            title={store.alert.title}
            message={store.alert.message}
            btnAction={store.alert.btnAction}
          />

          <LoadingModal
            show={store.loading.show}
            type={store.loading.type}
            title={store.loading.title}
            message={store.loading.message}
          />
        </div>
      );
    }
  };
};
