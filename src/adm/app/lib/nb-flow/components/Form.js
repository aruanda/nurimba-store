'use strict';

var React        = require('react');
var Reflux       = require('reflux');
var nbHistory    = require('react-router').browserHistory;
var FormContent  = require('nb-form');
var AlertModal   = require('nb-modal').AlertModal;
var LoadingModal = require('nb-modal').LoadingModal;
var Modes        = require('../Modes');

module.exports = function(pDomain, fields, pStore, pActions, links, buttons) {
  return {
    mixins: [
      Reflux.ListenerMixin
    ],

    getInitialState: function() {
      return {
        mode: Modes.new,

        model: {},

        alert: {
          show: false,
          type: '',
          title: '',
          message: '',
          action: null
        },

        loading: {
          show: false,
          type: '',
          title: '',
          message: ''
        },

        error: false,
        waiting: false
      };
    },

    componentDidMount: function() {
      this.listenTo(pStore, this.formStoreRender);
      var code = this.props.params.code;
      if (code) pActions.load(code);
    },

    componentDidUpdate: function() {
      var store = this.state;

      var isWait  = Boolean(store.waiting);
      var isError = Boolean(store.error);

      if (isWait) return;
      if (isError) return;

      var code = this.props.params.code;
      if (!code && store.model && store.model.id) return pActions.add();
      if (code && (!store.model || store.model.id != code)) pActions.load(code);
    },

    openModalLoading: function() {
      var state = this.state;
      var store = pStore.state;

      state.alert.show      = false;
      state.loading.show    = true;
      state.loading.title   = pDomain.concat('.waiting.form.title.', store.mode);
      state.loading.message = pDomain.concat('.waiting.form.message.', store.mode);
      state.loading.type    = 'warning';

      this.setState(state);
    },

    closeModalLoading: function() {
      var state = this.state;
      var store = pStore.state;

      state.error = false;
      state.model = store.model;
      state.loading.show = false;

      this.setState(state);
    },

    openModalAlertError: function() {
      var state = this.state;
      var store = pStore.state;

      state.error = store.error;
      state.model = store.model;

      state.loading.show  = false;
      state.alert.show    = true;
      state.alert.type    = 'danger';
      state.alert.title   = pDomain.concat('.alert.error.title.', store.mode);
      state.alert.message = store.error;

      state.alert.btnAction = function() {
        var formPageState = this.state;
        formPageState.alert.show = false;
        this.setState(formPageState);
      }.bind(this);

      return this.setState(state);
    },

    openModalSuccess: function() {
      var state = this.state;
      var store = pStore.state;

      state.error = false;
      state.model = store.model;

      state.loading.show  = false;
      state.alert.show    = true;
      state.alert.title   = pDomain.concat('.alert.form.title.', store.mode);
      state.alert.message = pDomain.concat('.alert.form.message.', store.mode);
      state.alert.type    = 'success';

      state.alert.btnAction = function() {
        nbHistory.push(links.viewById(state.model.id));
      }.bind(this);

      return this.setState(state);
    },

    formStoreRender: function() {
      var ref = this;
      if (ref.setTimeStoreRender) clearTimeout(ref.setTimeStoreRender);

      ref.setTimeStoreRender = setTimeout(function() {
        var store = pStore.state;

        var isWait   = Boolean(store.waiting);
        var isError  = Boolean(store.error);
        var isLoaded = !isError && !isWait && store.mode === Modes.loaded;
        var isSaved  = !isError && !isWait && !isLoaded && store.model.hasOwnProperty('id') && store.model.id;

        if (isWait)  return ref.openModalLoading();
        if (isError) return ref.openModalAlertError();

        if (isLoaded) {
          ref.state.mode = Modes.edit;
          return ref.closeModalLoading();
        }

        if (isSaved)  return ref.openModalSuccess();
      }, 450);
    },

    handleOnSubmit: function(data) {
      pActions.save(data);
    },

    render: function() {
      var store = this.state;

      if (!buttons) buttons = [];

      return (
        <div className="content-wrapper">
          <section className="content-header overflowHidden">
            <h1 className="pull-left">
              { pDomain.concat('.form.title').translate() }
              { ( store.model && store.model.id ? ' #'.concat(store.model.id) : '') }
            </h1>

            <ul className="list-unstyled pull-right las">
              {buttons.filter(function(btn) {
                return Boolean(!btn.hasOwnProperty('viewOnly') || !btn.viewOnly || btn.viewOnly(store.model));
              }).map(function(btn, index) {
                return (
                  <li key={index} className="pull-left marginLeft10px">
                    <button className={btn.className} type={btn.type} onClick={function() {
                      btn.onClick(store.model);
                    }}>
                      {btn.text.translate()}
                    </button>
                  </li>
                );
              })}
            </ul>
          </section>

          <section className="content">
            <div className="row">
              <FormContent
                btns={buttons}
                error={store.error}
                model={store.model}
                fields={fields}
                onSubmit={this.handleOnSubmit}
              />
            </div>
          </section>

          <AlertModal
            show={store.alert.show}
            type={store.alert.type}
            title={store.alert.title}
            message={store.alert.message}
            btnAction={store.alert.btnAction}
          />

          <LoadingModal
            show={store.loading.show}
            type={store.loading.type}
            title={store.loading.title}
            message={store.loading.message}
          />
        </div>
      );
    }
  };
};
