'use strict';

var React       = require('react');
var Reflux      = require('reflux');
var nbHistory   = require('react-router').browserHistory;
var ListContent = require('nb-list');

module.exports = function(pDomain, cols, pStore, pActions, links, buttons) {
  return {
    mixins: [
      Reflux.connect(pStore, 'store')
    ],

    goToViewRecord: function(record) {
      nbHistory.push(links.viewById(record.id));
    },

    goToEditRecord: function(record) {
      nbHistory.push(links.editById(record.id));
    },

    componentDidMount: function() {
      pActions.search();
    },

    getActionsList: function() {
      var actionsList = {
        btns: [
          {
            text: pDomain.concat('.list.view'),
            type: 'button',
            onClick: this.goToViewRecord,
            className: 'btn btn-block btn-default btn-sm',
            icon: 'glyphicon glyphicon-eye-open'
          },

          {
            text: pDomain.concat('.list.edit'),
            type: 'button',
            onClick: this.goToEditRecord,
            className: 'btn btn-block btn-default btn-sm',
            icon: 'glyphicon glyphicon-edit'
          }
        ]
      };

      return actionsList;
    },

    render: function() {
      var store = this.state.store;
      var actions = this.getActionsList();

      if (!buttons) buttons = [];

      return (
        <div className="content-wrapper">
          <section className="content-header overflowHidden">
            <h1 className="pull-left">
              { pDomain.concat('.list.title').translate() }
            </h1>

            <ul className="list-unstyled pull-right las">
              {buttons.filter(function(btn) {
                return Boolean(!btn.hasOwnProperty('viewOnly') || !btn.viewOnly || btn.viewOnly());
              }).map(function(btn, index) {
                return (
                  <li key={index} className="pull-left marginLeft10px">
                    <button className={btn.className} type={btn.type} onClick={function() {
                      btn.onClick();
                    }}>
                      {btn.text.translate()}
                    </button>
                  </li>
                );
              })}
            </ul>
          </section>

          <section className="content">
            <div className="row">
              <div className="col-xs-12">
                <div className="box">
                  <div className="box-body table-responsive no-padding">
                    <ListContent
                      cols={cols}
                      data={store.rows}
                      loaded={store.fetch}
                      actions={actions}
                    />
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      );
    }
  };
};
