'use strict';

var nbListStore = require('./stores/List');
var nbFormStore = require('./stores/Form');
var nbViewStore = require('./stores/View');

var nbListActions = require('./actions/List');
var nbFormActions = require('./actions/Form');
var nbViewActions = require('./actions/View');

var nbListPage = require('./components/List');
var nbFormPage = require('./components/Form');
var nbViewPage = require('./components/View');

var nbFields = require('./NurimbaFields');

module.exports = {
  nbListStore: nbListStore,
  nbFormStore: nbFormStore,
  nbViewStore: nbViewStore,

  nbListActions: nbListActions,
  nbFormActions: nbFormActions,
  nbViewActions: nbViewActions,

  nbListPage: nbListPage,
  nbFormPage: nbFormPage,
  nbViewPage: nbViewPage,

  nbFields: nbFields
};
