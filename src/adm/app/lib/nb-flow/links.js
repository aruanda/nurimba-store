'use strict';

module.exports = function(domain) {
  var code       = ':code';
  var addRecord  = '/add'.translate();
  var editRecord = '/edit'.translate();
  var viewRecord = '/view'.translate();
  var baseRoute  = '/'.translate() + '/'.concat(domain).translate();

  return {
    add:  baseRoute.concat(addRecord),
    edit: baseRoute.concat('/', code, editRecord),
    view: baseRoute.concat('/', code, viewRecord),
    list: baseRoute,
    domn: domain,

    editById: function(code) {
      return this.edit.replace(/\:code/g, code);
    },

    viewById: function(code) {
      return this.view.replace(/\:code/g, code);
    }
  };
};
