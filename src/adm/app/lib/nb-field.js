/* global window:false*/
'use strict';

var all = require('q').all;
var React = require('react');
var ValidationMessage = require('nb-check-message');

module.exports = {
  checkState: function(validations, value) {
    var notFilled = !value || !String(value).length;
    if (notFilled) value = undefined;

    var verifications = [];
    var existsValidations = !validations || validations.length;

    if (existsValidations) {
      verifications = validations.map(function(validation) {
        return validation.verify(value);
      });
    }

    return all(verifications).then(function() {
      return {
        dirty: true,
        valid: true,
        value: value,
        errorRef: undefined,
        errorMessage: undefined
      };
    }).catch(function(error) {
      throw {
        dirty: true,
        valid: false,
        value: undefined,
        errorRef: error.ref,
        errorMessage: error.message
      };
    });
  },

  handleChange: function(event) {
    var value = String(event.target.value || '').trim();
    this.registerValue(value);
  },

  getFieldValue: function(field, model){
    var value = undefined;
    var withValue = Boolean(model && model.hasOwnProperty(field.attr) && model[field.attr]);
    if (withValue) value = model[field.attr];

    if (field.kind === 'images') {
      if (!value) value = [];

      value.forEach(function(file) {
        var withoutPreview = !file.hasOwnProperty('preview');
        if (withoutPreview) file.preview = 'http://' + window.location.hostname + '/files/' + file.filetag + '/' + file.filename;

        var withoutName = !file.hasOwnProperty('name');
        if (withoutName) file.name = file.filename;

        var withoutFileTitle = !file.hasOwnProperty('filetitle');
        if (withoutFileTitle) file.filetitle = file.name;
      });
    }

    return value;
  },

  getFieldClass: function(field, state){
    var className  = 'form-group '.concat(field.hasOwnProperty('className') && field.className ? field.className : '');
    var fieldDirty = Boolean(state.dirty);
    if (fieldDirty) {
      var classState = state.valid ? 'has-success' : 'has-error';
      className = className + ' ' + classState;
    }

    return className;
  },

  getFieldState: function(field, model){
    if (!model.hasOwnProperty('_extra')) model._extra = {};
    if (!model._extra.hasOwnProperty(field.attr)) model._extra[field.attr] = {};

    return model._extra[field.attr];
  },

  getProperties: function(config) {
    var model = this.props.model;

    var value = this.getFieldValue(config, model);
    var state = this.getFieldState(config, model);
    var className = this.getFieldClass(config, state);

    var propertyName = 'count-'.concat(config.attr);
    var withoutCounter = !this.hasOwnProperty(propertyName);
    if (withoutCounter) this[propertyName] = 0;
    var counter = ++this[propertyName];

    var htmlFor      = config.label.concat('.', counter);
    var readOnly     = config.hasOwnProperty('readonly') && config.readonly;
    var messageField = this.getErrorMessage(state.errorRef, state.errorMessage);

    return {
      value: value,
      className: className,
      htmlFor: htmlFor,
      readOnly: readOnly,
      message: messageField,
      label: config.label,
      place: config.place,
      attr: config.attr,
      state: state,
      field: config,
      view: config
    };
  },

  getViewProperties: function() {
    var view = this.props.view;
    return this.getProperties(view);
  },

  getFieldProperties: function() {
    var field = this.props.field;
    return this.getProperties(field);
  },

  getErrorMessage: function(ref, msg) {
    var messageField;

    if (ref) {
      messageField = React.createElement(ValidationMessage, {
        ref: ref,
        type: 'danger',
        message: msg
      });
    }

    return messageField;
  },

  registerValue: function(value, attrField) {
    var field = this;
    var cfg = field.props.field;
    var model = field.props.model;
    var attr = attrField || cfg.attr;

    var renderView = function(state) {
      model[attr] = value;
      model._extra[attr] = state;
      field.forceUpdate();
    };

    if (this.registerTemp) clearTimeout(this.registerTemp);

    this.registerTemp = setTimeout(function() {
      field
        .checkState(cfg.validations, value)
        .then(renderView)
        .catch(renderView);
    }, 50);
  },

  handleKeyDownOnlyInteger: function(e) {
    var chrNum;
    var totDig =  9;
    var keyBck =  8;
    var keyTab =  9;
    var keyHom = 36;
    var keyEnd = 35;
    var keyDel = 45;
    var keyIns = 46;
    var keyEnt = 13;
    var keyUp  = 38;
    var keyDwn = 40;
    var keyPrr = 37;
    var keyNxt = 39;
    var iniNum = 48;
    var iniPad = 96;
    var keyNum = e.which || e.keyCode || e.charCode;

    var ctrl = [
      keyUp,
      keyDwn,
      keyPrr,
      keyNxt,
      keyHom,
      keyEnd,
      keyDel,
      keyIns,
      keyEnt,
      keyBck,
      keyTab
    ];

    if (ctrl.indexOf(keyNum) > -1) chrNum = keyNum;
    if (keyNum >= iniNum && keyNum <= (iniNum + totDig)) chrNum = keyNum - iniNum;
    if (keyNum >= iniPad && keyNum <= (iniPad + totDig)) chrNum = keyNum - iniPad;
    if (chrNum === undefined) e.preventDefault();
  }
};
