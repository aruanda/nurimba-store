'use strict';

var React = require('react');

var typesFields = {
  seo: require('./fields/FieldSEO'),
  text: require('./fields/FieldText'),
  email: require('./fields/FieldEmail'),
  group: require('./fields/FieldGroup'),
  title: require('./fields/FieldTitle'),
  string: require('./fields/FieldString'),
  images: require('./fields/FieldImages'),
  integer: require('./fields/FieldInteger'),
  currency: require('./fields/FieldCurrency'),
  textarea: require('./fields/FieldTextarea'),
  imageMain: require('./fields/FieldImageMain'),
  publication: require('./fields/FieldPublication'),
  suggestSingle: require('./fields/FieldSuggestSingle'),
  suggestMultiple: require('./fields/FieldSuggestMultiple')
};

var FormFields = React.createClass({
  render: function () {
    var form  = this;
    var model = form.props.model;

    var cols = {
      'firstCol': [],
      'secondCol': []
    };

    this.props.fields.forEach(function(field) {
      if (!typesFields.hasOwnProperty(field.kind)) return;

      field.label = field.domain.concat('.form.label.', field.attr);
      field.place = field.domain.concat('.form.place.', field.attr);

      var Field = typesFields[field.kind];
      cols[field.colName].push(<Field key={cols[field.colName].length} field={field} model={model} />);
    });

    return (
      <fieldset>
        <div className="col-md-8">
          {cols.firstCol}
        </div>

        <div className="col-md-4">
          {cols.secondCol}
        </div>
      </fieldset>
    );
  }
});

module.exports = FormFields;
