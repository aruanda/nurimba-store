'use strict';

var React = require('react');
var FieldMixin = require('nb-field');

var FieldTextarea = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    var props = this.getFieldProperties();

    return (
      <div className="nav-tabs-custom">
        <ul className="nav nav-tabs pull-right">
          <li className="pull-left header">
            <i className="fa fa-file-text-o"></i>
            { props.label.translate() }
          </li>
        </ul>

        <div className="tab-content">
          <div className="tab-pane active">
            <div className="form-group">
              <textarea
                className="form-control"
                rows="3"
                value={props.value}
                placeholder={props.place.translate()}
                onChange={this.props.onChange || this.handleChange}
              />
            </div>

            {props.message}
          </div>
        </div>
      </div>
    );
  }
});

module.exports = FieldTextarea;
