'use strict';

var React = require('react');
var FieldMixin = require('nb-field');

var FieldInteger = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    var props = this.getFieldProperties();

    return (
      <div ref='containerField' className={props.className}>
        <label ref='labelField' className='control-label' htmlFor={props.htmlFor}>
          {props.label.translate()}
        </label>

        <div className='input-group'>
          <label htmlFor={props.htmlFor} className='input-group-addon'>
            <i className='fa fa-sort-numeric-asc'></i>
          </label>

          <input
            id={props.htmlFor}
            ref='integerField'
            readOnly={props.readOnly}
            defaultValue={props.value}
            onChange={this.handleChange}
            onKeyDown={this.handleKeyDownOnlyInteger}
            type='text'
            className='form-control'
            placeholder={props.place.translate()}
          />
        </div>

        {props.message}
      </div>
    );
  }
});

module.exports = FieldInteger;
