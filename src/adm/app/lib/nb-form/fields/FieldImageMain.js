/*globals $:false*/
'use strict';

var React = require('react');
var FieldMixin = require('nb-field');
var ContentImage = require('nb-content-image');

var FieldSEO = React.createClass({
  mixins: [FieldMixin],

  handleSaveImageMain: function(img) {
    var image = $(img);
    var props = this.getFieldProperties();
    var imageMain = props.value;
    var withoutValue = Boolean(!imageMain);
    if (withoutValue) imageMain = {};

    imageMain.src    = image.attr('src');
    imageMain.alt    = image.attr('alt');
    imageMain.width  = image.css('width');
    imageMain.height = image.css('height');

    this.registerValue(imageMain);
  },

  render: function() {
    var props = this.getFieldProperties();
    var imageMain = props.value;
    var withoutValue = Boolean(!imageMain);
    if (withoutValue) imageMain = {};

    return (
      <div className="box">
        <div className="box-header with-border">
          <h3 className="box-title">
            Imagem principal
          </h3>
        </div>

        <div className="box-body">
          <ContentImage
            src={imageMain.src}
            alt={imageMain.alt}
            width={imageMain.width}
            height={imageMain.height}
            defaultWidth={508}
            defaultHeight={331}
            className={'img-responsive pad'}
            onChange={this.handleSaveImageMain}
          />
        </div>
      </div>
    );
  }
});

module.exports = FieldSEO;
