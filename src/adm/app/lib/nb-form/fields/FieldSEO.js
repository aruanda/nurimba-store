/*globals $:false*/
'use strict';

var React = require('react');
var FieldInput = require('./FieldInput');
var FieldSelect = require('./FieldSelect');
var FieldMixin = require('nb-field');
var ContentImage = require('nb-content-image');
var ContentEditable = require('nb-content-editable');

var FieldSEO = React.createClass({
  mixins: [FieldMixin],

  handleInline: function(property, texto) {
    var props = this.getFieldProperties();

    var seoAttrs  = props.value || {};
    seoAttrs[property] = texto;

    this.registerValue(seoAttrs);
  },

  handleSaveTwitterImage: function(img) {
    var image = $(img);

    this.handleInline('twitterImage', {
      src:    image.attr('src'),
      alt:    image.attr('alt'),
      width:  image.css('width'),
      height: image.css('height')
    });
  },

  handleSaveTwitterTitle: function(text) {
    this.handleInline('twitterTitle', text);
  },

  handleSaveTwitterDescription: function(text) {
    this.handleInline('twitterDescription', text);
  },

  handleSaveFacebookImage: function(img) {
    var image = $(img);

    this.handleInline('facebookImage', {
      src:    image.attr('src'),
      alt:    image.attr('alt'),
      width:  image.css('width'),
      height: image.css('height')
    });
  },

  handleSaveFacebookTitle: function(text) {
    this.handleInline('facebookTitle', text);
  },

  handleSaveFacebookDescription: function(text) {
    this.handleInline('facebookDescription', text);
  },

  handleSaveGoogleTitle: function(text) {
    this.handleInline('googleTitle', text);
  },

  handleSaveGoogleDescription: function(text) {
    this.handleInline('googleDescription', text);
  },

  handleSaveGoogleImage: function(img) {
    var image = $(img);

    this.handleInline('googleImage', {
      src:    image.attr('src'),
      alt:    image.attr('alt'),
      width:  image.css('width'),
      height: image.css('height')
    });
  },

  handleKeywordsChange: function(keywords) {
    this.handleInline('keywords', keywords);
  },

  handleDescriptionChange: function(description) {
    this.handleInline('description', description);
  },

  handleRobotChange: function(robots) {
    this.handleInline('robots', robots);
  },

  render: function() {
    var props = this.getFieldProperties();
    var seoAttrs = props.value;

    if (!seoAttrs) {
      seoAttrs = {};
      props.value = seoAttrs;
    }

    var googImg = seoAttrs.googleImage   ? seoAttrs.googleImage   : {};
    var twitImg = seoAttrs.twitterImage  ? seoAttrs.twitterImage  : {};
    var faceImg = seoAttrs.facebookImage ? seoAttrs.facebookImage : {};

    var options = [
      { value: 'index, follow',     label: 'index,   follow'   },
      { value: 'noindex, nofollow', label: 'noindex, nofollow' }
    ];

    return (
      <div>
        <div className="nav-tabs-custom">
          <ul className="nav nav-tabs pull-right">
            <li><a href="#indexacao" data-toggle="tab" aria-expanded="false">Indexação</a></li>
            <li className="active"><a href="#metatags" data-toggle="tab" aria-expanded="true">Meta tags</a></li>
            <li className="pull-left header"><i className="fa fa-globe"></i> SEO</li>
          </ul>
          <div className="tab-content">
            <div className="tab-pane active" id="metatags">
              <div className="form-group">
                <label>Description</label>

                <FieldInput
                  type="text"
                  className="form-control"
                  placeholder="Descrição da página"
                  value={seoAttrs.description}
                  onChange={this.handleDescriptionChange}
                />
              </div>

              <div className="form-group">
                <label>Keywords</label>

                <FieldInput
                  type="text"
                  className="form-control"
                  placeholder="Palavras-chave da página"
                  value={seoAttrs.keywords}
                  onChange={this.handleKeywordsChange}
                />
              </div>
            </div>

            <div className="tab-pane" id="indexacao">
              <div className="form-group">
                <label>Indexação</label>

                <FieldSelect
                  className="form-control"
                  placeholder="Indexação da página"
                  value={seoAttrs.robots}
                  onChange={this.handleRobotChange}
                  options={options}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="nav-tabs-custom">
          <ul className="nav nav-tabs pull-right">
            <li>
              <a href="#google" data-toggle="tab" aria-expanded="false">
                { 'seo.google.label'.translate() }
              </a>
            </li>

            <li>
              <a href="#twitter" data-toggle="tab" aria-expanded="false">
                { 'seo.twitter.label'.translate() }
              </a>
            </li>

            <li className="active">
              <a href="#facebook" data-toggle="tab" aria-expanded="true">
                { 'seo.facebook.label'.translate() }
              </a>
            </li>

            <li className="pull-left header">
              <i className="fa fa-rocket"></i>
              Midias Sociais
            </li>
          </ul>

          <div className="tab-content">
            <div className="tab-pane" id="google">
              <div className="box-body">
                <div className="col-xs-12 bg-red-active">
                  <i className="fa fa-google-plus fontSize18emPadding10px10px10px0"></i>
                  <span className="midiaSocial">
                    { 'seo.google.label'.translate() }
                  </span>
                </div>

                <div className="col-xs-12 attachment-block clearfix padding15px0">
                  <div className="col-xs-12 col-md-4">
                  <ContentImage
                    src={googImg.src}
                    alt={googImg.alt}
                    width={googImg.width}
                    height={googImg.height}
                    defaultWidth={175}
                    defaultHeight={115}
                    className={'img-responsive'}
                    onChange={this.handleSaveGoogleImage}
                  />
                  </div>

                  <div className="col-xs-12 col-md-8 marginTop20px">
                    <h4 className="attachment-heading text-red">
                      <ContentEditable
                        onlyText={true}
                        html={seoAttrs.googleTitle || 'seo.google.title'.translate() }
                        onChange={this.handleSaveGoogleTitle}
                      />
                    </h4>

                    <div className="attachment-tex">
                      <p>
                        <ContentEditable
                          onlyText={true}
                          html={seoAttrs.googleDescription || 'seo.google.description'.translate() }
                          onChange={this.handleSaveGoogleDescription}
                        />
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="tab-pane" id="twitter">
              <div className="box-body">
                <div className="col-xs-12 bg-aqua">
                  <i className="fa fa-twitter fontSize18emPadding10px10px10px0"></i>
                  <span className="midiaSocial">
                    { 'seo.twitter.label'.translate() }
                  </span>
                </div>

                <div className="col-xs-12 attachment-block clearfix padding15px0">
                  <div className="col-xs-12 col-md-4">
                  <ContentImage
                    src={twitImg.src}
                    alt={twitImg.alt}
                    width={twitImg.width}
                    height={twitImg.height}
                    defaultWidth={175}
                    defaultHeight={115}
                    className={'img-responsive'}
                    onChange={this.handleSaveTwitterImage}
                  />
                  </div>

                  <div className="col-xs-12 col-md-8 marginTop20px">
                    <h4 className="attachment-heading text-aqua">
                      <ContentEditable
                        onlyText={true}
                        html={seoAttrs.twitterTitle || 'seo.twitter.title'.translate() }
                        onChange={this.handleSaveTwitterTitle}
                      />
                    </h4>

                    <div className="attachment-tex">
                      <p>
                        <ContentEditable
                          onlyText={true}
                          html={seoAttrs.twitterDescription || 'seo.twitter.description'.translate() }
                          onChange={this.handleSaveTwitterDescription}
                        />
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="tab-pane active" id="facebook">
              <div className="box-body">
                <div className="col-xs-12 bg-blue-active">
                  <i className="fa fa-facebook-official fontSize18emPadding10px10px10px0"></i>
                  <span className="midiaSocial">
                    { 'seo.facebook.label'.translate() }
                  </span>
                </div>

                <div className="col-xs-12 attachment-block clearfix padding15px0">
                  <div className="col-xs-12 col-md-4">
                  <ContentImage
                    src={faceImg.src}
                    alt={faceImg.alt}
                    width={faceImg.width}
                    height={faceImg.height}
                    defaultWidth={175}
                    defaultHeight={115}
                    className={'img-responsive'}
                    onChange={this.handleSaveFacebookImage}
                  />
                  </div>

                  <div className="col-xs-12 col-md-8 marginTop20px">
                    <h4 className="attachment-heading text-light-blue">
                      <ContentEditable
                        onlyText={true}
                        html={seoAttrs.facebookTitle || 'seo.facebook.title'.translate() }
                        onChange={this.handleSaveFacebookTitle}
                      />
                    </h4>

                    <div className="attachment-tex">
                      <p>
                        <ContentEditable
                          onlyText={true}
                          html={seoAttrs.facebookDescription || 'seo.facebook.description'.translate() }
                          onChange={this.handleSaveFacebookDescription}
                        />
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = FieldSEO;
