/*globals $:false*/
'use strict';

var React        = require('react');
var copy         = require('copy-to-clipboard');
var Dropzone     = require('react-dropzone');
var nbLink       = require('nb-link');
var FieldMixin   = require('nb-field');
var nbConnection = require('nb-connection');
var LoadingModal = require('nb-modal').LoadingModal;

var FieldImages = React.createClass({
  mixins: [FieldMixin],

  getInitialState: function() {
    return {
      show: false,
      type:    '',
      title:   '',
      message: ''
    };
  },

  onDrop: function (files) {
    var comp  = this;
    var state = this.state;
    var props = this.getFieldProperties();

    var fileTag = props.attr;
    var backend = nbConnection.getInstance();
    var params  = { attr: fileTag };

    files.forEach(function(f) {
      f.field = fileTag + '/' + f.name;
    });

    state.show = true;
    state.type = 'warning';
    state.title = 'Aguarde';
    state.message = 'As fotos estão sendo enviadas para o servidor. O tempo dessa operação vai depender da qualidade de sua conexão.';

    this.setState(state, function() {
      backend.all('files').post(params, files).then(function(res) {
        var newListFiles = res.body[fileTag].concat(props.value || []);

        state.show = false;
        this.setState(state, function() {
          $(comp.refs.abaGaleria).click();
          this.registerValue(newListFiles);
        }.bind(this));
      }.bind(this));
    }.bind(this));
  },

  render: function() {
    var state = this.state;
    var props = this.getFieldProperties();
    if (!props.value) props.value = [];

    var dropZone;
    if (!props.readOnly) dropZone = (
      <Dropzone ref="dropzone" className="dropzone" onDrop={this.onDrop}>
        <div className="form-control" style={{ minHeight: '74px' }}>
          {props.place.translate()}
        </div>
      </Dropzone>
    );

    return (
      <div className="nav-tabs-custom">
        <ul className="nav nav-tabs pull-right">
          <li><a href="#galeria" ref="abaGaleria" data-toggle="tab" aria-expanded="false">Galeria</a></li>
          <li className="active"><a href="#upload" data-toggle="tab" aria-expanded="true">Upload</a></li>
          <li className="pull-left header"><i className="fa fa-image"></i> Imagens anexadas</li>
        </ul>

        <div className="tab-content">
          <div className="tab-pane active" id="upload">
            <div className="form-group">
              {dropZone}
            </div>
          </div>

          <div className="tab-pane overflowHidden" id="galeria">
            <ul className="mailbox-attachments">
              {props.value.filter(function(file) {
                return !file.hasOwnProperty('removed') || !file.removed;
              }).map(function(file, index) {
                file.preview = nbLink('files/' + file.filetag + '/' + file.filename);

                var withoutName = !file.hasOwnProperty('name');
                if (withoutName) file.name = file.filename;

                var copyFile = function() {
                  return copy(file.preview);
                };

                return (
                  <li key={index}>
                    <span className="mailbox-attachment-icon has-img">
                      <img src={file.preview} alt={file.name} />
                    </span>

                    <div className="mailbox-attachment-info">
                      <a href={file.preview} target="_blank" className="mailbox-attachment-name">
                        <i className="fa fa-camera"></i>
                        {file.name}
                      </a>

                      <button type="button" onClick={copyFile} className="btn btn-primary pull-right btn-xs">
                        <i className="fa fa-copy"></i> copiar url
                      </button>
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>

        <LoadingModal
          show={state.show}
          type={state.type}
          title={state.title}
          message={state.message}
        />
      </div>
    );
  }
});

module.exports = FieldImages;
