'use strict';

var React = require('react');

var FieldSelect = React.createClass({
  componentDidUpdate: function() {
    var isNotDiffValue = Boolean(this.refs.select.value === this.props.value);
    if (isNotDiffValue) return;

    var value = [null, undefined].indexOf(this.props.value) === -1 ? this.props.value : '';
    this.refs.select.value = value;
  },

  componentChangeValue: function(e) {
    var value = e.target.value;
    var isNotDiffValue = Boolean(value === this.props.value);
    if (isNotDiffValue) return;
    this.props.onChange(value);
  },

  render: function() {
    var props = this.props;
    var value = props.value;
    if (!value) value = '';

    var options = props.options.map(function(option, index) {
      return (
        <option key={index} value={option.value}>
          {option.label}
        </option>
      );
    });

    if (props.placeholder) options.unshift(<option key={-1} value={''}>{props.placeholder.translate()}</option>);

    return (
      <select
        ref={'select'}
        value={value}
        className={props.className}
        onChange={this.componentChangeValue}
      >
        {options}
      </select>
    );
  }
});

module.exports = FieldSelect;
