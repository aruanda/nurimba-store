/*globals $:false*/
'use strict';

var React = require('react');
var FieldMixin = require('nb-field');
var FieldSelect = require('./FieldSelect');
var nbMoment = require('nb-moment');
var nbHistory = require('react-router').browserHistory;

var FieldPublication = React.createClass({
  mixins: [FieldMixin],

  componentDidMount: function() {
    var field = this;
    var inputLaunch = $(field.refs.inputLaunch);

    inputLaunch.inputmask('dd/mm/yyyy', {
      'placeholder': 'dd/mm/aaaa'
    });

    inputLaunch.on('change', field.handleChangeDate);

    inputLaunch.datepicker({
      todayBtn: 'linked',
      language: 'pt-BR',
      autoclose: true,
      todayHighlight: true
    });
  },

  componentDidUpdate: function() {
    var props = this.props;

    var fieldDataTime = props.field.publication.fieldDateTime;
    var valueDateTime = props.model[fieldDataTime];
    var valueFormated = this.handleFormatDate(valueDateTime);

    var value = $(this.refs.inputLaunch).val();
    if (value === valueFormated) return;

    $(this.refs.inputLaunch).val(valueFormated);
  },

  componentWillUnmount: function() {
    var field = this;
    var inputLaunch = $(field.refs.inputLaunch);

    inputLaunch.off('change');
    inputLaunch.inputmask('destroy');
    inputLaunch.datepicker('destroy');
  },

  handleChangeStatus: function(status) {
    if (!this.isMounted()) return;
    var props = this.props;
    var fieldAttr = props.field.publication.fieldStatus;
    if (!status) status = undefined;
    if (status === this.props.model[fieldAttr]) return;
    this.registerValue(status, fieldAttr);
  },

  handleChangeDate: function() {
    var props = this.props;
    var fieldAttr = props.field.publication.fieldDateTime;
    var valueInput = $(this.refs.inputLaunch).val();

    var valueFormated = valueInput && nbMoment(valueInput, 'DD/MM/YYYY').isValid()
      ? nbMoment(valueInput, 'DD/MM/YYYY').format('YYYY-MM-DD')
      : undefined;

    if (valueFormated === this.props.model[fieldAttr]) return;

    this.registerValue(valueFormated, fieldAttr);
  },

  handleOnCancel: function() {
    nbHistory.goBack();
  },

  handleFormatDate: function(valueDateTime) {
    if (!valueDateTime) return '';

    var isInvalidValue = String(valueDateTime).toLowerCase() === 'invalid date';
    if (isInvalidValue) return '';

    var isInvalidDate = !nbMoment(valueDateTime).isValid();
    if (isInvalidDate) return;

    return nbMoment(valueDateTime).format('DD/MM/YYYY');
  },

  render: function() {
    var props = this.props;
    var nbProps = this.getFieldProperties();
    var statusLabel = nbProps.label.replace(nbProps.attr, props.field.publication.fieldStatus);
    var dateLabel = nbProps.label.replace(nbProps.attr, props.field.publication.fieldDateTime);

    var fieldDataTime = props.field.publication.fieldDateTime;
    var valueDateTime = props.model[fieldDataTime];
    var valueFormated = this.handleFormatDate(valueDateTime);

    var fieldStatus = props.field.publication.fieldStatus;
    var valueStatus = props.model[fieldStatus] ? props.model[fieldStatus] : props.field.publication.statusDefault;

    var options = props.field.publication.optionsStatus.map(function(status) {
      return {
        value: status,
        label: statusLabel.concat('.', status).translate()
      };
    });

    return (
      <div className="box">
        <div className="box-header with-border">
          <h3 className="box-title">
            { statusLabel.translate() }
          </h3>
        </div>

        <div className="box-body">
          <div className="form-group">
            <label htmlFor="inputEmail3" className="control-label">
              <i className="fa fa-map-signs fontSize11pxMiddle"></i>
              Status:
            </label>

            <FieldSelect
              className="form-control"
              placeholder="Status de publicação"
              options={options}
              value={valueStatus}
              onChange={this.handleChangeStatus}
            />
          </div>

          <div className="form-group">
            <label htmlFor="inputEmail3" className="control-label">
              <i className="fa fa-calendar-check-o fontSize11pxMiddle"></i>
              { dateLabel.translate() }
            </label>

            <input
              ref="inputLaunch"
              type="text"
              className="form-control"
              onChange={this.handleChangeDate}
              defaultValue={valueFormated}
            />
          </div>

          <div className="col-sm-12 form-group">
            <button type="submit" className="btn btn-block btn-primary btn-lg">
              Salvar
            </button>
          </div>

          <div className="col-sm-12 text-center">
            <a href="javascript:void(0);" onClick={this.handleOnCancel} className="text-muted">
              cancelar
            </a>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = FieldPublication;
