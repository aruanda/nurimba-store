'use strict';

var React = require('react');
var FieldMixin = require('nb-field');

var FieldEmail = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    var props = this.getFieldProperties();

    return (
      <div ref='containerField' className={props.className}>
        <label ref='labelField' className='control-label' htmlFor={props.htmlFor}>
          {props.label.translate()}
        </label>

        <div className='input-group'>
          <label htmlFor={props.htmlFor} className='input-group-addon'>
            <i className='fa fa-text-width'></i>
          </label>

          <input
            id={props.htmlFor}
            ref='emailField'
            readOnly={props.readOnly}
            defaultValue={props.value}
            onChange={this.handleChange}
            type='email'
            className='form-control'
            placeholder={props.pĺace.translate()}
            style={{ textTransform: 'lowercase' }}
          />
        </div>

        {props.message}
      </div>
    );
  }
});

module.exports = FieldEmail;
