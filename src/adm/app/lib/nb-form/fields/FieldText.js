/* globals MediumEditorTable: false */
'use strict';

var React = require('react');
var FieldMixin = require('nb-field');
var FieldInputArea = require('./FieldInputArea');
var FieldMixinText = require('nb-editor-text');
var ReactMediumEditor = require('react-medium-editor');
var linksIds = 0;

var FieldText = React.createClass({
  mixins: [FieldMixin, FieldMixinText],

  componentDidMount: function() {
    var fieldTxt = this;
    fieldTxt.prepareTextEditor();
  },

  componentWillUnmount: function() {
    var fieldTxt = this;
    fieldTxt.destroyTextEditor();
  },

  render: function() {
    var props    = this.getFieldProperties();
    var idHtml   = 'id_html_'.concat(++linksIds);
    var idEditor = 'id_editor_'.concat(++linksIds);
    var idDesign = 'id_Design_'.concat(++linksIds);

    return (
      <div className='nav-tabs-custom'>
        <ul className='nav nav-tabs pull-right'>
          <li>
            <a href={ '#'.concat(idEditor) } data-toggle='tab' aria-expanded='false'>
              Editor
            </a>
          </li>

          <li>
            <a href={ '#'.concat(idHtml) } data-toggle='tab' aria-expanded='false'>
              HTML
            </a>
          </li>

          <li className={'active'}>
            <a href={ '#'.concat(idDesign) } data-toggle='tab' aria-expanded='true'>
              Design
            </a>
          </li>

          <li className='pull-left header'><i className='fa fa-file-text-o'></i> Texto</li>
        </ul>

        <div className='tab-content'>
          <div className='tab-pane active' id={ idDesign }>
            <div className='form-group'>
              <ReactMediumEditor
                tag="p"
                text={props.value}
                onChange={this.handleChangeEditor}
                options={
                  {
                    placeholder: { text: '' },

                    toolbar: {
                      buttons: [
                        'h2',
                        'h3',
                        'bold',
                        'italic',
                        'underline',
                        'unorderedlist',
                        'orderedlist',
                        'strikethrough',
                        'quote',
                        'justifyLeft',
                        'justifyCenter',
                        'justifyRight',
                        'justifyFull',
                        'outdent',
                        'indent',
                        'anchor',
                        'image',
                        'table'
                      ]
                    },

                    buttonLabels: 'fontawesome',

                    extensions: {
                      table: new MediumEditorTable()
                    }
                  }
                }
              />
            </div>

            {props.message}
          </div>

          <div className='tab-pane' id={ idHtml }>
            <div className='form-group'>
              <FieldInputArea
                rows={5}
                className="form-control"
                placeholder={ props.place.translate() }
                value={props.value}
                onChange={this.handleChangeEditor}
              />
            </div>

            {props.message}
          </div>

          <div className='tab-pane' id={ idEditor }>
            <div className='form-group'>
              <div ref={'textEditor'} style={{ minHeight: '200px' }} dangerouslySetInnerHTML={{__html: props.value}}></div>
            </div>

            {props.message}
          </div>
        </div>
      </div>
    );
  }
});

module.exports = FieldText;
