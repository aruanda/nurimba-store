'use strict';

var React = require('react');
var FieldMixin = require('nb-field');

var FieldPrimary = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    var props = this.getFieldProperties();
    return (
      <div ref='containerField' className={props.className}>
        <label ref='labelField' className='control-label' htmlFor={props.htmlFor}>
          {props.label}
        </label>

        <div className='input-group'>
          <label htmlFor={props.htmlFor} className='input-group-addon'>
            <i className='fa fa-text-width'></i>
          </label>

          <input
            id={props.htmlFor}
            ref='stringField'
            readOnly={true}
            defaultValue={props.value}
            type='text'
            className='form-control'
            placeholder={props.pĺace}
          />
        </div>
      </div>
    );
  }
});

module.exports = FieldPrimary;
