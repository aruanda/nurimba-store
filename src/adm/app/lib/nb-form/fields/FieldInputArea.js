'use strict';

var React = require('react');

var FieldInputArea = React.createClass({
  componentDidUpdate: function() {
    var isNotDiffValue = Boolean(this.refs.inputarea.value === this.props.value);
    if (isNotDiffValue) return;

    var value = [null, undefined].indexOf(this.props.value) === -1 ? this.props.value : '';
    this.refs.inputarea.value = value;
  },

  componentChangeValue: function(e) {
    var value = e.target.value;
    var isNotDiffValue = Boolean(value === this.props.value);
    if (isNotDiffValue) return;

    this.props.onChange(value);
  },

  render: function() {
    var props = this.props;

    return (
      <div className="form-group">
        <textarea
          id={props.id}
          ref={'inputarea'}
          rows={props.rows}
          onChange={this.componentChangeValue}
          style={props.style}
          className={props.className}
          placeholder={props.placeholder}
        />
      </div>
    );
  }
});

module.exports = FieldInputArea;
