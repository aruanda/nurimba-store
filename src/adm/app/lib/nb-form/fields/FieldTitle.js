'use strict';

var React      = require('react');
var nbSlug     = require('nb-slug');
var nbLink     = require('nb-link');
var FieldMixin = require('nb-field');
var FieldInput = require('./FieldInput');

var FieldTitle = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    var props = this.getFieldProperties();
    var pathSlug = props.field.pathSlug;
    var model = this.props.model;
    var id = model && model.id ? ('-' + String(model.id)) : '';
    var slug = pathSlug.concat(nbSlug(props.value) + id);
    var link;

    if (id) {
      link = (
        <a href={nbLink(slug)} target="_blanck" title={props.value}>
          {nbLink(slug)}
        </a>
      );
    } else {
      link = (
        <span className="text-light-blue">
          {nbLink(slug)}
        </span>
      );
    }

    return (
      <div className="nav-tabs-custom">
        <FieldInput
          type="text"
          className="form-control input-lg"
          placeholder={ props.place.translate() }
          value={props.value}
          style={{ border: '0' }}
          onChange={this.registerValue}
        />

        {props.message || (
          <div className="box-body">{link}</div>
        )}
      </div>
    );
  }
});

module.exports = FieldTitle;
