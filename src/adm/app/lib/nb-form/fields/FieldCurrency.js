/*globals $:false*/
'use strict';

var React = require('react');
var FieldMixin = require('nb-field');
var nbNumeral = require('nb-numeral');

var FieldCurrency = React.createClass({
  mixins: [FieldMixin],

  componentDidMount: function() {
    var field = this;
    var el = $(field.refs.currencyField);

    el.maskMoney({
      prefix: 'R$'
    }).off('keyup').on('keyup', function() {
      var value = String(el[0].value || '');
      if (!value.length) value = undefined;

      var isZero = !value || (value.toLowerCase() === 'r$0,00'.toLowerCase());
      value = isZero ? 0 : nbNumeral().unformat(value);

      field.registerValue(value);
    });
  },

  componentDidUpdate: function() {
    var props = this.getFieldProperties();
    var value;

    if (props.value) {
      props.value = parseFloat(props.value, 10);
      value = nbNumeral(props.value).format('($0,0.00)');
    } else {
      value = '';
    }

    var isNotDiffValue = Boolean(this.refs.currencyField.value === value);
    if (isNotDiffValue) return;

    this.refs.currencyField.value = value;
  },

  componentWillUnmount: function() {
    $(this.refs.currencyField).maskMoney('destroy');
  },

  render: function() {
    var props = this.getFieldProperties();
    var value;

    if (props.value) {
      props.value = parseFloat(props.value, 10);
      value = nbNumeral(props.value).format('($0,0.00)');
    }

    return (
      <div className="box">
        <div className="box-header with-border">
          <h3 className="box-title">
            { props.label.translate() }
          </h3>
        </div>

        <div className="box-body">
          <input
            type='text'
            ref='currencyField'
            defaultValue={value}
            className="form-control"
            placeholder={ props.place.translate() }
            style={{ textTransform: 'uppercase' }}
            data-affixes-stay={ true }
            data-thousands={ '.' }
            data-decimal={ ',' }
            maxLength={ 21 }
          />
        </div>

        {props.message}
      </div>
    );
  }
});

module.exports = FieldCurrency;
