'use strict';

var React      = require('react');
var FieldMixin = require('nb-field');
var FieldInput = require('./FieldInput');
var htmlForId  = 0;

var FieldGroup = React.createClass({
  mixins: [FieldMixin],

  handleChangeValue: function(attr, value) {
    var field = this;
    if (!field.props.model) field.props.model = {};
    var model = field.props.model;
    model[attr] = value;
  },

  mountInput: function(input, index) {
    var group = this;
    var props = group.getFieldProperties();
    var model = group.props.model;

    var htmlFor = props.label.concat(input.attr, group.htmlForId);
    var inputValue = model.hasOwnProperty(input.attr) && model[input.attr] ? model[input.attr] : undefined;

    var inputChange = function(value) {
      group.handleChangeValue(input.attr, value);
    };

    return (
      <div key={index} className={input.classBox}>
        <label htmlFor={htmlFor}>
          { props.label.replace(props.attr, input.attr).translate() }
        </label>

        <div className="input-group">
          <label htmlFor={htmlFor} className="input-group-addon">
            <i className={input.icon}></i>
          </label>

          <FieldInput
            id={htmlFor}
            type="text"
            className="form-control"
            placeholder={ props.place.replace(props.attr, input.attr).translate() }
            value={inputValue}
            onChange={inputChange}
          />
        </div>
      </div>
    );
  },

  render: function() {
    var group  = this;
    group.htmlForId = ++htmlForId;
    var props  = group.getFieldProperties();
    var inputs = group.props.field.group.inputs.map(group.mountInput);

    return (
      <div className="nav-tabs-custom">
        <ul className="nav nav-tabs pull-right">
          <li className="pull-left header">
            <i className="fa fa-user"></i>
            { props.label.translate() }
          </li>
        </ul>

        <div className="tab-content overflowHidden">
          {inputs}
        </div>
      </div>
    );
  }
});

module.exports = FieldGroup;
