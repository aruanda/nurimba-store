'use strict';

var React = require('react');
var FieldMixin = require('nb-field');

var FieldString = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    var props = this.getFieldProperties();

    return (
      <div ref='containerField' className={props.className}>
        <label ref='labelField' className='control-label' htmlFor={props.htmlFor}>
          {props.label.translate()}
        </label>

        <div className='input-group'>
          <label htmlFor={props.htmlFor} className='input-group-addon'>
            <i className='fa fa-text-width'></i>
          </label>

          <input
            id={props.htmlFor}
            ref='stringField'
            readOnly={props.readOnly}
            defaultValue={props.value}
            onChange={this.props.onChange || this.handleChange}
            type='text'
            className='form-control'
            placeholder={props.place.translate()}
          />
        </div>

        {props.message}
      </div>
    );
  }
});

module.exports = FieldString;
