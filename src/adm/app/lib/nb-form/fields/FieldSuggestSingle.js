'use strict';

var React = require('react');
var FieldMixin = require('nb-field');
var ReactSelect = require('react-select');
var nbConnection = require('nb-connection');

var FieldSuggestSingle = React.createClass({
  mixins: [FieldMixin],

  handleOnUpdate: function(val) {
    this.registerValue(val);
  },

  getOptions: function(input) {
    var props = this.getFieldProperties();

    var field = props.field;
    var domain = field.suggest.domain;
    var backend = nbConnection.getInstance();

    var filterParams = { limit: 10 };
    if (input) filterParams.conditions =  [{
      field: field.suggest.label,
      value: input,
      comparator: 'like'
    }];

    var finish = function(data) {
      var options = [];

      data.forEach(function(row) {
        row.value = row.id;
        row.label = row[field.suggest.label];
        options.push(row);
      });

      return { options: options };
    };

    return backend.all(domain).get(filterParams).then(function(res) {
      return res.body.rows ? res.body.data : [];
    }).then(finish);
  },

  render: function() {
    var props = this.getFieldProperties();
    return (
      <div className="box">
        <div className="box-header with-border">
          <h3 className="box-title">
            {props.label.translate()}
          </h3>
        </div>

        <div className="box-body">
          <ReactSelect.Async
            id={props.htmlFor}
            ref='suggestField'
            name={props.htmlFor}
            value={props.value}
            multi={false}
            readOnly={props.readOnly}
            onChange={this.handleOnUpdate}
            loadOptions={this.getOptions}
            placeholder={props && String(props.place || '').trim() ? String(props.place).trim().translate() : '----'}
          />
        </div>

        {props.message}
      </div>
    );
  }
});

module.exports = FieldSuggestSingle;
