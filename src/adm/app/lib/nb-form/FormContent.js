'use strict';

var q = require('q');
var React = require('react');
var FormFields = require('./FormFields');
var FieldMixin = require('nb-field');

var FormContent = React.createClass({
  handleSubmit: function(e) {
    e.preventDefault();
    var fields = this.props.fields;
    var model = this.props.model || {};

    var check = function(fieldConfig) {
      var fieldAttr = fieldConfig.attr;
      var fieldValue = Boolean(model && model.hasOwnProperty(fieldAttr) && model[fieldAttr]) ? model[fieldAttr] : undefined;
      var fieldChecks = fieldConfig.validations;

      return FieldMixin.checkState(fieldChecks, fieldValue).then(function(state) {
        model._extra[fieldAttr] = state;
      }).catch(function(state) {
        model._extra[fieldAttr] = state;
        throw model._extra[fieldAttr];
      });
    };

    var checks = q.all(fields.map(check));

    var sucessForm = function() {
      var modelArgs = {};

      fields.forEach(function(cfg) {
        modelArgs[cfg.attr] = model[cfg.attr];
      });

      if (this.props.onSubmit) this.props.onSubmit(modelArgs);
    }.bind(this);

    var errorForm = function() {
      this.forceUpdate();
    }.bind(this);

    checks
      .then(sucessForm)
      .catch(errorForm);

    return false;
  },

  render: function() {
    if (!this.props.model) this.props.model = {};
    var model = this.props.model;
    var fields = this.props.fields;

    var viewFields = fields.filter(function(field) {
      var isNotIdField = Boolean(field.attr !== 'id');
      var modelWithId = Boolean(field.attr === 'id' && model.hasOwnProperty(field.attr) && model[field.attr]);
      var filterField = isNotIdField || modelWithId;
      return filterField;
    });

    return (
      <form role="form" onSubmit={this.handleSubmit}>
        <FormFields
          model={model}
          fields={viewFields}
        />
      </form>
    );
  }
});

module.exports = FormContent;
