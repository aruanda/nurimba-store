'use strict';

var React = require('react');
var FieldMixin = require('nb-field');

var ViewSEO = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    var props = this.getViewProperties();
    var seoArgs = props.value || {};

    if (!seoArgs.facebookImage || !seoArgs.facebookImage.hasOwnProperty('src')) seoArgs.facebookImage = { src: 'https://placeholdit.imgix.net/~text?txtsize=33&txt=image&w=600&h=400' };
    if (!seoArgs.twitterImage  || !seoArgs.twitterImage.hasOwnProperty('src'))  seoArgs.twitterImage  = { src: 'https://placeholdit.imgix.net/~text?txtsize=33&txt=image&w=600&h=400' };
    if (!seoArgs.googleImage   || !seoArgs.googleImage.hasOwnProperty('src'))   seoArgs.googleImage   = { src: 'https://placeholdit.imgix.net/~text?txtsize=33&txt=image&w=600&h=400' };

    return (
      <div>
        <div className="nav-tabs-custom">
          <div className="tab-content">
            <div className="form-group">
              <label>Description</label>
              <p>{seoArgs.description}</p>
            </div>
            <div className="form-group">
              <label>Keywords</label>
              <p>{seoArgs.keywords}</p>
            </div>
            <div className="form-group">
              <label>Indexação</label>
              <p>{seoArgs.robots}</p>
            </div>
          </div>
        </div>

        <div className="nav-tabs-custom">
          <div className="tab-content overflowHidden">
            <div className="box-body col-md-4">
              <div className="col-xs-12 bg-blue-active">
                <i className="fa fa-facebook-official fontSize18emPadding10px10px10px0"></i>
                <span className="midiaSocial">Facebook</span>
              </div>

              <div className="col-xs-12 attachment-block clearfix padding15px">
                <img
                  src={seoArgs.facebookImage.src}
                  alt={seoArgs.facebookImage.alt}
                  className="img-responsive"
                />

                <div className="marginTop20px">
                  <h4 className="attachment-heading text-light-blue">
                    {seoArgs.facebookTitle}
                  </h4>

                  <div className="attachment-tex overflowHidden">
                    <p>{seoArgs.facebookDescription}</p>
                  </div>
                </div>
              </div>
            </div>

            <div className="box-body col-md-4">
              <div className="col-xs-12 bg-aqua">
                <i className="fa fa-twitter fontSize18emPadding10px10px10px0"></i>
                <span className="midiaSocial">Twitter</span>
              </div>

              <div className="col-xs-12 attachment-block clearfix padding15px">
                <img
                  src={seoArgs.twitterImage.src}
                  alt={seoArgs.twitterImage.alt}
                  className="img-responsive"
                />

                <div className="marginTop20px">
                  <h4 className="attachment-heading text-light-blue">
                    {seoArgs.twitterTitle}
                  </h4>

                  <div className="attachment-tex overflowHidden">
                    <p>{seoArgs.twitterDescription}</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="box-body col-md-4">
              <div className="col-xs-12 bg-red-active">
                <i className="fa fa-google-plus fontSize18emPadding10px10px10px0"></i> <span className="midiaSocial">Google Plus</span>
              </div>
              <div className="col-xs-12 attachment-block clearfix padding15px">
                <img
                  src={seoArgs.googleImage.src}
                  alt={seoArgs.googleImage.alt}
                  className="img-responsive"
                />

                <div className="marginTop20px">
                  <h4 className="attachment-heading text-light-blue">
                    {seoArgs.googleTitle}
                  </h4>

                  <div className="attachment-tex overflowHidden">
                    <p>{seoArgs.googleDescription}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

module.exports = ViewSEO;
