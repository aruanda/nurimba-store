'use strict';

var React = require('react');

var ViewGroup = React.createClass({
  mountView: function(input, index) {
    var group = this;
    var model = group.props.model;

    var inputValue = model.hasOwnProperty(input.attr) && model[input.attr] ? model[input.attr] : undefined;

    if (!index) return <span key={index}><b>{inputValue}</b><br /></span>;
    if (!inputValue) return;

    return (
      <small key={index} style={{ display: 'inline-block', minWidth: '43%', marginRight: '5%', marginTop: '10px' }}>
        <span style={{ marginRight: '10px !important' }} className={input.icon}></span>
        {inputValue}
      </small>
    );
  },

  render: function() {
    var group  = this;
    var views = group.props.view.group.inputs.map(group.mountView);

    return (
      <div className="nav-tabs-custom padding1px15px10px">
        <h3 className="margin10px00">{views}</h3>
      </div>
    );
  }
});

module.exports = ViewGroup;
