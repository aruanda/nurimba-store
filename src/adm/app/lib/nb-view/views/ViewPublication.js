'use strict';

var React = require('react');
var nbMoment = require('nb-moment');
var FieldMixin = require('nb-field');

var ViewPublication = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    var view  = this.props.view;
    var model = this.props.model;
    var nbProps = this.getViewProperties();

    var fieldStatus   = view.publication.fieldStatus;
    var fieldDateTime = view.publication.fieldDateTime;
    var optionsStatus = view.publication.optionsStatus;

    var withStatus   = Boolean(model && model.hasOwnProperty(fieldStatus)   && model[fieldStatus]);
    var withDateTime = Boolean(model && model.hasOwnProperty(fieldDateTime) && model[fieldDateTime]);

    var status   = withStatus ? model[fieldStatus] : optionsStatus[0];
    var dateTime = withDateTime ? nbMoment(model[fieldDateTime]).format('dddd, LL') : 'date.not.filled';
    var color    = status === 'online' ? 'green' : 'red';

    var statusLabel = nbProps.label.replace(nbProps.attr, fieldStatus).concat('.', status).translate();

    return (
      <div className="nav-tabs-custom">
        <div className="tab-content">
          <p>
            <i className={ 'fa fa-circle fontSize06em text-'.concat(color) }></i>
            &nbsp;&nbsp;
            {statusLabel}
          </p>

          <p>
            Publicação: {dateTime.translate()}
          </p>
        </div>
      </div>
    );
  }
});

module.exports = ViewPublication;
