'use strict';

var React      = require('react');
var nbLink     = require('nb-link');
var nbSlug     = require('nb-slug');
var FieldMixin = require('nb-field');

var ViewTitle = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    var props = this.getViewProperties();
    var pathSlug = props.field.pathSlug;
    var model = this.props.model;
    var id = model && model.id ? ('-' + String(model.id)) : '';
    var slug = pathSlug.concat(nbSlug(props.value) + id);

    var link = (
      <a href={nbLink(slug)} target="_blanck" title={props.value}>
        {nbLink(slug)}
      </a>
    );

    var notFilled = [null, undefined, ''].indexOf(props.value) > -1;
    var value = notFilled
      ? <span style={{ color: '#8A8A8A' }}>Título não informado</span>
      : <h1>{props.value}</h1>;

    return (
      <div className={'nav-tabs-custom padding1px15px10px'}>
        {value}
        {link}
      </div>
    );
  }
});

module.exports = ViewTitle;
