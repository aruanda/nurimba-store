'use strict';

var React = require('react');
var FieldMixin = require('nb-field');

var ViewCustomer = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    return (
      <div className="nav-tabs-custom padding1px15px10px">
        <h2>
          João Moureira
          <br />
          <small><span className="fa fa-whatsapp"></span> 67-9999-9999</small>&nbsp;&nbsp;&nbsp;
          <small><span className="fa fa-envelope-o"></span> joão@gmail.com</small>
          <br />
          <small><span className="fa fa-home"></span> Rua do Oeste Vermelho, 189</small>&nbsp;&nbsp;&nbsp;
          <small><span className="fa fa-truck"></span> 79013-300</small>&nbsp;&nbsp;&nbsp;
          <small><span className="fa fa-map-o"></span> Florianópolis, SC</small>&nbsp;&nbsp;&nbsp;
        </h2>
      </div>
    );
  }
});

module.exports = ViewCustomer;
