'use strict';

var React      = require('react');
var copy       = require('copy-to-clipboard');
var nbLink     = require('nb-link');
var FieldMixin = require('nb-field');

var ViewImages = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    var props = this.getViewProperties();
    if (!props.value) props.value = [];
    var withoutValue = Boolean(!props.value || !props.value.length);
    if (withoutValue) return <div></div>;

    return (
      <div className="nav-tabs-custom">
        <div className="tab-content overflowHidden">
          <ul className="mailbox-attachments">
            {props.value.map(function(file, index) {
              file.preview = nbLink('files/' + file.filetag + '/' + file.filename);

              var copyImage = function() {
                copy(file.preview);
              };

              return (
                <li key={index}>
                  <span className="mailbox-attachment-icon has-img imgMax132px">
                    <img
                      src={file.preview}
                      alt="{file.filetitle}"
                    />
                  </span>

                  <div className="mailbox-attachment-info">
                    <button onClick={copyImage} type="button" className="btn btn-primary pull-right btn-xs">
                      <i  className="fa fa-copy"></i>
                      copiar url
                    </button>

                    <a href={file.preview} target="__blank" className="mailbox-attachment-name">
                      <i  className="fa fa-camera"></i>
                      {file.filetitle}
                    </a>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    );
  }
});

module.exports = ViewImages;
