'use strict';

var React = require('react');
var FieldMixin = require('nb-field');

var ViewText = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    var props = this.getViewProperties();

    var notFilled = [null, undefined, ''].indexOf(props.value) > -1 || !String(props.value).trim();

    var value = notFilled
      ? <span style={{ color: '#8A8A8A', textAlign: 'center' }}>Texto não informado</span>
      : <div dangerouslySetInnerHTML={{ __html: props.value }}></div>;

    return (
      <div className="nav-tabs-custom">
        <div className="tab-content">
          {value}
        </div>
      </div>
    );
  }
});

module.exports = ViewText;
