'use strict';

var React = require('react');
var FieldMixin = require('nb-field');
var nbNumeral = require('nb-numeral');

var ViewTitle = React.createClass({
  mixins: [FieldMixin],

  render: function() {
    var props = this.getViewProperties();
    var valFormated = props.value ? nbNumeral(props.value).format('($0,0.00)') : <span style={{ color: '#8A8A8A', textAlign: 'center' }}>Valor não informado</span>;

    return (
      <div className="nav-tabs-custom">
        <div className="tab-content">
          {valFormated}
        </div>
      </div>
    );
  }
});

module.exports = ViewTitle;
