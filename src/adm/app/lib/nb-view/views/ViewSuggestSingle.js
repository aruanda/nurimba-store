'use strict';

var React = require('react');

var ViewSuggestSingle = React.createClass({
  render: function() {
    var props = this.props;
    var view  = props.view;
    var model = props.model;
    var value = model[view.attr];

    if (!value) return <span></span>;

    return (
      <div className="nav-tabs-custom">
        <div className="tab-content">
          <span className={'label label-primary'}>
            {value.label}
          </span>
        </div>
      </div>
    );
  }
});

module.exports = ViewSuggestSingle;
