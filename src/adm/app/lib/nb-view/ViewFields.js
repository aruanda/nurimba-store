'use strict';

var React = require('react');

var typesViews = {
  seo: require('./views/ViewSEO'),
  text: require('./views/ViewText'),
  email: require('./views/ViewEmail'),
  title: require('./views/ViewTitle'),
  group: require('./views/ViewGroup'),
  string: require('./views/ViewString'),
  images: require('./views/ViewImages'),
  integer: require('./views/ViewInteger'),
  primary: require('./views/ViewPrimary'),
  textarea: require('./views/ViewText'),
  currency: require('./views/ViewCurrency'),
  imageMain: require('./views/ViewImageMain'),
  publication: require('./views/ViewPublication'),
  suggestSingle: require('./views/ViewSuggestSingle'),
  suggestMultiple: require('./views/ViewSuggestMultiple')
};

var ViewFields = React.createClass({
  render: function () {
    var form  = this;
    var model = form.props.model;

    var cols = {
      'firstCol': [],
      'secondCol': []
    };

    this.props.views.forEach(function(view) {
      if (!typesViews.hasOwnProperty(view.kind)) return;

      view.label = view.domain.concat('.view.', view.attr).translate();

      var View = typesViews[view.kind];
      cols[view.colName].push(
        <View
          key={cols[view.colName].length}
          view={view}
          model={model}
        />
      );
    });

    return (
      <div className="row">
        <div className="col-md-8">
          {cols.firstCol}
        </div>

        <div className="col-md-4">
          {cols.secondCol}
        </div>
      </div>
    );
  }
});

module.exports = ViewFields;
