'use strict';

var moment = require('moment');
require('moment/i18n/pt-br');
moment.locale('pt-br');

module.exports = moment;
