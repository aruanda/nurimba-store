'use strict';

String.prototype.translate = function() {
  var key = this;
  var degaultLang = 'br';

  var i18n = {
    en: {
      msgErrorEmail:         'This email is invalid',
      msgErrorRequired:      'This field is required',
      msgErrorLessThan:      'This number is less than {0}',
      msgErrorBiggerThan:    'This number is bigger than {0}',
      msgErrorMultipleEmail: 'This email is invalid: "{0}"'
    },

    br: {
      msgErrorEmail:         'Este e-mail é inválido',
      msgErrorRequired:      'Este campo é obrigatório',
      msgErrorLessThan:      'Este número é menor que {0}',
      msgErrorBiggerThan:    'Este número é maior que {0}',
      msgErrorMultipleEmail: 'Este e-mail é inválido: "{0}"',

      '/': '/admin',

      'menu.loja': 'Loja',
      'menu.loja.ver': 'Ver loja',
      'menu.site': 'Site',
      'menu.site.ver': 'Ver site',
      'date.not.filled': 'data não informada',

      'sites.menu': 'Site',
      'sites.menu.ver': 'Ver site',
      'blogs.menu': 'Blog',
      'blogs.menu.ver': 'Ver blog',

      'table.notFound': 'Nenhum registro encontrado',

      'images.properties.title': 'Propriedades da imagem',

      'properties.btn.cancel': 'Cancelar',
      'properties.btn.save': 'Aplicar mudanças',
      'properties.label.src': 'Link da imagem',
      'properties.label.alt': 'Descrição',
      'properties.label.width': 'Largura px',
      'properties.label.height': 'Altura px',
      'properties.label.keywords': 'Palavras chaves',
      'properties.label.description': 'Descrição',
      'properties.label.robots': 'Indexação',

      'properties.place.src': 'Link da imagem',
      'properties.place.alt': 'Descrição',
      'properties.place.width': 'em pixels',
      'properties.place.height': 'em pixels',
      'properties.place.keywords': 'Digite as palavras chaves',
      'properties.place.description': 'Digite a descrição para SEO',
      'properties.place.robots': 'Meta tags do Engine Search',

      'table.loading': 'Carregando registros',
      'error.not.found': 'Registro não encontrado',

      'seo.twitter.label': 'Twitter',
      'seo.twitter.title': 'Informe aqui o título para o bloco do Twitter',
      'seo.twitter.description': 'Informe aqui a descrição para o bloco do Twitter',

      'seo.facebook.label': 'Facebook',
      'seo.facebook.title': 'Informe aqui o título para o bloco do Facebook',
      'seo.facebook.description': 'Informe aqui a descrição para o bloco do Facebook',

      'seo.google.label': 'Google Plus',
      'seo.google.title': 'Informe aqui o título para o bloco do Google+',
      'seo.google.description': 'Informe aqui a descrição para o bloco do Google+',

      '/login': '/entrar',
      '/remember': '/lembrar-senha',
      '/add':  '/adicionar',
      '/view': '/visualizar',
      '/edit': '/alterar',

      /* -----------------------------------*/

      '/config': '/configuracoes',
      'configs.menu': 'Configurações',

      'configs.btn.view.edit':   'Editar',
      'configs.btn.view.list':   'voltar para listagem',
      'configs.btn.view.cancel': 'cancelar',

      'configs.waiting.view.title': 'Aguarde!',
      'configs.waiting.view.message': 'Carregando configuração, o tempo vai depender de sua conexão',

      'configs.waiting.form.title.edit': 'Aguarde!',
      'configs.waiting.form.message.edit': 'Carregando configuração, o tempo vai depender de sua conexão',

      'configs.waiting.form.title.savingEdit': 'Aguarde!',
      'configs.waiting.form.message.savingEdit': 'Salvando alterações da configuração, o tempo vai depender de sua conexão',

      'configs.waiting.form.title.savingNew': 'Aguarde!',
      'configs.waiting.form.message.savingNew': 'Salvando nova configuração, o tempo vai depender de sua conexão',

      'configs.alert.form.title.savingNew': 'Registro salvo com sucesso',
      'configs.alert.form.message.savingNew': 'Configuração cadastrada com sucesso',

      'configs.alert.form.title.savingEdit': 'Alteração bem sucedida',
      'configs.alert.form.message.savingEdit': 'Configuração alterada com sucesso',

      'configs.alert.view.title': 'Erro ao carregar configuração',
      'configs.alert.error.title.savingNew': 'Erro ao salvar nova configuração',
      'configs.alert.error.title.savingEdit': 'Erro ao salvar alterações da configuração',

      'configs.form.title':               'Configurações',
      'configs.form.label.confweb':       'Dados Gerais',
      'configs.form.label.confname':      'Seção',
      'configs.form.label.conffacebook':  'Facebook',
      'configs.form.label.confgoogle':    'Goole Plus',
      'configs.form.label.confwhatsapp':  'Whatsapp',
      'configs.form.label.conftwitter':   'Twitter',
      'configs.form.label.confinstagram': 'Instagram',
      'configs.form.label.confemail':     'E-mail',
      'configs.form.label.confaddress':   'Endereço',
      'configs.form.label.confcep':       'CEP',
      'configs.form.label.confstatecity': 'Estado/Cidade',

      'configs.form.place.confname':      'Informe um nome para configuração',
      'configs.form.place.conffacebook':  'Informe o facebook',
      'configs.form.place.confgoogle':    'Informe o goole Plus',
      'configs.form.place.confwhatsapp':  'Informe o whatsapp',
      'configs.form.place.conftwitte':    'Informe o twitter',
      'configs.form.place.confinstagram': 'Informe o instagram',
      'configs.form.place.confemail':     'Informe o e-mail',
      'configs.form.place.confaddress':   'Informe o endereço',
      'configs.form.place.confcep':       'Informe o cep',
      'configs.form.place.confstatecity': 'Informe o estado/cidade',

      'configs.form.label.conflaunch':            'Data Previsão/Publicação',
      'configs.form.label.confstatus':            'Status Web',
      'configs.form.label.confstatus.construcao': 'Em construção',
      'configs.form.label.confstatus.manutencao': 'Em manutenção',
      'configs.form.label.confstatus.online':     'Online',

      'configs.view.confstatus':            'Status Web',
      'configs.view.confstatus.construcao': 'Em construção',
      'configs.view.confstatus.manutencao': 'Em manutenção',
      'configs.view.confstatus.online':     'Online',

      'configs.form.place.confacervo': 'Arraste as imagens aqui ou click aqui para escolher as imagens',

      'configs.list.title': 'Configurações',
      'configs.list.confweb': 'Seções',
      'configs.list.view': 'Visualizar configurações',
      'configs.list.edit': 'Editar configurações',

      /* -----------------------------------*/

      '/tags': '/tags',
      'tags.menu': 'Tags',
      'tags.btn.new': 'Nova Tag',
      'tags.list.view': 'Visualizar tag',
      'tags.list.edit': 'Alterar tag',
      'tags.form.title': 'Tag',

      'tags.waiting.view.title': 'Aguarde!',
      'tags.waiting.view.message': 'Carregando tag, o tempo vai depender de sua conexão',

      'tags.waiting.form.title.edit': 'Aguarde!',
      'tags.waiting.form.message.edit': 'Carregando tag, o tempo vai depender de sua conexão',

      'tags.waiting.form.title.savingEdit': 'Aguarde!',
      'tags.waiting.form.message.savingEdit': 'Salvando alterações da Tag, o tempo vai depender de sua conexão',

      'tags.waiting.form.title.savingNew': 'Aguarde!',
      'tags.waiting.form.message.savingNew': 'Salvando nova Tag, o tempo vai depender de sua conexão',

      'tags.alert.form.title.savingNew': 'Registro salvo com sucesso',
      'tags.alert.form.message.savingNew': 'Tag cadastrado com sucesso',

      'tags.alert.form.title.savingEdit': 'Alteração bem sucedida',
      'tags.alert.form.message.savingEdit': 'Tag alterada com sucesso',

      'tags.alert.view.title': 'Erro ao carregar tag',
      'tags.alert.error.title.savingNew': 'Erro ao salvar nova tag',
      'tags.alert.error.title.savingEdit': 'Erro ao salvar alterações da tag',

      'tags.btn.view.new':    'nova tag',
      'tags.btn.view.edit':   'Editar',
      'tags.btn.view.list':   'voltar para listagem',
      'tags.btn.view.page':   'ver no blog',
      'tags.btn.view.cancel': 'cancelar',

      'tags.btn.form.save':  'Salvar',
      'tags.btn.form.cancel': 'Cancelar',

      'tags.list.title': 'Tags',
      'tags.view.title': 'Tag',
      'tags.form.title.new':  'Nova Tag',
      'tags.form.title.edit': 'Alterar Tag',

      'tags.view.id':        'Código',
      'tags.view.tagname':   'Tag',
      'tags.view.tagparent': 'Tag pai',
      'tags.view.tagdesc':   'Descrição',
      'tags.view.tagseo':    'SEO',

      'tags.form.label.id':        'Código',
      'tags.form.label.tagname':   'Tag',
      'tags.form.label.tagparent': 'Tag pai',
      'tags.form.label.tagdesc':   'Descrição',
      'tags.form.label.tagacervo': 'Imagens',

      'tags.form.place.id':        '######',
      'tags.form.place.tagname':   'Digite aqui um nome para a tag',
      'tags.form.place.tagparent': 'Informe aqui a tag pai',
      'tags.form.place.tagdesc':   'Informe aqui uma descrição para tag',
      'tags.form.place.tagacervo': 'Arraste as imagens aqui ou click aqui para escolher as imagens',

      'tags.list.id':        'Código',
      'tags.list.tagname':   'Tag',
      'tags.list.tagparent': 'Tag pai',
      'tags.list.tagdesc':   'Descrição',

      'tags.form.label.taglaunch':          'Data',
      'tags.form.label.tagstatus':          'Publicação',
      'tags.form.label.tagstatus.offline':  'Offline',
      'tags.form.label.tagstatus.rascunho': 'Rascunho',
      'tags.form.label.tagstatus.online':   'Online',

      'tags.view.tagstatus':          'Publicação',
      'tags.view.tagstatus.offline':  'Offline',
      'tags.view.tagstatus.rascunho': 'Rascunho',
      'tags.view.tagstatus.online':   'Online',

      /* -----------------------------------*/

      '/session':            '/sessoes',
      'sessions.menu':       'Sessões',
      'sessions.btn.new':    'Nova Sessão',
      'sessions.list.view':  'Visualizar sessão',
      'sessions.list.edit':  'Alterar sessão',
      'sessions.form.title': 'Sessão',

      'sessions.waiting.view.title':   'Aguarde!',
      'sessions.waiting.view.message': 'Carregando sessão, o tempo vai depender de sua conexão',

      'sessions.waiting.form.title.edit':   'Aguarde!',
      'sessions.waiting.form.message.edit': 'Carregando sessão, o tempo vai depender de sua conexão',

      'sessions.waiting.form.title.savingEdit':   'Aguarde!',
      'sessions.waiting.form.message.savingEdit': 'Salvando alterações da Sessão, o tempo vai depender de sua conexão',

      'sessions.waiting.form.title.savingNew':   'Aguarde!',
      'sessions.waiting.form.message.savingNew': 'Salvando nova Sessão, o tempo vai depender de sua conexão',

      'sessions.alert.form.title.savingNew':   'Registro salvo com sucesso',
      'sessions.alert.form.message.savingNew': 'Sessão cadastrado com sucesso',

      'sessions.alert.form.title.savingEdit':   'Alteração bem sucedida',
      'sessions.alert.form.message.savingEdit': 'Sessão alterada com sucesso',

      'sessions.alert.view.title':             'Erro ao carregar sessão',
      'sessions.alert.error.title.savingNew':  'Erro ao salvar nova sessão',
      'sessions.alert.error.title.savingEdit': 'Erro ao salvar alterações da sessão',

      'sessions.btn.view.new':    'nova sessão',
      'sessions.btn.view.edit':   'Editar',
      'sessions.btn.view.list':   'voltar para listagem',
      'sessions.btn.view.page':   'ver no site',
      'sessions.btn.view.cancel': 'cancelar',

      'sessions.btn.form.save':   'Salvar',
      'sessions.btn.form.cancel': 'Cancelar',

      'sessions.list.title':      'Sessões',
      'sessions.view.title':      'Sessão',
      'sessions.form.title.new':  'Nova Sessão',
      'sessions.form.title.edit': 'Alterar Sessão',

      'sessions.view.id':         'Código',
      'sessions.view.sessname':   'Sessão',
      'sessions.view.sessparent': 'Sessão pai',
      'sessions.view.sessdesc':   'Descrição',
      'sessions.view.sessseo':    'SEO',

      'sessions.form.label.id':         'Código',
      'sessions.form.label.sessname':   'Sessão',
      'sessions.form.label.sessparent': 'Sessão pai',
      'sessions.form.label.sessdesc':   'Descrição',
      'sessions.form.label.sessacervo': 'Imagens',
      'sessions.form.label.sessresume': 'Resumo',

      'sessions.form.place.id':         '######',
      'sessions.form.place.sessname':   'Digite aqui um nome para a sessão',
      'sessions.form.place.sessparent': 'Informe aqui a sessão pai',
      'sessions.form.place.sessdesc':   'Informe aqui uma descrição para sessão',
      'sessions.form.place.sessacervo': 'Arraste as imagens aqui ou click aqui para escolher as imagens',
      'sessions.form.place.sessresume': 'Informe o resumo dessa sessão',

      'sessions.list.id':         'Código',
      'sessions.list.sessname':   'Sessão',
      'sessions.list.sessparent': 'Sessão pai',
      'sessions.list.sessdesc':   'Descrição',

      'sessions.form.label.sesslaunch':          'Data',
      'sessions.form.label.sessstatus':          'Publicação',
      'sessions.form.label.sessstatus.offline':  'Offline',
      'sessions.form.label.sessstatus.rascunho': 'Rascunho',
      'sessions.form.label.sessstatus.online':   'Online',

      'sessions.view.sessstatus':          'Publicação',
      'sessions.view.sessstatus.offline':  'Offline',
      'sessions.view.sessstatus.rascunho': 'Rascunho',
      'sessions.view.sessstatus.online':   'Online',

      /* -----------------------------------*/

      '/categories': '/categorias',
      'categories.menu': 'Categorias',
      'categories.btn.new': 'Nova Categoria',
      'categories.list.view': 'Visualizar categoria',
      'categories.list.edit': 'Alterar categoria',
      'categories.form.title': 'Categoria',

      'categories.waiting.view.title': 'Aguarde!',
      'categories.waiting.view.message': 'Carregando categoria, o tempo vai depender de sua conexão',

      'categories.waiting.form.title.edit': 'Aguarde!',
      'categories.waiting.form.message.edit': 'Carregando categoria, o tempo vai depender de sua conexão',

      'categories.waiting.form.title.savingEdit': 'Aguarde!',
      'categories.waiting.form.message.savingEdit': 'Salvando alterações da Categoria, o tempo vai depender de sua conexão',

      'categories.waiting.form.title.savingNew': 'Aguarde!',
      'categories.waiting.form.message.savingNew': 'Salvando nova Categoria, o tempo vai depender de sua conexão',

      'categories.alert.form.title.savingNew': 'Registro salvo com sucesso',
      'categories.alert.form.message.savingNew': 'Categoria cadastrado com sucesso',

      'categories.alert.form.title.savingEdit': 'Alteração bem sucedida',
      'categories.alert.form.message.savingEdit': 'Categoria alterada com sucesso',

      'categories.alert.view.title': 'Erro ao carregar categoria',
      'categories.alert.error.title.savingNew': 'Erro ao salvar nova categoria',
      'categories.alert.error.title.savingEdit': 'Erro ao salvar alterações da categoria',

      'categories.btn.view.new':    'nova categoria',
      'categories.btn.view.edit':   'Editar',
      'categories.btn.view.list':   'voltar para listagem',
      'categories.btn.view.page':   'ver na loja',
      'categories.btn.view.cancel': 'cancelar',

      'categories.btn.form.save':  'Salvar',
      'categories.btn.form.cancel': 'Cancelar',

      'categories.list.title': 'Categorias',
      'categories.view.title': 'Categoria',
      'categories.form.title.new':  'Nova Categoria',
      'categories.form.title.edit': 'Alterar Categoria',

      'categories.view.id':        'Código',
      'categories.view.catname':   'Categoria',
      'categories.view.catparent': 'Categoria pai',
      'categories.view.catdesc':   'Descrição',
      'categories.view.catseo':    'SEO',

      'categories.form.label.id':        'Código',
      'categories.form.label.catname':   'Categoria',
      'categories.form.label.catparent': 'Categoria pai',
      'categories.form.label.catdesc':   'Descrição',
      'categories.form.label.catacervo': 'Imagens',

      'categories.form.place.id':        '######',
      'categories.form.place.catname':   'Digite aqui um nome para a categoria',
      'categories.form.place.catparent': 'Informe aqui a categoria pai',
      'categories.form.place.catdesc':   'Informe aqui uma descrição para categoria',
      'categories.form.place.catacervo': 'Arraste as imagens aqui ou click aqui para escolher as imagens',

      'categories.list.id':        'Código',
      'categories.list.catname':   'Categoria',
      'categories.list.catparent': 'Categoria pai',
      'categories.list.catdesc':   'Descrição',

      'categories.form.label.catlaunch':          'Data',
      'categories.form.label.catstatus':          'Publicação',
      'categories.form.label.catstatus.offline':  'Offline',
      'categories.form.label.catstatus.rascunho': 'Rascunho',
      'categories.form.label.catstatus.online':   'Online',

      'categories.view.catstatus':          'Publicação',
      'categories.view.catstatus.offline':  'Offline',
      'categories.view.catstatus.rascunho': 'Rascunho',
      'categories.view.catstatus.online':   'Online',

      /* -----------------------------------*/

      '/posts': '/posts',
      'posts.menu': 'Posts',
      'posts.btn.new': 'Novo Post',
      'posts.list.view': 'Visualizar post',
      'posts.list.edit': 'Alterar post',

      'posts.waiting.view.title': 'Aguarde!',
      'posts.waiting.view.message': 'Carregando post, o tempo vai depender de sua conexão',

      'posts.waiting.form.title.edit': 'Aguarde!',
      'posts.waiting.form.message.edit': 'Carregando post, o tempo vai depender de sua conexão',

      'posts.waiting.form.title.savingEdit': 'Aguarde!',
      'posts.waiting.form.message.savingEdit': 'Salvando alterações do Post, o tempo vai depender de sua conexão',

      'posts.waiting.form.title.savingNew': 'Aguarde!',
      'posts.waiting.form.message.savingNew': 'Salvando novo Post, o tempo vai depender de sua conexão',

      'posts.alert.form.title.savingNew': 'Registro salvo com sucesso',
      'posts.alert.form.message.savingNew': 'Post cadastrado com sucesso',

      'posts.alert.form.title.savingEdit': 'Alteração bem sucedida',
      'posts.alert.form.message.savingEdit': 'Post alterado com sucesso',

      'posts.alert.view.title': 'Erro ao carregar post',
      'posts.alert.error.title.savingNew': 'Erro ao salvar novo post',
      'posts.alert.error.title.savingEdit': 'Erro ao salvar alterações do post',

      'posts.btn.view.new':    'novo post',
      'posts.btn.view.edit':   'Editar',
      'posts.btn.view.list':   'voltar para listagem',
      'posts.btn.view.page':   'ver no blog',
      'posts.btn.view.cancel': 'cancelar',

      'posts.btn.form.save':  'Salvar',
      'posts.btn.form.cancel': 'Cancelar',

      'posts.list.title': 'Posts',
      'posts.view.title': 'Post',
      'posts.form.title': 'Post',

      'posts.form.title.new':  'Adicionar Post',
      'posts.form.title.edit': 'Alterar Post',

      'posts.view.id':         'Código',
      'posts.view.posttitle':  'Título',
      'posts.view.postresume': 'Resumo',
      'posts.view.postdesc':   'Descrição',
      'posts.view.postseo':    'SEO',

      'posts.form.label.id':         'Código',
      'posts.form.label.posttitle':  'Título',
      'posts.form.label.postresume': 'Resumo',
      'posts.form.label.postdesc':   'Descrição',
      'posts.form.label.postacervo': 'Imagens',
      'posts.form.label.posttags':   'Tags',

      'posts.form.place.id':         '######',
      'posts.form.place.posttitle':  'Digite aqui um título para o post',
      'posts.form.place.postresume': 'Informe o resumo do post',
      'posts.form.place.postdesc':   'Informe aqui uma descrição para o post',
      'posts.form.place.postacervo': 'Arraste as imagens aqui ou click aqui para escolher as imagens',
      'posts.form.place.posttags':   'Informe as tags desse post',

      'posts.form.label.postlaunch':          'Data',
      'posts.form.label.poststatus':          'Publicação',
      'posts.form.label.poststatus.offline':  'Offline',
      'posts.form.label.poststatus.rascunho': 'Rascunho',
      'posts.form.label.poststatus.online':   'Online',

      'posts.view.poststatus':          'Publicação',
      'posts.view.poststatus.offline':  'Offline',
      'posts.view.poststatus.rascunho': 'Rascunho',
      'posts.view.poststatus.online':   'Online',

      'posts.list.id':         'Código',
      'posts.list.posttitle':  'Título',
      'posts.list.postresume': 'Resumo',
      'posts.list.postdesc':   'Descrição',
      'posts.list.posttags':   'Tags',

      /* -----------------------------------*/

      '/page': '/paginas',
      'pages.menu': 'Páginas',
      'pages.btn.new': 'Nova página',
      'pages.list.view': 'Visualizar Página',
      'pages.list.edit': 'Alterar Página',

      'pages.waiting.view.title': 'Aguarde!',
      'pages.waiting.view.message': 'Carregando página, o tempo vai depender de sua conexão',

      'pages.waiting.form.title.edit': 'Aguarde!',
      'pages.waiting.form.message.edit': 'Carregando página, o tempo vai depender de sua conexão',

      'pages.waiting.form.title.savingEdit': 'Aguarde!',
      'pages.waiting.form.message.savingEdit': 'Salvando alterações da página, o tempo vai depender de sua conexão',

      'pages.waiting.form.title.savingNew': 'Aguarde!',
      'pages.waiting.form.message.savingNew': 'Salvando nova página, o tempo vai depender de sua conexão',

      'pages.alert.form.title.savingNew': 'Registro salvo com sucesso',
      'pages.alert.form.message.savingNew': 'Página cadastrada com sucesso',

      'pages.alert.form.title.savingEdit': 'Alteração bem sucedida',
      'pages.alert.form.message.savingEdit': 'Página alterada com sucesso',

      'pages.alert.view.title': 'Erro ao carregar Página',
      'pages.alert.error.title.savingNew': 'Erro ao salvar nova página',
      'pages.alert.error.title.savingEdit': 'Erro ao salvar alterações da página',

      'pages.btn.view.new':    'nova página',
      'pages.btn.view.edit':   'Editar',
      'pages.btn.view.list':   'voltar para listagem',
      'pages.btn.view.page':   'ver no site',
      'pages.btn.view.cancel': 'cancelar',

      'pages.btn.form.save':   'Salvar',
      'pages.btn.form.cancel': 'Cancelar',

      'pages.list.title': 'Páginas',
      'pages.view.title': 'Página',
      'pages.form.title': 'Página',

      'pages.form.title.new':  'Adicionar Página',
      'pages.form.title.edit': 'Alterar Página',

      'pages.view.id':        'Código',
      'pages.view.pagtitle':  'Página',
      'pages.view.pagresume': 'Resumo',
      'pages.view.pagdesc':   'Descrição',
      'pages.view.pagseo':    'SEO',

      'pages.form.label.id':          'Código',
      'pages.form.label.pagtitle':    'Página',
      'pages.form.label.pagresume':   'Resumo',
      'pages.form.label.pagdesc':     'Descrição',
      'pages.form.label.pagacervo':   'Imagens',
      'pages.form.label.pagsessions': 'Tags',

      'pages.form.place.id':          '######',
      'pages.form.place.pagtitle':    'Digite aqui um título para a página',
      'pages.form.place.pagresume':   'Informe o resumo da página',
      'pages.form.place.pagdesc':     'Informe aqui uma descrição para a página',
      'pages.form.place.pagacervo':   'Arraste as imagens aqui ou click aqui para escolher as imagens',
      'pages.form.place.pagsessions': 'Informe as tags desse post',

      'pages.form.label.paglaunch':          'Data',
      'pages.form.label.pagstatus':          'Publicação',
      'pages.form.label.pagstatus.offline':  'Offline',
      'pages.form.label.pagstatus.rascunho': 'Rascunho',
      'pages.form.label.pagstatus.online':   'Online',

      'pages.view.pagstatus':          'Publicação',
      'pages.view.pagstatus.offline':  'Offline',
      'pages.view.pagstatus.rascunho': 'Rascunho',
      'pages.view.pagstatus.online':   'Online',

      'pages.list.id':          'Código',
      'pages.list.pagtitle':    'Página',
      'pages.list.pagresume':   'Resumo',
      'pages.list.pagdesc':     'Descrição',
      'pages.list.pagsessions': 'Tags',

      /* -----------------------------------*/

      '/products':           '/produtos',
      'products.menu':       'Produtos',
      'products.btn.new':    'Novo Produto',
      'products.list.view':  'Visualizar produto',
      'products.list.edit':  'Alterar produto',
      'products.form.title': 'Produto',

      'products.waiting.view.title':   'Aguarde!',
      'products.waiting.view.message': 'Carregando produto, o tempo vai depender de sua conexão',

      'products.waiting.form.title.edit':   'Aguarde!',
      'products.waiting.form.message.edit': 'Carregando produto, o tempo vai depender de sua conexão',

      'products.waiting.form.title.savingEdit':   'Aguarde!',
      'products.waiting.form.message.savingEdit': 'Salvando alterações do Produto, o tempo vai depender de sua conexão',

      'products.waiting.form.title.savingNew':   'Aguarde!',
      'products.waiting.form.message.savingNew': 'Salvando novo Produto, o tempo vai depender de sua conexão',

      'products.alert.form.title.savingNew':   'Registro salvo com sucesso',
      'products.alert.form.message.savingNew': 'Produto cadastrado com sucesso',

      'products.alert.form.title.savingEdit':   'Alteração bem sucedida',
      'products.alert.form.message.savingEdit': 'Produto alterado com sucesso',

      'products.alert.view.title':             'Erro ao carregar produto',
      'products.alert.error.title.savingNew':  'Erro ao salvar novo produto',
      'products.alert.error.title.savingEdit': 'Erro ao salvar alterações do produto',

      'products.btn.view.new':    'novo produto',
      'products.btn.view.edit':   'Editar',
      'products.btn.view.list':   'voltar para listagem',
      'products.btn.view.page':   'ver na loja',
      'products.btn.view.cancel': 'cancelar',

      'products.btn.form.save':   'Salvar',
      'products.btn.form.cancel': 'Cancelar',

      'products.list.title':      'Listagem de Produtos',
      'products.view.title':      'Visualizar o Produto',
      'products.form.title.new':  'Adicionar Produto',
      'products.form.title.edit': 'Alterar Produto',

      'products.view.id':             'Código',
      'products.view.prodname':       'Produto',
      'products.view.prodcategories': 'Categorias',
      'products.view.proddesc':       'Descrição',
      'products.view.prodprice':      'Preço',
      'products.view.prodseo':        'SEO',

      'products.form.label.id':             'Código',
      'products.form.label.prodname':       'Produto',
      'products.form.label.prodcategories': 'Categorias',
      'products.form.label.proddesc':       'Descrição',
      'products.form.label.prodprice':      'Preço',
      'products.form.label.prodacervo':     'Imagens',
      'products.form.label.prodresume':     'Resumo',

      'products.form.place.id':             '######',
      'products.form.place.prodname':       'Digite aqui um nome para o produto',
      'products.form.place.prodcategories': 'Informe as categorias desse produto',
      'products.form.place.proddesc':       'Informe aqui uma descrição para o produto',
      'products.form.place.prodprice':      'R$ 0,00',
      'products.form.place.prodacervo':     'Arraste as imagens aqui ou click aqui para escolher as imagens',
      'products.form.place.prodresume':     'Informe um resumo para o produto aqui',

      'products.list.id':             'Código',
      'products.list.prodname':       'Produto',
      'products.list.prodcategories': 'Categorias',
      'products.list.proddesc':       'Descrição',
      'products.list.prodprice':      'Preço',

      'products.form.label.prodlaunch':          'Data',
      'products.form.label.prodstatus':          'Publicação',
      'products.form.label.prodstatus.offline':  'Offline',
      'products.form.label.prodstatus.rascunho': 'Rascunho',
      'products.form.label.prodstatus.online':   'Online',

      'products.view.prodstatus':          'Publicação',
      'products.view.prodstatus.offline':  'Offline',
      'products.view.prodstatus.rascunho': 'Rascunho',
      'products.view.prodstatus.online':   'Online'
    }
  };

  var keyKnown = Boolean(i18n[degaultLang].hasOwnProperty(key) && i18n[degaultLang][key]);
  if (!keyKnown) return key;

  var translate = i18n[degaultLang][key];

  if (arguments) {
    for(var arg in arguments) {
      var val = arguments[arg];
      var argReplace = new RegExp('\\{' + String(arg) + '\\}', 'g');
      translate = String(translate).replace(argReplace, val);
    }
  }

  return translate;
};
