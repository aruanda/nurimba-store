/*globals $: false*/
'use strict';

var FieldMixinText = {
  prepareTextEditor: function() {
    var fieldTxt = this;
    fieldTxt.refs.textEditor;
    fieldTxt.textEditor = $(fieldTxt.refs.textEditor);
    fieldTxt.textEditor.wysihtml5({
      locale: 'pt-BR',
      toolbar: { fa: true },
      events: {
        blur: function() {
          var val = String(fieldTxt.textEditor.html() || '').trim();
          fieldTxt.handleChangeEditor(val);
        }
      }
    });
  },

  changeTextEditor: function() {
    var fieldTxt = this;
    var valProps  = fieldTxt.getFieldProperties().value;
    var valEditor = fieldTxt.textEditor.html();
    if (valProps === valEditor) return;
    fieldTxt.textEditor.html(valProps);
  },

  destroyTextEditor: function() {
    var fieldTxt = this;
    fieldTxt.textEditor.remove();
    fieldTxt.textEditor.undefined;
  },

  handleChangeEditor: function(val) {
    var fieldTxt = this;
    var props = fieldTxt.getFieldProperties();
    if (val === props.value) return;
    fieldTxt.registerValue(val || '');
  }
};

module.exports = FieldMixinText;
