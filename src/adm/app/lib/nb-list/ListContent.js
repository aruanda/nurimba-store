'use strict';

var React = require('react');
var Loading = require('./Loading');

var colTypes = {
  seo: require('./cols/ColSEO'),
  text: require('./cols/ColText'),
  email: require('./cols/ColEmail'),
  group: require('./cols/ColGroup'),
  title: require('./cols/ColString'),
  string: require('./cols/ColString'),
  images: require('./cols/ColImages'),
  integer: require('./cols/ColInteger'),
  primary: require('./cols/ColPrimary'),
  textarea: require('./cols/ColText'),
  customer: require('./cols/ColCustomer'),
  currency: require('./cols/ColCurrency'),
  suggestSingle: require('./cols/ColSuggestSingle'),
  suggestMultiple: require('./cols/ColSuggestMultiple')
};

var ListContent = React.createClass({
  render: function () {
    var trRows;
    var trLoading;

    var props      = this.props;
    var rows       = props.data;
    var cols       = props.cols;
    var loaded     = Boolean(props.loaded);
    var othersCols = 1;
    var totalCols  = cols.length + othersCols;

    var tableCols = [];

    cols.forEach(function(col) {
      if (!colTypes.hasOwnProperty(col.kind)) return;

      col.label = col.domain.concat('.list.', col.attr).translate();

      var config = { textAlign: 'left' };

      if (col.kind === 'primary') {
        config.textAlign = 'center';
      }

      if (col.kind === 'currency') {
        config.textAlign = 'right';
      }

      tableCols.push(
        <th key={tableCols.length} style={config}>
          { col.domain.concat('.list.', col.attr).translate() }
        </th>
      );
    });

    tableCols.push(<th key={tableCols.length}></th>);

    if (!loaded) {
      trLoading =
        <tr>
          <td colSpan={totalCols} style={{ textAlign: 'center', backgroundColor: '#fff' }}>
            <Loading />
          </td>
        </tr>
      ;
    }

    if (loaded) {
      var withData    = rows && rows.length;
      var withoutData = !withData;

      if (withoutData) {
        trRows = (
          <tr>
            <td colSpan={totalCols} style={{ textAlign: 'center', backgroundColor: '#fff' }}>
              <section>
                <h4>{ 'table.notFound'.translate() }</h4>
              </section>
            </td>
          </tr>
        );
      }

      if (withData) {
        trRows = [];
        rows.forEach(function(row) {
          var tds = [];
          cols.forEach(function(col) {
            if (!colTypes.hasOwnProperty(col.kind)) return;
            var ColType = colTypes[col.kind] || colTypes.string;
            var state = { value: row && row.hasOwnProperty(col.attr) ? row[col.attr] : undefined };

            tds.push(
              <ColType
                key={tds.length}
                col={col}
                row={row}
                data={state}
              />
            );
          });

          tds.push(
            <td key={tds.length} style={{ textAlign: 'right', maxWidth: '135px !important' }}>
              <ul className="list-unstyled list-buttons">
                {(props.actions || []).btns.map(function(btn, index) {
                  var callActionBtn = function() {
                    if (btn.onClick) btn.onClick(row);
                  };

                  return (
                    <li key={index} style={{ margin: '0 5px' }}>
                      <button key={index} onClick={ callActionBtn } type={ btn.type } title={ btn.text.translate() } className={ btn.className } >
                        <span className={ btn.icon }></span>
                      </button>
                    </li>
                  );
                })}
              </ul>
            </td>
          );

          trRows.push(<tr key={trRows.length}>{tds}</tr>);
        });
      }
    }

    return (
      <table className="table table-hover">
        <thead>
          <tr>{tableCols}</tr>
        </thead>

        <tbody>
          {trLoading}
          {trRows}
        </tbody>
      </table>
    );
  }
});

module.exports = ListContent;
