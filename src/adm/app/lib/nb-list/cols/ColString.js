'use strict';

var React = require('react');

var ColString = React.createClass({
  render: function() {
    var val = this.props.data.value;
    if (!val) return <td style={{ color: '#8A8A8A', textAlign: 'left' }}>vazio</td>;
    return <td>{val}</td>;
  }
});

module.exports = ColString;
