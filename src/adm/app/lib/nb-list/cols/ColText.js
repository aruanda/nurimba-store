'use strict';

var React = require('react');

var ColText = React.createClass({
  render: function() {
    var val = this.props.data.value;
    if (!val) return <td style={{ color: '#8A8A8A', textAlign: 'center' }}>vazio</td>;
    return <td dangerouslySetInnerHTML={{__html: val}}></td>;
  }
});

module.exports = ColText;
