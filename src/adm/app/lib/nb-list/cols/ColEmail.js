'use strict';

var React = require('react');

var ColEmail = React.createClass({
  render: function() {
    var val = this.props.data.value;
    if (!val) return <td style={{ color: '#8A8A8A', textAlign: 'center' }}>vazio</td>;
    return <td style={{ textAlign: 'center' }}>{val}</td>;
  }
});

module.exports = ColEmail;
