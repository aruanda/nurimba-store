'use strict';

var React = require('react');

var ColSuggestSingle = React.createClass({
  render: function() {
    var props = this.props;
    var col = props.col;
    var val = props.row[col.attr];

    if (!val) return <td style={{ color: '#8A8A8A', textAlign: 'left' }}>vazio</td>;

    return (
      <td>
        <span className={'label label-primary'}>
          {val.label}
        </span>
      </td>
    );
  }
});

module.exports = ColSuggestSingle;
