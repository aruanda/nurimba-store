'use strict';

var nbNumeral = require('numeral');
var ptBr = require('numeral/i18n/pt-br');
nbNumeral.language('pt-br', ptBr);
nbNumeral.language('pt-br');

module.exports = nbNumeral;
