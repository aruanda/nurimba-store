#!/bin/bash

while ! nc -q 1 pw-db 5432 </dev/null; do sleep 3; done

echo ""
echo "-----------------------------"
echo "Máquina pronta para Trabalhar"
echo "-----------------------------"
echo ""

exec "$@"
