# Pedal Works
Sample Store of http://nurimba.com.br

[DEMO](http://pedalworks.nurimba.com.br)

Environment with:

* [Docker](https://docs.docker.com/)
* [Debian](https://www.debian.org/releases/stable/)
* [Make](http://www.gnu.org/software/make/manual/make.html#Running)
* [NodeJS](https://nodejs.org/dist/latest-v4.x/docs/api/)
* [Bower](http://bower.io/)
* [Gulp](http://gulpjs.com/)


Docker install the linux environment:

```sh
$ curl -sSL https://get.docker.com/ | sh
```


Make tasks of environment

* Build docker image - ```$ make build-images```
* Create docker container - ```$ make build-development```
* Run docker container (only container already created) - ```$ make attach-container```


First steps after environment builded (inside the container)

```sh
pw@dev$ make install-enviroment
pw@dev$ gulp serve
```
