#!/bin/bash

sudo chown -R $USER:$USER ./

if [ ! -f server.json ]
then
  cp ./server.json.sample server.json
fi

npm install && \
bower install --allow-root --config.interactive=false && \
make db-migrate && \
gulp build:prod
