
run:
	docker-compose run -p 8080:8080 -p 3000:3000 --rm dev

attach-container-database:
	docker exec -it pw-db psql -h localhost -U nurimba

install:
	docker-compose run --rm dev sh install.sh

#node Migration.js

deploy: install
	docker-compose run --rm dev sh deploy.sh

db-migrate:
	 cd migrations/ && node Migration.js && cd ../

db-rollback:
	 cd migrations/ && node Migration.js --rollback && cd ../

prod-migrate:
	docker-compose run --rm prod make db-migrate

down:
	docker-compose down

up: prod-migrate down
	docker-compose up -d prod
