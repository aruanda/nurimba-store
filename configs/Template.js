'use strict';

var path    = require('path');
var HTMLing = require('htmling');
var moment  = require('moment');
require('../node_modules/moment/locale/pt-br');
moment.locale('pt-br');

function EngineConfig(app) {
  var engineName = 'html';
  var pathViews  = path.dirname(__dirname).concat('/src/web/views');

  app.set('views', pathViews);

  app.engine(engineName, HTMLing.express(pathViews + '/', {
    watch: true, //app.get('isDevelopment'),
    minify: false, //app.get('isProduction'),

    elements: {
      'txt-html': function (params) {
        return params.content;
      },

      'txt-date': function (params) {
        return moment(params.date).format(params.format);
      }
    }
  }));

  app.set('view engine', engineName);

  return app;
}

module.exports = EngineConfig;
