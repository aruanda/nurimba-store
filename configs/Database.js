'use strict';

var qPostgres = require('q-postgres');

function DatabaseConfig(app) {
  var db = app.get('props').db;
  app.set('database', new qPostgres(db.user, db.pass, db.host, db.base));
  return app;
}

module.exports = DatabaseConfig;
