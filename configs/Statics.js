'use strict';

var path    = require('path');
var express = require('express');

function StaticsConfig(app) {
  var basePath = path.resolve(path.dirname(__dirname).concat('/static'));

  var pathJS    = basePath.concat('/js');
  var pathImg   = basePath.concat('/img');
  var pathCSS   = basePath.concat('/css');
  var pathFonts = basePath.concat('/fonts');
  var pathAdmin = basePath.concat('/admin.html');

  app.use('/assets/js',    express.static(pathJS));
  app.use('/assets/css',   express.static(pathCSS));
  app.use('/assets/fonts', express.static(pathFonts));
  app.use('/assets/img',   express.static(pathImg));
  app.use('/files',        express.static('/files'));

  var adminStatic = express.static(pathAdmin);
  app.use('/admin',   adminStatic);
  app.use('/admin/*', adminStatic);

  return app;
}

module.exports = StaticsConfig;
