'use strict';

var fs     = require('fs');
var glob   = require('glob');
var colog  = require('colog');
var path   = require('path');
var multer = require('multer');

var getUploadLocalStorage = function(app) {
  var pathStorage = app.get('props').storage + '/';

  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      var fieldTable = '';
      var fieldname  = file.fieldname;
      var partsName  = fieldname.split('/');
      var isMultPart = Boolean(partsName.length > 1);
      if (isMultPart) fieldTable = partsName.splice(0, 1).pop();
      fieldname = partsName.join('/');

      file.fieldname  = fieldname;
      file.fieldTable = fieldTable;

      var fullPathTmp = pathStorage.concat(fieldTable ? fieldTable + '/' : '');

      var fullPath = '/';
      fullPathTmp.split('/').forEach(function(part) {
        if (part === '/' || !part) return;
        fullPath += part + '/';
        try {
          fs.mkdirSync(fullPath);
        } catch (e) {
          e;
        }
      });

      cb(null, fullPathTmp);
    },

    filename: function (req, file, cb) {
      var fileExt = '';

      var partsExt = file.fieldname.split('.');
      var withExt = Boolean(partsExt.length > 1);
      if (withExt) fileExt = '.' + partsExt.pop();
      var fieldTable = file.fieldTable;
      var fullPathTmp = pathStorage.concat(fieldTable ? fieldTable + '/' : '');

      var fileName = partsExt
        .join('.')
        .replace(/^\s\s*/, '') // Trim start
        .replace(/\s\s*$/, '') // Trim end
        .toLowerCase() // Camel case is bad
        .replace(/[^a-z0-9_\-~!\+\s]+/g, '') // Exchange invalid chars
        .replace(/[\s]+/g, '-'); // Swap whitespace for single hyphen

      var verifyFileName = function(name, extension, count) {
        if (!count) count = 0;
        var preName = (name + String(count ? '-'.concat(count) : '')).replace(/--/, ''); // Trim end
        var otherName = preName + extension;

        var pathFile = fullPathTmp.concat(otherName);
        fs.exists(pathFile, function(exists) {
          if (exists) return verifyFileName(name, extension, ++count);
          cb(null, otherName);
        });
      };

      verifyFileName(fileName, fileExt);
    }
  });

  return (multer({ storage: storage })).any();
};

var catchError = function(err) {
  colog.error(err);
  process.exit();
};

function RoutesConfig(app) {
  var admRoutes = path.resolve(__dirname + '/../src/adm/api/**/*Route.js');
  var webRoutes = path.resolve(__dirname + '/../src/web/routes/**/*Route.js');
  var paths     = '{' + admRoutes + ',' + webRoutes + '}';

  glob.sync(paths, {}).map(function(routeFile) {
    var RouteClass = require(routeFile);
    var routeInstance = new RouteClass();
    routeInstance.file = routeFile;
    return routeInstance;
  }).sort(function(a, b) {
    if (a.url < b.url) return -1;
    if (a.url > b.url) return  1;
    return 0;
  }).forEach(function(route) {
    var routeVrb = route.verb.toLowerCase();
    var routeUrl = route.url;

    var routeAction = function(req, res, next) {
      var database = app.get('database');

      database.connect().then(function(connection) {
        return connection.openTransaction().then(function(transaction) {
          req.database = transaction;

          //setTimeout(function() {
          route.action(req, res, function() {
            var isError = res.statusCode >= 400;
            var actionDatabase = isError ? 'rollback' : 'commit';

            req.database[actionDatabase]().then(function() {
              req.database.end();
            }).catch(function(err) {
              colog.error(err);
              res.status(500).send(err.message || 'Internal Error');
            }).then(next);
          });
          //}, 200000);
        });
      }).catch(catchError);
    };

    if (route.upload) {
      var uploadLocalStorage = getUploadLocalStorage(app);
      app[routeVrb](routeUrl, uploadLocalStorage, routeAction);
    } else {
      app[routeVrb](routeUrl, routeAction);
    }
  });

  return app;
}

module.exports = RoutesConfig;
