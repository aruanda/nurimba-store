'use strict';

var routes   = require('./Routes');
var statics  = require('./Statics');
var database = require('./Database');
var template = require('./Template');

function AllConfigs(app) {
  return statics(routes(template(database(app))));
}

module.exports = AllConfigs;
