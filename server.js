'use strict';

var http        = require('http');
var helmet      = require('helmet');
var express     = require('express')();
var bodyParser  = require('body-parser');
var compression = require('compression');
var allConfigs  = require('./configs/AllConfigs');

var env = process.env.NODE_ENV || 'development';
var props = require('./server.json')[env];

express.set('props', props);
express.set('port', props.port);
express.set('isProduction',  Boolean(env === 'production'));
express.set('isDevelopment', Boolean(env === 'development'));

express.use(helmet());
express.use(compression());
express.use(bodyParser.json({ limit: '2mb' }));
express.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, DELETE, GET, POST');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, X-Custom-Header');
  next();
});

var server = http.createServer(allConfigs(express));

server.listen(props.port);

server.on('error', function(err) {
  console.log('error:' + err);
});

server.on('listening', function(){
  console.log('server is up, all is well');
});
