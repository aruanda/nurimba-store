#!/bin/bash

sudo chown -R $USER:$USER ./ && \
rm -Rf ./deploy/ && \
mkdir -p ./deploy && \
rsync -avzh --exclude-from "${PWD}/deployExclude.txt" --delete ./ ./deploy && \
cd ./deploy && \
npm install --production --ignore-scripts && \
cp ../server.json.sample server.json && \
cd ../ && \
rsync -avzhe "ssh -o StrictHostKeyChecking=no" ./deploy/ deploy@nurimba.com.br:/deploy/projects/pedalworks && \
ssh -o StrictHostKeyChecking=no deploy@nurimba.com.br 'cd /deploy/projects/pedalworks && make up'
