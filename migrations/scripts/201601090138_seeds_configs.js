'use strict';

var moment = require('moment');
var builder = require('../../src/adm/lib/QueryBuilder');
var configs = 'configs';
var faker = require('faker');
faker.locale = 'pt_BR';

module.exports = {
  up: function() {
    var blogArgs = {
      id: 1,
      confname: 'BLOG',
      confstatus: 'online',
      conflaunch: moment().format('YYYY-MM-DD').concat(' 00:00:00.000'),
      conffacebook: 'https://www.facebook.com/Nurimba-380400322071156',
      conftwitter: 'https://twitter.com/nurimbeiros',
      confgoogle: '',
      confinstagram: '',
      confwhatsapp: '(48) 8801-2021',
      confemail: 'contato@nurimba.com',
      confcep: '00000-000',
      confstatecity: faker.address.stateAbbr() + ' / ' + faker.address.city(),
      confaddress: faker.address.streetName() + ', 7547'
    };

    var lojaArgs = {
      id: 2,
      confname: 'LOJA',
      confstatus: 'online',
      conflaunch: moment().format('YYYY-MM-DD').concat(' 00:00:00.000'),
      conffacebook: 'https://www.facebook.com/Nurimba-380400322071156',
      conftwitter: 'https://twitter.com/nurimbeiros',
      confgoogle: '',
      confinstagram: '',
      confwhatsapp: '(48) 8801-2021',
      confemail: 'contato@nurimba.com',
      confcep: '00000-000',
      confstatecity: faker.address.stateAbbr() + ' / ' + faker.address.city(),
      confaddress: faker.address.streetName() + ', 7547'
    };

    var configBlog = builder
      .insert(blogArgs)
      .into(configs)
      .build();

    var configSite = builder
      .insert(lojaArgs)
      .into(configs)
      .build();

    return configBlog + '; ' + configSite + ';';
  },

  down: function() {
    return 'DELETE FROM configs where id in (1, 2)';
  }
};
