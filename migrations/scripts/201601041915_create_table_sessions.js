'use strict';

var builder = require('../../src/adm/lib/QueryBuilder');
var sessions = 'sessions';

module.exports = {
  up: function() {
    var required = true;
    var nullable = false;

    return builder
      .createTable(sessions)
      .field('id',            'id',        required)
      .field('sessname',      'string',    required)
      .field('sessresume',    'text',      nullable)
      .field('sessdesc',      'text',      nullable)
      .field('sessseo',       'json',      nullable)
      .field('sessimagemain', 'json',      nullable)
      .field('sessstatus',    'string',    required)
      .field('sesslaunch',    'timestamp', nullable)
      .field('sessparent',    'int',       nullable)
      .build();
  },

  down: function() {
    return builder.dropTable(sessions).build();
  }
};
