'use strict';

var builder = require('../../src/adm/lib/QueryBuilder');
var categories = 'categories';

module.exports = {
  up: function() {
    var required = true;
    var nullable = false;

    return builder
      .createTable(categories)
      .field('id',           'id',        required)
      .field('catname',      'string',    required)
      .field('catdesc',      'text',      nullable)
      .field('catseo',       'json',      nullable)
      .field('catimagemain', 'json',      nullable)
      .field('catstatus',    'string',    required)
      .field('catlaunch',    'timestamp', nullable)
      .field('catparent',    'int',       nullable)
      .build();
  },

  down: function() {
    return builder.dropTable(categories).build();
  }
};
