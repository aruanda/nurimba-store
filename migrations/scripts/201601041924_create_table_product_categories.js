'use strict';

var builder = require('../../src/adm/lib/QueryBuilder');
var productCategories = 'productcategories';

module.exports = {
  up: function() {
    var required = true;

    var sql = builder
      .createTable(productCategories)
      .field('id',         'id',  required)
      .field('productid',  'int', required)
      .field('categoryid', 'int', required)
      .build();

    return sql;
  },

  down: function() {
    return builder.dropTable(productCategories).build();
  }
};
