'use strict';

var builder = require('../../src/adm/lib/QueryBuilder');
var pageSessions = 'pagesessions';

module.exports = {
  up: function() {
    var required = true;

    var sql = builder
      .createTable(pageSessions)
      .field('id',        'id',  required)
      .field('pagetid',   'int', required)
      .field('sessionid', 'int', required)
      .build();

    return sql;
  },

  down: function() {
    return builder.dropTable(pageSessions).build();
  }
};
