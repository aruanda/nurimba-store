'use strict';

var builder = require('../../src/adm/lib/QueryBuilder');
var tags = 'tags';

module.exports = {
  up: function() {
    var required = true;
    var nullable = false;

    return builder
      .createTable(tags)
      .field('id',           'id',        required)
      .field('tagname',      'string',    required)
      .field('tagdesc',      'text',      nullable)
      .field('tagseo',       'json',      nullable)
      .field('tagimagemain', 'json',      nullable)
      .field('tagstatus',    'string',    required)
      .field('taglaunch',    'timestamp', nullable)
      .field('tagparent',    'int',       nullable)
      .build();
  },

  down: function() {
    return builder.dropTable(tags).build();
  }
};
