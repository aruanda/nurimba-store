'use strict';

var builder = require('../../src/adm/lib/QueryBuilder');
var posttags = 'posttags';

module.exports = {
  up: function() {
    var required = true;

    var sql = builder
      .createTable(posttags)
      .field('id',     'id',  required)
      .field('postid', 'int', required)
      .field('tagid',  'int', required)
      .build();

    return sql;
  },

  down: function() {
    return builder.dropTable(posttags).build();
  }
};
