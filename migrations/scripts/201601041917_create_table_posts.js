'use strict';

var builder = require('../../src/adm/lib/QueryBuilder');
var posts = 'posts';

module.exports = {
  up: function() {
    var required = true;
    var nullable = false;

    return builder
      .createTable(posts)
      .field('id',            'id',        required)
      .field('posttitle',     'string',    required)
      .field('postresume',    'text',      nullable)
      .field('postdesc',      'text',      nullable)
      .field('postseo',       'json',      nullable)
      .field('postimagemain', 'json',      nullable)
      .field('poststatus',    'string',    required)
      .field('postlaunch',    'timestamp', nullable)
      .build();
  },

  down: function() {
    return builder.dropTable(posts).build();
  }
};
