'use strict';

module.exports = {
  up: function() {
    return 'DELETE FROM categories; DELETE FROM products; DELETE FROM tags; DELETE FROM posts;';
  },

  down: function() {
    return false;
  }
};
