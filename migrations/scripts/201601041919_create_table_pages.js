'use strict';

var builder = require('../../src/adm/lib/QueryBuilder');
var pages = 'pages';

module.exports = {
  up: function() {
    var required = true;
    var nullable = false;

    return builder
      .createTable(pages)
      .field('id',           'id',        required)
      .field('pagtitle',     'string',    required)
      .field('pagresume',    'text',      nullable)
      .field('pagdesc',      'text',      nullable)
      .field('pagseo',       'json',      nullable)
      .field('pagimagemain', 'json',      nullable)
      .field('pagstatus',    'string',    required)
      .field('paglaunch',    'timestamp', nullable)
      .build();
  },

  down: function() {
    return builder.dropTable(pages).build();
  }
};
