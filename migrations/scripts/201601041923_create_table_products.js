'use strict';

var builder = require('../../src/adm/lib/QueryBuilder');
var products = 'products';

module.exports = {
  up: function() {
    var required = true;
    var nullable = false;

    return builder
      .createTable(products)
      .field('id',            'id',        required)
      .field('prodname',      'string',    required)
      .field('prodresume',    'text',      nullable)
      .field('proddesc',      'text',      nullable)
      .field('prodseo',       'json',      nullable)
      .field('prodimagemain', 'json',      nullable)
      .field('prodstatus',    'string',    required)
      .field('prodlaunch',    'timestamp', nullable)
      .field('prodoldprice',  'money',     nullable)
      .field('prodprice',     'money',     required)

      .build();
  },

  down: function() {
    return builder.dropTable(products).build();
  }
};
