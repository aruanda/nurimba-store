'use strict';

var builder = require('../../src/adm/lib/QueryBuilder');
var images = 'images';

module.exports = {
  up: function() {
    var required = true;
    var nullable = false;

    return builder
      .createTable(images)
      .field('id',          'id',        required)
      .field('filecreated', 'timestamp', required)
      .field('fileupdated', 'timestamp', nullable)
      .field('filetag',     'string',    required)
      .field('filesrc',     'string',    required)
      .field('filename',    'string',    required)
      .field('filetype',    'string',    required)
      .field('filesize',    'int',       required)
      .field('filetitle',   'string',    nullable)
      .field('filetagid',   'int',       nullable)
      .build();
  },

  down: function() {
    return builder.dropTable(images).build();
  }
};
