'use strict';

var q = require('q');
var moment = require('moment');
var builder = require('../../src/adm/lib/QueryBuilder');

var faker = require('faker');
faker.locale = 'pt_BR';

var images = [
  'http://wallpapercave.com/wp/S3xqF25.jpg',
  'http://north.fuelthemes.net/wp-content/uploads/2014/10/44.jpg'
];

var createTag = function(transaction) {
  var row = {
    tagname: faker.name.jobArea(),
    tagdesc: faker.lorem.paragraphs(),
    tagstatus: 'online',
    taglaunch: moment().format('YYYY-MM-DD').concat(' 00:00:00.000')
  };

  var sqlTag = builder
    .insert(row)
    .into('tags')
    .returning(['id'])
    .build();

  return transaction.runScript(sqlTag).then(function(resultset) {
    return resultset.rows.pop();
  });
};

var img = 0;
var createPost = function(transaction) {
  var row = {
    posttitle: faker.name.jobTitle(),
    postresume: faker.lorem.sentences(),
    postdesc: faker.lorem.paragraphs(15),
    poststatus: 'online',
    postlaunch: moment().format('YYYY-MM-DD').concat(' 00:00:00.000'),
    postimagemain: JSON.stringify({
      src: images[img],
      alt: faker.name.jobType()
    })
  };

  ++img;
  if (img >= images.length) img = 0;

  var sqlPosts = builder
    .insert(row)
    .into('posts')
    .returning(['id'])
    .build();

  return transaction.runScript(sqlPosts).then(function(resultset) {
    return resultset.rows.pop();
  });
};

var createPostTag = function(transaction, tag, post) {
  var row = {
    tagid: tag,
    postid: post
  };

  var sqlPostsTags = builder
    .insert(row)
    .into('posttags')
    .build();

  return transaction.runScript(sqlPostsTags);
};

module.exports = {
  up: function(transaction) {
    var count = 15;
    var insertTags = [];

    for (var c = 0; c < count; c++) insertTags.push(createTag(transaction));

    return q.all(insertTags).then(function(tags) {
      var createPosts = tags.map(function(tag, index) {
        var insertPosts = [];

        for (var c = 0; c < count; c++) insertPosts.push(createPost(transaction));

        return q.all(insertPosts).then(function(posts) {
          var postTags = [];

          posts.forEach(function(post) {
            postTags.push(createPostTag(transaction, tag.id, post.id));

            var idx = post.id - (Math.floor(post.id / tags.length) * tags.length);

            var otherTag1 = index + 3 + idx;
            if (otherTag1 >= tags.length) otherTag1 -= tags.length;
            if (otherTag1 >= tags.length) otherTag1 -= tags.length;

            var otherTag2 = index + 5 + idx;
            if (otherTag2 >= tags.length) otherTag2 -= tags.length;
            if (otherTag2 >= tags.length) otherTag2 -= tags.length;

            var otherTag3 = index + 7 + idx;
            if (otherTag3 >= tags.length) otherTag3 -= tags.length;
            if (otherTag3 >= tags.length) otherTag3 -= tags.length;

            postTags.push(createPostTag(transaction, tags[otherTag1].id, post.id));
            postTags.push(createPostTag(transaction, tags[otherTag2].id, post.id));
            postTags.push(createPostTag(transaction, tags[otherTag3].id, post.id));
          });

          return q.all(postTags);
        });
      });

      return q.all(createPosts).then(function() {
        return false;
      });
    });
  },

  down: function() {
    return 'DELETE FROM tags; DELETE FROM posts;';
  }
};
