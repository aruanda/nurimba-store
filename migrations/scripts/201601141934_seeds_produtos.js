'use strict';

var q = require('q');
var moment = require('moment');
var builder = require('../../src/adm/lib/QueryBuilder');

var faker = require('faker');
faker.locale = 'pt_BR';

var images = [
  'http://wallpapercave.com/wp/S3xqF25.jpg',
  'http://north.fuelthemes.net/wp-content/uploads/2014/10/44.jpg'
];

var createCategory = function(transaction) {
  var row = {
    catname: faker.name.jobArea(),
    catdesc: faker.lorem.paragraphs(),
    catstatus: 'online',
    catlaunch: moment().format('YYYY-MM-DD').concat(' 00:00:00.000')
  };

  var sqlCategory = builder
    .insert(row)
    .into('categories')
    .returning(['id'])
    .build();

  return transaction.runScript(sqlCategory).then(function(resultset) {
    return resultset.rows.pop();
  });
};

var img = 0;
var createProduct = function(transaction) {
  var row = {
    prodname: faker.name.jobTitle(),
    prodresume: faker.lorem.sentences(),
    proddesc: faker.lorem.paragraphs(15),
    prodstatus: 'online',
    prodprice: 1250.49,
    prodlaunch: moment().format('YYYY-MM-DD').concat(' 00:00:00.000'),
    prodimagemain: JSON.stringify({
      src: images[img],
      alt: faker.name.jobType()
    })
  };

  ++img;
  if (img >= images.length) img = 0;

  var sqlProducts = builder
    .insert(row)
    .into('products')
    .returning(['id'])
    .build();

  return transaction.runScript(sqlProducts).then(function(resultset) {
    return resultset.rows.pop();
  });
};

var createProductCategory = function(transaction, tag, prod) {
  var row = {
    categoryid: tag,
    productid: prod
  };

  var sqlProductsCategorys = builder
    .insert(row)
    .into('productcategories')
    .build();

  return transaction.runScript(sqlProductsCategorys);
};

module.exports = {
  up: function(transaction) {
    var count = 15;
    var insertCategorys = [];

    for (var c = 0; c < count; c++) insertCategorys.push(createCategory(transaction));

    return q.all(insertCategorys).then(function(categories) {
      var createProducts = categories.map(function(cat, index) {
        var insertProducts = [];

        for (var c = 0; c < count; c++) insertProducts.push(createProduct(transaction));

        return q.all(insertProducts).then(function(products) {
          var productCategories = [];

          products.forEach(function(prod) {
            productCategories.push(createProductCategory(transaction, cat.id, prod.id));

            var idx = prod.id - (Math.floor(prod.id / categories.length) * categories.length);

            var otherCategory1 = index + 3 + idx;
            if (otherCategory1 >= categories.length) otherCategory1 -= categories.length;
            if (otherCategory1 >= categories.length) otherCategory1 -= categories.length;

            var otherCategory2 = index + 5 + idx;
            if (otherCategory2 >= categories.length) otherCategory2 -= categories.length;
            if (otherCategory2 >= categories.length) otherCategory2 -= categories.length;

            var otherCategory3 = index + 7 + idx;
            if (otherCategory3 >= categories.length) otherCategory3 -= categories.length;
            if (otherCategory3 >= categories.length) otherCategory3 -= categories.length;

            productCategories.push(createProductCategory(transaction, categories[otherCategory1].id, prod.id));
            productCategories.push(createProductCategory(transaction, categories[otherCategory2].id, prod.id));
            productCategories.push(createProductCategory(transaction, categories[otherCategory3].id, prod.id));
          });

          return q.all(productCategories);
        });
      });

      return q.all(createProducts).then(function() {
        return false;
      });
    });
  },

  down: function() {
    return 'DELETE FROM categories; DELETE FROM products;';
  }
};
