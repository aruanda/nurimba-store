'use strict';

var builder = require('../../src/adm/lib/QueryBuilder');
var configs = 'configs';

module.exports = {
  up: function() {
    var required = true;
    var nullable = false;

    return builder
      .createTable(configs)
      .field('id',            'id',        required)
      .field('confseo',       'json',      nullable)
      .field('confstatus',    'string',    required)
      .field('conflaunch',    'timestamp', nullable)
      .field('confname',      'string',    nullable)
      .field('conffacebook',  'string',    nullable)
      .field('conftwitter',   'string',    nullable)
      .field('confgoogle',    'string',    nullable)
      .field('confinstagram', 'string',    nullable)
      .field('confwhatsapp',  'string',    nullable)
      .field('confemail',     'string',    nullable)
      .field('confaddress',   'string',    nullable)
      .field('confcep',       'string',    nullable)
      .field('confstatecity', 'string',    nullable)
      .build();
  },

  down: function() {
    return builder.dropTable(configs).build();
  }
};
