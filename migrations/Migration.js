'use strict';

var env = process.env.NODE_ENV || 'development';
var props = require('../server.json')[env];
var db = props.db;

var q         = require('q');
var colog     = require('colog');
var moment    = require('moment');
var findFiles = require('glob');
var qPostgres = require('q-postgres');
var shellArgs = require('shell-arguments');

var migrationsPath = __dirname.concat('/scripts/**/*.js');
var isUpMigration  = (shellArgs.hasOwnProperty('rollback') && shellArgs.rollback ? 'down' : 'up') === 'up';
var poolPostgres   = new qPostgres(db.user, db.pass, db.host, db.base);

var mgtId       = 'mgtid';
var mgtFile     = 'mgtfile';
var mgtBatch    = 'mgtbatch';
var mgtDtHr     = 'mgtdthr';
var migrations  = 'migrations';
var builder     = require('../src/adm/lib/QueryBuilder');
var batchNumber = 0;
var getFileObject;

var searchMigrations = function(lastFile) {
  if (!lastFile) lastFile = {};
  if (!lastFile.hasOwnProperty(mgtBatch)) lastFile[mgtBatch] = 0;
  if (!lastFile.hasOwnProperty(mgtFile))  lastFile[mgtFile] = '';

  var filterMigrations = function(path) {
    var parts = path.split('/');
    var file  = parts[parts.length - 1];

    var isDowngrade = !isUpMigration;
    if (isDowngrade) {
      var fileObject = getFileObject(file);
      if (!fileObject) return false;
      if (fileObject[mgtBatch] < batchNumber) return false;
    }

    return isUpMigration ?
      file > lastFile[mgtFile] :
      file <= lastFile[mgtFile];
  };

  var sortMigrations = function(a, b) {
    if (a < b) return isUpMigration ? -1 :  1;
    if (a > b) return isUpMigration ?  1 : -1;
    return 0;
  };

  var requireMigration = function(path) {
    var parts = path.split('/');
    var migration = require(path);
    migration.file = parts[parts.length - 1];
    return migration;
  };

  return q.Promise(function(resolve, reject) {
    findFiles(migrationsPath, {}, function(err, files) {
      if (err) return reject(err);

      resolve(files
        .filter(function(file) {
          return filterMigrations(file);
        })
        .sort(sortMigrations)
        .map(requireMigration));
    });
  });
};

var createTableMigrations = function(transaction) {
  var required = true;
  var ifTableNotExist = true;

  var sqlTableMigrations = builder
    .createTable(migrations, ifTableNotExist)
    .field(mgtId,    'id',        required)
    .field(mgtFile,  'string',    required)
    .field(mgtDtHr,  'timestamp', required)
    .field(mgtBatch, 'int',       required)
    .build();

  return transaction
    .runScript(sqlTableMigrations)
    .then(transaction.commit)
    .then(transaction.openTransaction);
};

var selectMigrationsExecuted = function(transaction) {
  var idDesc = mgtId.concat(' DESC');

  var sqlSelectMigrations = builder
    .select([mgtFile, mgtBatch])
    .from(migrations)
    .orderBy([idDesc])
    .build();

  return transaction.runScript(sqlSelectMigrations).then(function(resultSet) {
    return resultSet.rowCount ? resultSet.rows : [];
  });
};

return poolPostgres.connect().then(function(connection) {
  return connection.openTransaction().then(createTableMigrations).then(function(transaction) {
    return selectMigrationsExecuted(transaction).then(function(allMigrations) {
      var lastMigration = allMigrations[0];
      if (lastMigration) batchNumber = lastMigration[mgtBatch];
      if (isUpMigration) batchNumber++;

      getFileObject = function(file) {
        return allMigrations.filter(function(migration) {
          return migration[mgtFile] === file;
        })[0];
      };

      return searchMigrations(lastMigration).then(function(resultSet) {
        var executeMigration = function(sqlAction, file) {
          var insertMigration = builder
            .insert({ mgtFile: file, mgtBatch: batchNumber, mgtDtHr: moment().format('YYYY-MM-DD HH:mm') })
            .into(migrations)
            .build();

          var deleteMigration = builder
            .delete({ mgtFile: file })
            .from(migrations)
            .build();

          var sql = isUpMigration ? insertMigration : deleteMigration;

          return transaction.runScript(sql).then(function() {
            return sqlAction(transaction);
          }).then(function(sqlExecute) {
            if (!sqlExecute) return;
            return transaction.runScript(sqlExecute);
          }).then(transaction.commit).then(transaction.openTransaction).then(function(newTransaction) {
            transaction = newTransaction;
            colog.warning('SCHEMA-ADJUST:: '.concat(file));
          }).catch(function(err) {
            transaction.rollback();
            colog.error(err);
            process.exit();
          });
        };

        var beginTrs;

        resultSet.forEach(function(m) {
          var sqlAction = m[isUpMigration ? 'up' : 'down'];

          if (!beginTrs) {
            beginTrs = executeMigration(sqlAction, m.file);
          } else {
            beginTrs = beginTrs.then(function() {
              return executeMigration(sqlAction, m.file);
            });
          }
        });

        return beginTrs;
      });
    });
  });
}).catch(function(err) {
  colog.error(err);
}).then(function() {
  process.exit();
});
