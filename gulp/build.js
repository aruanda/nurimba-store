'use strict';

var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('build', function(cb) {
  runSequence(
    'clean',
    'babel',
    'copyCSS',
    'copyJson',
    'copyFonts',
    'copyImages',
    'browserify',
    'copyJS',
    'inject',
    cb
  );
});

gulp.task('build:prod', function(cb) {
  runSequence(
    'clean',
    'babel',
    'copyCSS:prod',
    'copyJson',
    'copyFonts',
    'copyImages',
    'browserify',
    'copyJS:prod',
    'inject:prod',
    cb
  );
});
