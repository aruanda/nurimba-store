'use strict';

var gulp = require('gulp');
var babel = require('gulp-babel');
var paths = require('./paths');
var srcMain = paths.app.concat('/**/*.js');

gulp.task('babel', function() {
  var babelOptions = {
    plugins: [
      ['transform-react-jsx']
    ]
  };

  return gulp
    .src(srcMain)
    .pipe(babel(babelOptions))
    .pipe(gulp.dest(paths.build.concat('/app')));
});
