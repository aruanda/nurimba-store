'use strict';

var gulp = require('gulp');
var del = require('del');
var paths = require('./paths');
var vinylPaths = require('vinyl-paths');

gulp.task('clean', function() {
  return gulp.src([
    paths.stc,
    paths.build
  ]).pipe(vinylPaths(del));
});
