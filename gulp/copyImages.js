'use strict';

var gulp       = require('gulp');
var paths      = require('./paths');
var imagemin   = require('gulp-imagemin');
var pngquant   = require('imagemin-pngquant');
var bowerFiles = require('main-bower-files');

gulp.task('copyAllImages', function() {
  var imageFiles = bowerFiles('/**/*.{jpg,png,bmp,ico,jpeg,gif}')
    .concat(paths.web.concat('/assets/**/*.{jpg,png,bmp,ico,svg,jpeg,gif}'));

	return gulp.src(imageFiles)
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
		.pipe(gulp.dest(paths.stc));
});

gulp.task('copyImages', ['copyAllImages'], function() {
	return gulp.src(paths.stc.concat('/*.{jpg,png,bmp,ico,jpeg,gif}'))
		.pipe(gulp.dest(paths.css));
});
