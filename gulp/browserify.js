'use strict';

var gulp = require('gulp');
var paths = require('./paths');
var browserify = require('gulp-browserify2');

var srcMain = paths.build.concat('/app/Main.js');

function runBrowserify(isProdution) {
  isProdution = Boolean(isProdution);
  var isDevelopment = !isProdution;

  var browserifyOptions = {
    fileName: 'nbAdmin.js',
    insertGlobals: false,
    options: { debug: isDevelopment }
  };

  return gulp.src(srcMain)
    .pipe(browserify(browserifyOptions))
    .pipe(gulp.dest(paths.js));
}

gulp.task('browserify', function() {
  var isDevelopment = false;
  return runBrowserify(isDevelopment);
});
