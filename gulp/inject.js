'use strict';

var gulp = require('gulp');
var paths = require('./paths');
var inject = require('gulp-inject');
var bowerFiles = require('main-bower-files');
var ts = '?' + (require('moment')().format('YYYYMMDDHHmmss'));

function injectFiles(allFiles, path) {
  var pipes = gulp.src(paths.app.concat('/admin.html')).pipe(inject(gulp.src(allFiles, {read: false}), {
    transform: function(filepath) {
      var fileParts = filepath.split('/');
      fileParts.shift();

      var fileName = fileParts.pop();
      var fileExt  = fileName.split('.').pop().toLowerCase();

      if (fileExt === 'css') {
        if (fileParts[1] === 'adm') {
          fileParts.shift();
          fileParts.shift();
          fileParts.shift();
          fileName = fileParts.join('/') + fileName;
        }

        var fileCSS = '/assets/css/'.concat(fileName);
        var tagCSS = '<link rel="stylesheet" href="' + fileCSS + ts + '">';
        return tagCSS;
      }

      if (fileExt === 'js') {
        var fileJS = '/assets/js/'.concat(fileName);
        var tagJS = '<script src="' + fileJS + ts + '"></script>';
        return tagJS;
      }
    }
  }));

  return pipes.pipe(gulp.dest(path));
}

gulp.task('inject', function() {
  var allFiles = bowerFiles('/**/*.{css,js}')
    .concat(paths.app.concat('/**/*.css'))
    .concat(paths.js.concat('/nbAdmin.js'));

  return injectFiles(allFiles, paths.stc);
});

gulp.task('inject:prod', function() {
  var allFiles = [
    paths.js.concat('/nbAdminProd.js'),
    paths.css.concat('/nbAdminProd.css')
  ];

  return injectFiles(allFiles, paths.stc);
});
