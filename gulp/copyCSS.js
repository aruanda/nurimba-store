'use strict';

var gulp = require('gulp');
var paths = require('./paths');
var cssnano = require('gulp-cssnano');
var concatCss = require('gulp-concat-css');
var bowerFiles = require('main-bower-files');

function copyCSSWeb() {
  var cssWebFiles = bowerFiles('/**/*.css')
    .concat(paths.web.concat('/assets/css/*.css'))
    .concat(paths.app.concat('/**/*.css'));

  return gulp
    .src(cssWebFiles)
    .pipe(gulp.dest(paths.css));
}

function copyCSSAdm(isProd) {
  var cssFiles = bowerFiles('/**/*.css')
    .concat(paths.app.concat('/**/*.css'));

  var stream = gulp.src(cssFiles);

  if (isProd) stream = stream
    .pipe(concatCss('nbAdminProd.css', { rebaseUrls: false }))
    .pipe(cssnano({
      discardComments: {
        removeAll: true
      }
    }));

  return stream.pipe(gulp.dest(paths.css));
}

gulp.task('copyCSSWeb', copyCSSWeb);
gulp.task('copyCSS', ['copyCSSWeb'], function() {
  return copyCSSAdm(false);
});

gulp.task('copyCSS:prod', ['copyCSSWeb'], function() {
  return copyCSSAdm(true);
});
