'use strict';

var path = require('path');
var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var runSequence = require('run-sequence');
var nodeInstance;
var dir = path.resolve(path.dirname(__dirname));
gulp.task('nodemon', function (cb) {
  nodeInstance = nodemon({
    script: 'server.js',
    ext: 'html js json',
    ignore: [
      dir.concat('/.git/**/*'),
      dir.concat('/gulp/**/*'),
      dir.concat('/static/**/*'),
      dir.concat('/src/adm/app/**/*'),
      dir.concat('/src/adm/build/**/*'),
      dir.concat('/migrations/**/*'),
      dir.concat('/node_modules/**/*'),
      dir.concat('/bower_components/**/*'),
      dir.concat('/pedal-works-guide/**/*')
    ],
    env: { 'NODE_ENV': 'development', 'DEBUG': 'http,express:*' }
  });

  setTimeout(cb, 2000);
});

var paths = require('./paths');

function watch() {
  var watchFiles = [
    paths.app.concat('/**/*.{js,css,html}'),
    paths.web.concat('/assets/**/*.*')
  ];

  gulp.watch(watchFiles, ['alert']);
}

gulp.task('watch', watch);
gulp.task('alert', function(cb) {
  runSequence(
    'build',
    function() {
      nodeInstance.emit('restart');
      cb();
    }
  );
});
