'use strict';

var gulp = require('gulp');
var paths = require('./paths');
var bowerFiles = require('main-bower-files');

gulp.task('copyFonts', function() {
  var fontsFiles = bowerFiles('/**/*.{eot,svg,ttf,woff,woff2}');

  return gulp
    .src(fontsFiles)
    .pipe(gulp.dest(paths.fonts));
});
