'use strict';

var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('serve', function() {
  return runSequence(
    'build',
    'watch',
    'nodemon',
    'browsersync'
  );
});
