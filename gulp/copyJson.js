'use strict';

var gulp = require('gulp');
var paths = require('./paths');
var bowerFiles = require('main-bower-files');

gulp.task('copyJson', function() {
  var jsonFiles = bowerFiles('/**/*.json')
    .concat(paths.adm.concat('/**/*.json'));

  return gulp
    .src(jsonFiles)
    .pipe(gulp.dest(paths.build));
});
