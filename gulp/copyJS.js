'use strict';

var gulp = require('gulp');
var paths = require('./paths');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var bowerFiles = require('main-bower-files');

function copyJS() {
  var jsWebFiles = bowerFiles('/**/*.js')
    .concat(paths.web.concat('/assets/js/*.js'));

  return gulp
    .src(jsWebFiles)
    .pipe(gulp.dest(paths.js));
}

function copyJSProd() {
  var jsFiles = bowerFiles('/**/*.js')
    .concat(paths.js.concat('/nbAdmin.js'));

  return gulp
    .src(jsFiles)
    .pipe(concat('nbAdminProd.js'))
    .pipe(uglify())
    .pipe(gulp.dest(paths.js));
}

gulp.task('copyJS', copyJS);
gulp.task('copyJS:prod', ['copyJS'], copyJSProd);
