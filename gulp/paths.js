'use strict';

var path = require('path');
var src = path.resolve(__dirname.concat('/../src'));
var adm = src.concat('/adm');
var web = src.concat('/web');
var stc = path.resolve(__dirname.concat('/../static'));

module.exports = {
  web:   web,
  adm:   adm,
  stc:   stc,
  app:   adm.concat('/app'),
  build: adm.concat('/build'),
  js:    stc.concat('/js'),
  css:   stc.concat('/css'),
  fonts: stc.concat('/fonts')
};
