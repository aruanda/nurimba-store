'use strict';

var glob = require('glob');

glob.sync('./gulp/**/*.js', {}).map(function(file) {
  require(file);
});
